-- MySQL dump 10.13  Distrib 5.5.29, for Win64 (x86)
--
-- Host: localhost    Database: pick_me
-- ------------------------------------------------------
-- Server version	5.5.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `blacklist`
--

DROP TABLE IF EXISTS `blacklist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blacklist` (
  `IssuerUserID` int(11) NOT NULL,
  `TargetUserID` int(11) NOT NULL,
  `Reason` text,
  `IssueDate` datetime DEFAULT NULL,
  PRIMARY KEY (`IssuerUserID`,`TargetUserID`),
  KEY `Blacklist_Issuer` (`IssuerUserID`),
  KEY `Blacklist_Target` (`TargetUserID`),
  CONSTRAINT `Blacklist_Issuer` FOREIGN KEY (`IssuerUserID`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Blacklist_Target` FOREIGN KEY (`TargetUserID`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blacklist`
--

LOCK TABLES `blacklist` WRITE;
/*!40000 ALTER TABLE `blacklist` DISABLE KEYS */;
/*!40000 ALTER TABLE `blacklist` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 trigger blacklist_auto_date
before insert on blacklist
for each row
set new.IssueDate = now() */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `pick_me`.`blacklist_remove_friends`
AFTER INSERT ON `pick_me`.`blacklist`
FOR EACH ROW
BEGIN
delete from friends
    where (UserID0 = new.IssuerUserID and UserID1 = new.TargetUserID)
       or (UserID0 = new.TargetUserID and UserID1 = new.IssuerUserID);
delete from requests 
    where (SenderUserID = new.IssuerUserID and RecipientUserID = new.TargetUserID) 
       or (SenderUserID = new.TargetUserID and RecipientUserID = new.IssuerUserID);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `friends`
--

DROP TABLE IF EXISTS `friends`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `friends` (
  `UserID0` int(11) NOT NULL,
  `UserID1` int(11) NOT NULL,
  PRIMARY KEY (`UserID0`,`UserID1`),
  KEY `Friend_User0` (`UserID0`),
  KEY `Friend_User1` (`UserID1`),
  CONSTRAINT `Friend_User0` FOREIGN KEY (`UserID0`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Friend_User1` FOREIGN KEY (`UserID1`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `friends`
--

LOCK TABLES `friends` WRITE;
/*!40000 ALTER TABLE `friends` DISABLE KEYS */;
INSERT INTO `friends` VALUES (19,13);
/*!40000 ALTER TABLE `friends` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messages` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SenderUserID` int(11) NOT NULL,
  `RecipientUserID` int(11) NOT NULL,
  `SentDate` datetime DEFAULT NULL,
  `ReadDate` datetime DEFAULT NULL,
  `Text` text NOT NULL,
  `Read` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `Message_Sender` (`SenderUserID`),
  KEY `Message_Recipient` (`RecipientUserID`),
  CONSTRAINT `Message_Recipient` FOREIGN KEY (`RecipientUserID`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Message_Sender` FOREIGN KEY (`SenderUserID`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 trigger message_auto_date before insert on messages
for each row
set new.SentDate = now() */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 trigger message_auto_read_date before update on messages
for each row
begin
    if new.Read = 1 then 
        set new.ReadDate = now();
    end if;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifications` (
  `UserID` int(11) NOT NULL DEFAULT '0',
  `NewRequests` int(11) NOT NULL DEFAULT '0',
  `NewMessages` int(11) NOT NULL DEFAULT '0',
  `NewFriends` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`UserID`),
  CONSTRAINT `notifications_ibfk_1` FOREIGN KEY (`UserID`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifications`
--

LOCK TABLES `notifications` WRITE;
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
INSERT INTO `notifications` VALUES (13,0,0,0),(14,0,0,0),(16,0,0,0),(19,0,0,0);
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `requests`
--

DROP TABLE IF EXISTS `requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `requests` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SenderUserID` int(11) NOT NULL,
  `RecipientUserID` int(11) NOT NULL,
  `IssueDate` datetime DEFAULT NULL,
  `Declined` tinyint(1) NOT NULL DEFAULT '0',
  `Text` text NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SenderUserID` (`SenderUserID`,`RecipientUserID`,`IssueDate`),
  KEY `Request_Sender` (`SenderUserID`),
  KEY `Request_Recipient` (`RecipientUserID`),
  CONSTRAINT `Request_Recipient` FOREIGN KEY (`RecipientUserID`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Request_Sender` FOREIGN KEY (`SenderUserID`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `requests`
--

LOCK TABLES `requests` WRITE;
/*!40000 ALTER TABLE `requests` DISABLE KEYS */;
/*!40000 ALTER TABLE `requests` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 trigger request_auto_date before insert on requests
for each row
set new.IssueDate = now() */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DeviceID` varchar(18) NOT NULL,
  `Name` varchar(45) NOT NULL,
  `PasswordHash` varchar(64) NOT NULL,
  `Birthday` datetime NOT NULL,
  `Gender` enum('m','f') NOT NULL,
  `Picture` longtext,
  `Description` varchar(200) DEFAULT NULL,
  `LookingForGender` enum('m','f') DEFAULT NULL,
  `LookingForAgeMin` tinyint(4) NOT NULL DEFAULT '18',
  `LookingForAgeMax` tinyint(4) NOT NULL DEFAULT '99',
  `LookingForRange` smallint(6) NOT NULL DEFAULT '200' COMMENT 'meters',
  `Latitude` double DEFAULT NULL,
  `Longitude` double DEFAULT NULL,
  `LastUpdate` datetime DEFAULT NULL,
  `RequestRepostDelay` smallint(6) NOT NULL DEFAULT '7' COMMENT 'days',
  `RequestDefaultMessage` text,
  `Country` varchar(45) NOT NULL,
  `Secret` varchar(64) NOT NULL,
  `Blocked` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `DeviceID_UNIQUE` (`DeviceID`),
  UNIQUE KEY `Name_UNIQUE` (`Name`),
  KEY `Country_Index` (`Country`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (13,'819387623712801','Noemi','1411242b2139f9fa57a802e1dc172e3e1ca7655ac2d06d83b22958951072261b','1992-02-05 00:00:00','f',NULL,'Bruciate all\'inferno.',NULL,16,99,90,45.15696262,9.40209245,'2014-12-27 15:15:36',7,NULL,'IT','4341df9cbc540793285fb66cc8f55dda0926d1291dff128f5e8d31f6fee2705d',0),(14,'357194042542241','NexusS','1411242b2139f9fa57a802e1dc172e3e1ca7655ac2d06d83b22958951072261b','1990-01-07 00:00:00','f','/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsK\nCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQU\nFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wAARCAHgAeADASIA\nAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQA\nAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3\nODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWm\np6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEA\nAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSEx\nBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElK\nU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3\nuLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwDyWG+0\nmdTIjBP9h6ytd8ZaJYxlZzGGA4IIFeEap8RNRvgyQlbdP9nr+dczNdy3RZppGdiR1NfP0sDreoz6\n2pj+VWgj0rV/Gvhy8v08yxFwM/NMqDI/xrvfB1xompQB7RonIx8ncfUV87ADaMn2z61PZ39xp1wJ\nreV4pFOVdDgiuueGUlZM5KeMcX7y3PsK0C7FwcKOwFacQG1SuOK+ePB/xou9Pljj1ZftMJ4MqcOP\nqOhr2vwx4u0rxDbmWxu45iAdyg4dfqteXUpzpfEj0qdSnU+FmzcwlofmcIx7msibRJ7t/lvVBPYH\n61ffWNOuG8mdzGeRzxSyeHo7gM1hdbifes1OyOaXvS90wn8GXoPEysD/ALfX/OKqN4O1CPOxFkPs\n1V9aj8U6NqUaw20txaEHc68kcdBVaw8canHEXnY2zq23bKQT+NVKMrXRyzhFfEiSbQdTRSfssnHG\nQKga0uoVKvGyfUfnTpfi1JCxhytxJ/EEGant/HzXhCSRRsx5C55xUck18SMPZ03qmzJaQoGJ7dB6\n02JWuMqDgfyq1rN0uogOB5Qx90dKz4JGgYMjkdeatK6PRoZXHEQvCor9izIslsu7BOBxxXB+IPEO\nuWd05H7qPPymMZx9a9Jk1WF4kX7Ou4jDNnrWTLb6cZpJGtRukGCQetb0WoO8o3E8mxcE5NXXqefa\nZ4/1CKYfamE0ROWyuD+Fbf8Awsi1DALDKVPf1p+oeBLPULOS4jdbGUEkBTlMe/pXC3ml3OlSMkoD\nL/DInKt9DXpKlQrapHkzpcrsz1HRPiOpu0a1nkgkJxh+hrp57+31n95byjSNSJyHUfuJP95e2fUV\n4hoEVtPqEa3E5hUEYOO/pXrLWSTKm1wOc8152JpwpO0Tpo4ipQlaLujWb4u6p4MuVttf0+SFSPkn\ngIaOQeqnoa37P4++Hbzre+W3/TRMVztla3F3DJY3EK6nYv1hlXcPqPQ/SuQ8TfAK93G70pHWBvnN\nrL99PYHvWdP2UtJuz/A9yGIr25oq56rN8adAUkjVIgB1wo/xqhdfHzQ1+SGYTv3CRElvyr55l0C3\n0yZ0nilaZDhkk+XB+lLHN5QKwqsX+6MV0rDwfVsJYqq91Y9T8Q/GnULouunW32cH/lpOcHn261wN\n7r+paqGF9qlzMsnWNTsX6VlSMTGxLbnOPekstPlvJ1UzrHzxvOK3hRhE551Kkn3LtvawrbyGFY43\nXq3Umo4g8rDerysecUkmoW2iSvAYzdTdyGwtMh8QXDzIyLHAegULnPpzXRpHVakxg5Oz0Oi0/S47\nuHzTbhGI6E4rSgsI4VYSyIvfA5Ncpqt1qGjXkcV8j+cyiRU6DB5BqmNXnmU/vWGcj5uBmuaam/I9\nmhKjGC1udlcixtW3ozMehLMP5VXfXDyyeULfbtGFwxbPc1yElxCQxuJi0qj7oyc+2auWHiG2g+V7\nUumMLnse1Y+zlNa6nZCtSi72SOs03Wo/tIFzlUxlSPr0p72keq+IftNhJsOw4Yj2rnf+ElgRhLHA\nqAoylGGQO6n867zw9FBf30dxDEI7eSFSWUcBivKj3pKHs3fuYYqvCvTlDsa1r4J1jT7OW7ub4LLH\nC0kSxnqw5HP4V2nwz+OlvfhdN14RGb7okYZD/wCBq356alaRRlWREHVhjcMYI/WvmzxHps3h/Wrm\n3kBQpISjeoPQipqUY1ZyUdDw6FaVKmub5n1J4i8IaF4r8QWd+0ckkcaCL92fUk8j0HtWXLY6X4Qu\nryxOnksspMCMBjYBjcWI6ZzXnvw28d63FpscazrtYkCSRdznHTFb1/rN7qp8zVpZGt5D5ayIoV8d\n81UF7JRc3sQlKrWkqaWvcyvEN7ceJ7saZpthDNh929B9zGecmua1uCXSr22WB3u5U4dwuF3eg9a9\nI1OxtNK0K4uLK5jtrNYy7mPlnIBwCf8APWvEtT8ayDB2lQCQjDv705V/be7Fe6jqpYT2Pvyd5P7k\njubS/nh022kmJWK7ctE275Q6nGDkfT86l17RNRi015/7Vea1t4nlMk3yxg9dsY64ryu98XXVxo8V\nowIhicygL3Y9/wBK2b7xos/w/vVnVnu3xEkhJOASP6ZqKLqUrLzLxNKlUu+yPK7uZ5rlySchsmr+\nk3r2l9FLExyeD268H+dZ0cDzszIrM3tzU8ltcWU9s80TQiT50LgjcoJGR68g16TXNoeJB8iO38AZ\n+w6tH0A8tuD6MR/WvtL4VyGfwvpb9cwr1r4r+Hh/fazEx+9BkAezrX2N8HboHwVppJyVQjP414+N\nbjNNkVNYJo67xZ8YPDPgGSO11W7MdyVDiJEJYr6+leYa1+17ZNJJBoulGeQfce7k2Aj1IH+NcZ+1\nHaSal4x0p7WBpme02lUGTwx/xryrTPhZrd64aWJbRCM7riQJ+nWtVOLim3Y2p0/dTtc+o/gz8VNS\n+JcOuTal5KfZ2VY4oV4UHP8AhWb8Z4RceFL3AHGD+tc/8I/DV34Atr77PJLetdhd/wBntztXGcYZ\niB39DW34hv78afcTPpMMoRSf9OmMuf8AgAwv5g15c5fvk4jjRSqczaR8z+LfCWq+J5tMj0nTri/m\na32stvGW5Dt6e2K1NC/Z/wDEggRtV+x6Mvdb64AcfRBlv0rZ+I3xP8WWWl2cdpqJsLW4DAw2SrCn\nGOMLjsa9+/ZOisNY+FsGoXNtHcah58qSzyLudjuyMk+xFezGVX2SaaS+8c3SUtm2eS+Hf2e7a4K7\nzqOqH/p1tvIiP/A3/wAK7iLQdLtNTt/CFn4Ks7bUXC5vNQ/e4A53AZw3APtX0ZcXFpp8BlmeK3RR\nnMjBQPzrwHx/8SPD2j/Euz8QC/jvbKzhCTNaEPtPzjHH1FZJzmrPUOaybSsaPjvwNqvhfwVqN7Z6\n1LbTwR5CWVvHbp1x0UZr438Qa5reta9Y2d/q165mYLI7yscknsM19GfEr9rfS9b0O/03SdEneKeM\nxm4uXC4Hc7RnP518xapqQm8Q6ZdhhgOpH/fVelg6Mab96J5+JrVJw0lqeq2fw10W2tN0iSXEgALP\nLIefwrz/AMWaTY6XcO9mgjwSpANe1WOqaGtlm8uHaYgHyk7/AOFeb/ExdImG+yVopzg/N3r6zHUa\napNU42SPlMuq1XVTqyvf/M83kUMoJUc0LGFjPIHpSl8oS3WpdiyA/Svk1sfY2ESxkXlvkOec1atd\nMSd2AIJHJJbArrI7WzvrSO5up5r6dyAIIQF2n3/xrqtA8IaY08MF1odyGlXO8tlR+OayniFBNs64\nYJzlZP8Ar8TC0f4UDWrRbi1vopF6Hg8H0qef4LakisUEUpHQBsZr1vRPC9jo8X+hRtbqxBK5yPfi\nrs801vC7xKJSvOwjGa8v67V5rReh7yyrD8vvqz62Z8+X/wAP9SsAyy2UigfxqNwFZMFpd6bIZLae\nS3mU9VJUj8a9lu/ie1vO0T2IOP7zYxXOat8QtNu3dL3w+shPIdWGfzxXoRq1npOGnyPDqYXDJ/uq\nuvmn/kVtC+LNzFstfEFoL2AdLlPllX8ehruLK7sdciEukagLlUG4w7ykqfhnn8K8e1C8sbu4Is7a\naKBv4ZDkA/WqUyy6e4uLWR4XHIMbEEfjRLCxn70HZnF7XlfLP7z6AsvEc9odj3t4hUY2l9w/I5rA\n1XT21iSWVY9sznPmxgAn3OMZridB+Kc0SrBrUX26D7omXiZR657/AI10yywazCZtC1Np2A3Nbfdl\nA9CD1/CuXknRe3zNfaOWm6/E0dP+G19eRRz7oZTj7q4Rvxp8Xhs6NGRcrHHOTkMOv0rmLzxPeWL7\nUup7eVcgq5ZCKzrvxFe6ogSedpucglg1X7KdT4noOUoODVrHXX99DZxqZZVQOQFLHgmq8kpUfOwC\ngZrkJUW6g2Nu2KwI+U8H8DWhFbTRsongSVCPvNIcgfQ0exjHW5zwbp6wZu/2hEFRBKm5v4cjNNab\nIJIJz0xVWPSnu8CxtC577V6c+tdLo3w91O7dTLiJDjryQKiU4x6n0GFzLES9yUbo52abI2tkjuD3\nqWz01r0eSLUyqw+7syK9X0v4XWcTCS4zKR13dK5D4yeJD4Na00vS4BGZo97OoweSQBnt0rKnUlUl\nywR51bBpylWm7LtuZ/h74Q20l6sk4ityWyqyvwv4V6tpfwqsQFaWbzz6g8flXzp4l1G60W7tbG7j\njmu5Cu4h2ypPTnNa3hf4karoV4GjLRKszIqFztcLgYOeCa3q4erKPNJ3N6H1VO2780fVmmeFrCwi\nVUiUYx91QKvzadE8RXyxzx0qpomrf2lpVrdfc86NXI+ozV7zgw47da8xJnsrRaHnPjr4SaV4pidp\nYvKucHE8fDD6+tfOnjD4Y6r4Uld5U+0Wuflnj5GPf0r7PIJ3cbuMZrK1HRLbUY3WSNWDZyCBzXTS\nqypvQ56lNVF5nwmrPCWIfYTjnFR2+iX2qK8kbgbSBvJxnntXsXxi+H9t4ektrmxgUW8zFGCcgNk/\nlXD+T9jiWGPjC7mHua9KNZTjeJ83icU6DcOqIdc8D29roFvd+aRcQxjzSvRveuh+Cnw/tPGWuPNO\nwk0ywUPKyjqx6Ln86isbV9c0uaxZmAZMK/XGe1bOlSXXgLwz/YdjeGOG5cvOVwHfOOCeuOKqlWjD\n4ndnPLGuVB8vXqYfxzjj1rxJbGxGGVjbloxngHjOPTmvONT8NX9nYS3KiWVYZNpkVWx7npXpUhYN\ntVQOc816x4C1OKLw/Z6XAIbq7kBeWMoeMnufpWlOpzXbJw1VyjyrofI6a7JHgNCpk/vEVZHiGWaM\nqUQgjB45r0X9obwTbeGfE1pPb2sdrHeQh3SEjYXB5Ix07cVwFnp0en2iXk43ufuRevpn2rojytXS\nPS9rWTcXIltpmtIRLdAkE/Knc16joWqf2V4ZgdMi5nO8Rr/AOg/SvIJpZbu5MspJOePQCvp7wJ4Q\n0/XfDemTXdv5SRIn72AklwRyCP8ACuatZWN6NaWqlsXtctNQPwX8Iaml751z/ad/CbPADxqFt3Vi\nc5Ibe2M9Npx3rjvEOlp8QdNjkMH2PVLVQSSOHX+L/GvoXTfh9od7o0YjzcQn50VyePw9eK4DxrbW\nujXFvp9tDHBHIzYkjX5vmwDk98Y6fWuapNuXMtGOjy8ri9dzmfhxYaJplxbWl6JHEUg8xQQMrn5i\nD0ziuq8VQeHLcyzJqAht0laMRHvjpkdjTtJTwhpUWmLpNzHeXz+Ybj7QpLsUG7GCOPukflXW6341\n8MalqY0my8PwvDcALJPIg4LDClcdRzWbpNrVlLFckrxieNeMruGPw7daZbqHjuo0MEycKATyT69C\nK5vS/B+k21tEb8o+wDl+wq5qlqkXhdbhblDPHJ5SWv8AE2GIyPYVj2OlCeFn1TUNreZt2AbgF9cC\nsaNO636nqvEwpL3ldsn1Wbw9EGjiSGVcY+SL3NcD4ngjjs5I7faYJnVgF9M13N9BYNGEiMjlh12j\nGfTArlboWFsGl+zyXChtp3naoP0rqjTjBppnPPGuvFwcenQxtHsxEyBAST6cVc1HwrqGqurqhKKc\n7piAAPYmvTfCvgYaxZ2d62oWmmWMmGZiNpCk43c9a4Xx5O0d7cWG5JEt5WVZY84cZ4PPbAz+NdFp\nJnnSjGUdA8NaVBpN/dPJexu80TKYYMyMF6k8emK9u+HFvreuaFbw6VcvBYKSoZ2wTzzwOf1r598A\nKr+LEU8CWCQAeuYjX1t+zkFl8KTJjGy5YfhXBjU+W630/U5udxpnQ6F8HFu2Wa/vp7iU/e2nb+vW\nu40f4YabpmTBZIGBzvcZY/ia7PRIUW3BRcH1rQx8vP1qIUk0mzJzlJas5eXw7HFbtlVUemK848fa\ndGug6gqj5jC2Dj2Ney3yg2zZ/KvIPGmr2DxX9kZ0E6oQyE8jI4rjxFNRkmiYt8ySPjP4hIz+G7Ny\nC3l3Lr+aqf6VH8Nvi94u8LeHrrQtCuzb27StKfKjDOCRg4JBx0q548mgbw9NCsiNKt2CqA8gYbP8\nhXGeBfE114O1S4uo7eObzFKESDsTzivepJeytY3qp+0OivfEGveKpnfVNVurpicETSM36URafHF4\ne1SAyZLqrBSeTg1m674nm1zUZrqKGKyRxjy4h0AqLStWl02/SaRBcqpG5JDkMK1tewKDs9LmzqWl\naFo3gvbNL9r1e6TfF5J4i6cGvM10+6uWRcAlGyDXqVvHpfiy/dYo3sZiRgAgrio73wDc25aewK6j\nGn3hFyy/hWyvFaHPGlCbtPQ5xricyTtHlFdQDk5OKqXsElxG87yEuqcZ9q+j/wBnL4TeE/GP26Xx\nVMYygGy2kYoD+Pau8t/2R/CvjrTdTk8PapLZTxzvHEsnzxsMcdeccjmuqXtaq3M4Sw9KTSWnc+FD\ncSMG4A9qemoMi88kcDjrXqXxU/Zy8WfCuZzqlkXsSTsvIctG349vxry57J4yy8H0Oa5ZR3ujWNns\nb1raXtsu4EmMdcdq2ovGOoW9lHbfaWTy33KcnP0zRbzvCj5UAHGQKYY7WV8yWit0yw4pSpJ7q5nG\ncoO6djvdL+LbnS1W6ZEuMhdyLk4x1qaP4oWem2xdriXUJZmzswAFrz6XT7SQ4jt8ZweWPvUlvbww\nNhbdNwx8wHWuX6lTPSWZ17bmlq9nH4hvmv1DRLPg7F6CrsPhi0t4g81tHsXB82SZjn/gNZiXzsVB\nbIHGF4IqJtRmE2GZtvo3etZUHJWTtY5aeJjGTnJXbPT47nRLG1SOKGzdVUDaFBb6etcP49h0jTXS\nS2crLMMvaheFBzz7fSs22nWO53wkwOBwynNUrzQTeSvKtwXkc5+cE1z0sM6UuZyOuvjViabg6ev5\nGfHa21/kRAq55wv+FWLPSJ7aZJY5nideQy8MK3fDnw81SW6jnRTtHfbgH869G0j4XSTgvNKyjOCE\nGB+ZrWpiacFZs4KeDrT1icZp/iSWS2W31W2GooeFlYYkH49604PCFtqKGWyiDLxlJF2ke3vXpun/\nAA/sbROYkYjnLfMfzNbkOlW0S4SJcdTxXkyxN37isezDBO1qsrnlekfDmXYULCFWPKoM12um/DvT\n4FzKnnMMcvzW89grfMh2sOgppu5bX/XA4z1rnlOT3Z1QwtKGyLdlo9vZqEjiUAelXolCYCjjPaqt\nrdLOuVYEH061ZTjGKhO51JdLBem6khZbeURSdsjINeE/FptU1G7WK7ijiuY1ABxkPhsgj/PevfAC\npOee+a8f+PN7/Zt7pBcApIrgkdeowf1rqoytNcu55WPp1HRcqb2PIfF0OoeKNVlv2WKGdypKLnHA\nwP5VRs7PUYJIorpWMIkZ/lOeTj/CurB8sKxw6t0I696tw7LhQqoGUdujV6KrStZnyEcXOErs+kfB\nniWwm0KxRbiPesCKy55Bx0pniP4oaV4aWLeTdmTPEOCQBx/kV87CwvIZs2dzKOeI2bFVbu0vdPv9\nupB7V25zJkg+9cao3Xke5LOOeH7tWkfTWifFHQtXA23scPosjYP5VR134iw2+qS2dswYRYBKcliR\nmvnWEwzcMpOejR84962dBivxrUKR5lSY7SXPUdqmEOTVmbzWdSHK1ZnZ6ze/2teTSyNI0BPmCGQ9\nG7nFeX3Mcs+q3MccTM244AHI5r1F4lZXV8CVeOa5ULFHqzzSOFmxgEn7wrRXhc8Wq/au7epLYQJo\nGmF2T98fXrmuUvrxjdRyXD+ZuO5h7V1l2YmiM11LmNc8Z68VyeiaNNrt/c+YP3MKtIx9ABkCrgl8\nTMrNO3Q1NC8P3uuZuD+5hY/I7V6L8HZYtO8QatYN+9leMT+cw5CqcYHtzUdoIrbwlZSkBV8pWYKO\n+KsfB2609Truu31zFam6xbQwyMAQgzk8+prfDuTm77Hfh/dTZ10/w50b4janBdaj/pEFuWVIh0Oe\n5/Kuiu/2bvAV/a4fRkV8YDpIykD861fhLp9v/Y6zRYmXJO8njqa7HV9f0rSonkur2C0Xr+8kAr0F\nC0dDvlNuTR8h/Gb9muDwokl5oPmMgAYWzHcHGedp9fY16J8H9CF34FsLS9UxuqbcHgqQTWj8S/jh\nokunvY20y3bo4ZXXjac84Jrg9H+IWs6vIbfQ9PLOWyHYcDnn2rzqjblY7YfDdn0jplrY6bpa2sco\nZU43Zyfzrw34wXdpB4g05hKh8qX94EIJAz1xV+z+HXjnXoJJL/V2tIy25YoZDjP4dBXI+M/hcdHh\nkM08slxn75Y46f8A16zcbsdJqLHy+L/Dvhx7WLw3pcWqX7EvLeXyFiGPZF6V0mneJvGvjcXEOnaV\npWmm0I82VLdIyrEcZJzzj0rq/AXwd8HWGk2NxqUH2q+RVczyTtGCTjjAPT8K9ThHh+1t106zS0ih\nkXJijByxxxzjk9OtbJX3OWpKzfKr26nwnr1ne2K6ibiSMSW0rqQPmG8Mc4/nVDR7C5v3d7reEILA\nOcHA716Br621tqfjWBIfOTz5jGrDJToc81zWhJNqhO0qyqw2tI2AKzpS+KPY9FqLUZS6okbw3HFY\ngnUynU7T1FchqOnkaXqS+dnYdwPr713mp6Z5BwR5hxnIbAzXD6vaS/Z7zEgUhDwe9E9Ve/VfmbUk\noydo2un3MfR7+7ujFbweZOUGEQOfrgVraxoeoNafbtQt0sYzhBubk4Hp1zWN4N8S2mjXsclxHJJI\npDDywCOvPX1FT+PPGEviq6YQI8EHQK5ya25Z+020MPaU403d3fRB4G8uPxzpKoxZXkWMEdSDla+n\nP2fNZt9M0a/W5kESicNknjkf/Wr5F0qe60+6tbqF9lxbEMjY6kHIrTbWNQmVgbuVVJyUVyAfwqK9\nH2uiZxxjzQaa7H6CzfHzwV4atj9t1iHzVGRDEd7fpXm/ib9s/SYnP9j6fcXO3jMxCA/UV8v2ngm7\n1XSY7+0mS7upGK/ZlOZMDvisW4sp7M4kgePnHzL3rSNDkjZkwjTb3PbvEX7XPjLXS0ViLfTY24xC\nm5vzNeSeI/FOr61fy3V7ezSzSsBJIW5YgdwPxr0H4bfB221DSG8SeKtT/sPRUkCRHjzJW7AeldfN\n+z/4e8bTXP8AwiOsO3lqXzdsGDv2GQP1rohR8hSqU4u62R82XCk4HJJqARtlsLkjj3NdH4q8Jaz4\nQ1afTtVtPsl1E2CG5BHqD0IrGjtJ7mViJCB0wBipa5dGdcVz6wVxbaxnuMKFxnjJOAK07fQkkmCT\n3kSMTjanzn9Km0/SIlG2Vi57jrXYeG9EtrrUbe3LC3ilkG+THKr3P5VnGScrI6J0nSp80y58Jvhw\n3izxlaaNaIZFmcebKRysQ5f6cV9Ba3+zPFYJv0p5LRlO4bScfT6V7J+zb+z3J4O8M/8ACbz2hVNY\nTFozDlIM8Z9C2Aa9UudGjmV+AMdiOtevGklT5pHzLqydRpbHy1rnhTQNH+FUunWVl5WuHaZpQpzI\nc8nI6145pGu6r4HnQ6VeT2zRn5lDHHuMV9t3vgayKOqWyoHO4gDqfWvOPFfwbsdQjlf7KHc8jHBz\n9a82reo+aDsz0sLWp0ouM43uQ/Cr4u6f8S7Kfw74os7e7aaIoVmXKygjGQPUda+dfjx+zrpPgPxK\n/wBlt8abcjzIME/L/s/hXpsPwpuvCmtwahYyyR+U4fB5wM811H7Ql/aeJPDOiGGdJpo2KyKD8w47\njtVVXN4due6NKCpRxcVS1jLddj4X3y2ybLyByRgD5cZ/Gk2w3CHy5ij8fK/y4/GtqDUxYRkLNGtu\nRzBKPMJ/CpNNu9MvZWMWlyyNgAMELID6la5Y13C52PCxqaN6makflqfMi5x1Y02dyMOh2k4wQK6K\nfwtrMkqytJBbwsBywwCM/wB2tLT9D0mK7itpZ3klK7uB+7J7kVf1qCV7XOR4S0rJ2OIW2lmICxl3\nPTatdBpngnVdSgAa3IyOGfivWtA0HSFRfKKbu3rXVW9lbwBdqjA9K4Z5hJ6QVjupZbDepK55Ro/w\niRG8y5kZmP8ACpwK7PS/A1hZH5YEDD0GT+tdX5SlScDIpCmAPYdq4KlapV+KR6dOhSpfDGxVtNPh\nthwnPTpVyNVCjA+U9q55/GcCXkkAhcrGxUuOgNdFAyyRKRkhsGsmmtzZSUloP5IPHAqPy8dzjtUy\nDk45BFQ3t5FZfPK20N8qj1pRTeiE7RV2Hk5B5xxTGiDqF4cehpI7wTLgLgeuetSpNGMZAGDgc9a6\nZUJxV5LQ5oV6c3aLuyldWSRIDGfJbOcnpRbXtzC371PMQHhkFUvEPmT6lY28bld5OABnJ9BXV2/h\nCa0glW5eYsV+TawG0+p45+ldNPD03DmnI87FY94efIlqY9x4h0+y2i4nWEE/x5AryP8AaBurTVoN\nEnt50mVXkBZDnHSvcJ/B1teWrR3LNJnn5hya4TxL8FJNWmU6bdC0ZekZjBWinCnTqqSehwTzRSpN\nSWp4NoN5cvcfZhZyzwucK5U4H413GieEBcSI1wzQwseQOte6+EPgfY6dp8b6jfSXM4X5+Aq59gKy\nvFPh7R7e5+x6bOUuQCcgjaKqbdWT9mj5mbhK7Zz+j+C9LMyb7tnhAAIcjIPrmrXiTQdIjtCl1dRz\nwp08whyv9awbnS5o32NcM397bxnrVDUYI7aEZj8zd3c54rOM6lPSwo0420M3VfhvA8TXml6jCqr8\nyhJcn8B1pLXQ9f0i1+3tNb3dqXX9+p+dfQ47Go7WVZtQltdilyNyELn8Kg8QahJo1oFceWTzsHp9\nK1U3OVmjSKaV7jtW1y+MkoTMrgBmkFVrWxubt47q5JWNUyWI6nPSs6LxCt5os0EEHkSSDMsznGR2\nrB1PxXeRafb6ZDJgKSzOvJOT0rfkb0iXTt1Oj1+Y3bx21vnMh6DsK67w7p1noGmTW1xciO7mBV4i\nRu6d646HVbzxV/ZdhEsFrPBCId6JgyZPVj611kfwxvVtjczmXehzLLJ8qkD0J61MmorlKinK9ixq\n2sW0+iW+jR7h5aL5j9M49K5DUtPmmYWWmWsl1PNhUjjGSD1J/IUa2kCxLFbFhLuyTnPFQ6Rr+q+H\np5pLS48uV1CltoOB6ZNXTUVq2XGcabV+h6n8OdP8eeINNjsoNWGh2CHYQv8ArBzg9P8AGvSLP9mu\nxkmY6rqt1q9wFMjky7VK4z9envXzWnxp1zwxI6WdzGrj7yumdxbJJ/D+tJafGzxlLemaDVJ1uRC0\njNvwDGASeO4xmvShKNloevGnKcee9rnsHxX8IaF4S8N3L2Gm28LJgiUjLHn1PNR/DOQyeF57qCE3\nVzwgSMhSeg4NeBat8WNd8VWd1ZX9z9pic5JI5GD7V6d8HvG+naPoRGoXKQxxkMSWx+FcGJ3ujspp\nqFmz6d8BSXcWkSrexCBSR5UJfdIBznca4X4toJLCQ7cOCSc96p2f7RPgpLbLatHERwB5TF+Pwrzv\n4i/tB+HtRhki08zX7jo5j2J+vP6ViqVS6dtDFSWp9GeBdLsZbeC9ngSSQxIWZwMDjsT0rofEeq2P\nh3SHvriWCG3t8b5JJcjBHt3r438MfHa81/RdS0u61CWyljtwLGKBQfMccYY/T0rzq98aeIdcsJU1\nPU7q4USrH5Mjkhcg8gfhXWqbjuYazurnb+MvFumv4s8W3sFxHJb3bukTqwO7MfYVwdv45h0uIrbx\nkn8vxzXIzeZ5rKxJIPpj1qu5Ck5wAD3PNSqUbvQ9GNScYqztY6u++IeoXikIkaKOM8k/rXP3N/c3\nrt5krtu4Izxip/D40ye6ddSuXt4ArFWjXPPau70bwp4Pu9AuNUutUuFtoz5LMEwUfGRx37V0qmkt\nEcssRr7zbPPbOwmnfbBG0j7fuoMmrc/h/ULNDJNZTxKP4niIH517Lo8Fr4G+E51e2jSabUZpPLuz\nHhhGoAQ5Pq27j2rI0n4reMJY/NkW3vrUYDRSwrhhUy5V8TJg51NYRPIkuI14zn1pwkdjhUZgT1xX\nr/irwfpPjHwtP4q0OzFhdW77b+yQfKp/vD2715dHbusuQMYOAKh2Wx2UYyqLXQ2vBstzb30bYjjC\nsG8xmOR7DFd9p9rL4l1cwW1ut0ch2ixkAZ5Nc54G8JX/AIi1SCxsbeS6uJSQI40LNgAk4A9ACa+n\nf2WvhU4v9c1W7gwImNoiOOd2QW/lXVTg5NRPKr1FFuUUcd8VfA23wPpE9pI115BL3NuowsRA4O38\nTzXl/hbxBqHhDVY76wmeKRMFlzhWHoRX3V4k+HVrfpNG8CldhU8cc+1eFfEX4HafpNhJfRrJBGhA\ndolLBQe5FVWoz5uaJeExdKEPZ1EP+LXh61+M3wYTxTptuDq+nJuZ1Xl1H30P05NfMei/DjWNUhaf\nbFborAZmnVCffBNfYPwjtB4M0m60K9uD/Zt6pYtIcJvIwSPTIx+VfMevW8vhTxtJpGsnyLVmYRz7\nvkI5w2R+FclaE5xU9n1OvDVlh5umveXT5kB+F2s2nzRfZ7pFPJtplkP6Gu4+HPwuvvFfifSdOjxu\nuHBmAz+6jByzN+Fcxpeu2XhW4uZdPle9nkXaC5IRR7Cvbv2bfjJ4f8M3E9tqkAtb67bLX7n5SM8K\nfQVyYSTU/f2+49PH06lWknFfI+1NFupdJ0O20a3nkFhBGsaxFiVwBjp+FZ994y0iXV2sVEVvcxoo\naFCeOOvJ6ms+y8UafNp4u4riOWIjcGjYENXyV8fPH+sJ4uM2h2txAsJE002whuuAee3WvdnWjU92\nJ8i6M6WrR9nxIt9D5kYyp6GqdzpiuD8uT7Cvmz4L/HjxBN4lk8P6xdwXEmwPbx7QjMuOgPc+1fRN\nl4ss76FmjlBcDLIeo9iK5HFDg2zN1Pw5HOpAQZ78VxWt/Da3vtwe3WRcdcc5r1qJVuYwyHcp5zTJ\nLRMkFScdq2owbl5BOfKtNz8rtO0Dw7ayHa02qTL2Zgqn8BW1He3FvbE2VstrDn5kij5rRhi0psyS\neH5bfI+8tbWgX9lYRzR21pcspGSWXIr4+pieVX5W35n2UKEpz+NJW6HAu9zeSSs7PIMZRc9Kn0vQ\nLvU5I0jJXyR0J5HXoah/4T6Se58wWsEbSTtEsTLyRuxXfW1nZ6ZDDdSYMsrDr1BNa18RUopR5dXt\n1MKGEpVeafM2lv0OetNN1ewnSHbv3ZwyHkD1rR0nxlqVrcTWt2uXjbARhgt7itGXUbe21ZPMnWPa\nGU5cAfnXFeMPG0+lw3Nwmn299bowUzO3zIxJwRipoe0xDalFeYpyWHdoy9D1G08dacUYXEyQsn3g\nXAFdDb3MV3GssTq0bDjHevlz4gJYtpdnPBG8l1eok0cmfuDHzA13Hh/VtV8OeHLG4dyYwgBIPAz0\nzW9TDpQjOHU0p4puco1Ft2O+bU7GGbXbSSxZ52kyk6nAU7emPrXQy6pDp1jb+aeZFAUDvxXm1tA+\nuRy3E04LT/6QDG2OnFbN7rVtdtosTHzAEKEEdG9amMFUqRhIty9jTnNHZwasvllhtxjI+bNcr408\nQW8c1gJtxTftO1sZJ4p3hvWo9Se+t5LdI1hk8sYT73JrE8QWdpNrltamMNg78dRkdK9aOFpRalFb\nHjvF1Zpwmzt42stO0+S5KqYyMepJqjcTJLpX2td8QYBggbI61B4mt5j4PDr8uyVQWyB1zUAd28HJ\nI+cbSBn0zxXTUXNBo46MuSopM3tRguL7UNHnsbgQzQnzXbaCNvGR+NdLdfEgaUfLv7dxGSAk0Q3B\nh7+lctpmpiCwKzKxliIXag5xjIP6086/p1xJG0U8DOCBtnkA5+lfPxcorll0DGVKVXEuUtY+XodY\nfFD3JjNrE0qMOGPHFNvLq684b5WUMAVwcEVw+teMLjS5vtLvApAxtDggj1x/npW7afEOzuNPX7dY\nSZKBkkjIquWT1R4ra2Ok+zaxd2Dy28shz8uQCc49q4XUtIvNPnL3MbJvOQWHIINdvoXxjsblINNe\n1eNj8iNGO+O9c7418T2cDYurkLvBMaAHr3Br3KbpRgknqc0oNs5bVb+C0Ekky4GMhjwK4i98YRal\nKbQKqoCSGxj8KyfE3iaTVbpoySqA4VB0H+Nc+XOCmQGyN0np7CuWcuccIOLRdudcl0u6neN/KkYe\nWu372PaqRuZZGL30xfuY5Dk1ItobjymjR57kn5OM4Fdj4a+GcjRS6hrcflwqpcRH7z4559Kz5oxO\nhK+xxVt4auPEPFhBNOqjkJwBW7onwkuUcy3oFqiDOZSDz9K9U+H1t9tsg8Fo1tEvy4KYArR8Z/Dn\nxL4n0trfRlWJW4edzjj0FQp1JPlSNYU5ylypHj+ka+uh6oYtOtLe4mUkNJIu7gdTVfxR8XrrW5pt\nPFtBPEOElDMCD0yMGuivvgz4u0Sykg/cyK/yyyRnaxHoa8ovtNfw/wCIXt7mLZKv8JHHPFdsacJa\nvobOjKlH3kdxpnh9tS+aS5/fAYGBkn8BXpHhr4f6TDB59yr3EjLgpIMAcdQK8Ntb27hDCO8ePOQS\nnGa7fQdc8T20MP2eWa5j2DHG4Fa5Zwk9mcbiYvxh8FaRpjxy6U9w94/DW4G9QPXPauIv9V1KbSrK\nzuYUS3gyI5PJCvyACC4GSPlHBOBz6mvXJ9SuomMl7HJBNKcEuNu4/WuO8RWkOsTM6hlKDlS+Rn6V\n0UeZe60exRqy5OVnA210untKqKrEnuM0w3crps7HnBq/DbWU0/k3LvatnaZNu4D3NTa54YuNH8si\nSK6glBdJ4nypH8x+NdFuh1xtJXMdmyMhs9qkjQM/IyT0FJ9hneMkcA+tdN4U8L/a5DPPl1U/KvrQ\ni5Pl3RJ4M0ueLXbG4O2GNHDB5fu8V02s6PBr2qFJLqOwWSTc04QlenUgc1BLGVkKoMAcAAcCrei6\nNqHiC/hsbON5p5WEaooySTVxi5aM4p1Peckcf4wsJ/Depz2NxGgZlWSOWJciRT0YMRyDXGSIQ5P3\ns9z1r66+Nfwr/wCKBhsrlkk17w3ZpOpUfNLbMSHX32tgj2Jr5ZntAGOOtKS5Hys6KK9rDmTN/wCG\nfw/l8casY3cx2iAvM4GSFB5rv9V8K2JlutMthGumxfMUY7c4HXPQmui+GdjB4a+HbT/akspdSn+z\n+cw+6qjJ/MnH41X1690PT/ErGSzkuIrwLFa2hO5GOACSeOM4P41pa7UUcd1rKXyOd8VeIHm8HaXo\nN0cWEJ2wRoMMoHrWHFod3ZbTa3E4gI5Y8hfwr0rxVa3Hhy7igiaBo7pF8uDYrJx1IPXgVa8MwPqU\nb6TcWAtpIs3DXVt08vHTnuSf1p+yUhQryhfldhfhTYXVroviDTpI2vBqESjzguAh5HP51SvP2erj\nRPD1zrF3e7mh5ECR9Rnuc8V794V8Lw+HtL3uiloQC3H3pSOfrjgVPHOdVaWwlfbHcq0RyMhc+1Y1\nIJaR6HoYerNqUpPc+f8A4XeKfFnw98a20vhZGtdYvYHs45DCGbbKNh2gjrg9a/QD4a+FtP8ACfhW\n3065Rxd+WrNLEQQZCPnLZ65I65r5++EGjaB4P8fXkOpXKXniG3RVg8wfJFGwyCue/wDKvpayuLRL\nSSWaZAc53s2FUdzXVhqijH3tzhxFD3vd2M/Xrq10XTLm/u2xawoZZGUFsKPYc15J438Yaf4YkOq3\n0y33hjV40VGJ3CFiPu4x/EvP4Guy1/4zeELHUE02S/ScyEo7gDylxwdzHivG7jSNI1pdSNleWz2H\n2rzls4nDJMoy2B33dR0rqdSDTs9Th9lOLvJOxiweLk0+1u0sbi11mwjlKxq5OY0z8uD34xxTLHwl\npHj+6+z6hp9rNI2fLx84PGe/IOK6LwJ8LtF1V5n02Zkgk2ytpqMAQGznJ74OK9V8G+ArLSdQuvLg\n8uGzk2Q+rEpkk/TcR+dWoQlBPqNVHBtLY+dbr9mbTzcMITPbplgFU8Ljp1rwPxXpFx4T12/013Ob\naUx5HfB4r9LLvSIipcqvyjJPpX53+Ob9vE/xM1sWsa3E0t9J5cYG7K7uOPpivHxNFU7OKPosBjKl\nS6m9EjN8M/E3W9Lt7qytNQnjt5kKNHvJHPceh969B+FnjEzai1vrd99ta5CW267OAsQJ6nufmNQr\n4eHhvRESW2hikmYFmRACDjpnvUCaemryx2sdosryMMdjjPPNcK5ovTc7ZzpV91p3OU+K+rxeHPiy\n8mhap5qae6/Z5YjyCDuxkdcGvuDwBqmn/FzwVpviXT3Fvqhj8u5RDgGUDDKw+vIPoRXylqvw+8Iy\nXf8AZ8ymXVJGXEdsSDH2++fzOa9n+B9vN8PvE7aRBeJFprp+8gmHzFv4XU9Ce1byrJep5f1RvVbH\n0x4djuLbTYxccTgncO2faulh1C0uIXjvIAJncf6THwUGecqODxnHSsK1mWeFWRw3HGTXPeKvFkek\n3dho6v8A8THUW2qF6xpz8x+pGB/9avYpTjCnrueVOjJzsfn+ba8js2UX83mY5JIKitfRfEVvDpyw\n3OBME2kjufWu5v8AwvY34IaPy8jHyf4Vx9/8P/7OWSe2lXy05Kynp6/1r4FqlVjaR9zCNai7x1PJ\nE0i/vtfguBboba3uRJuBxhd3WvR/HyJDYJNH+8lJBALYAxVXVJbNdMukgZVlb7mw471yvimK4vtO\nSGOaZWVg2QfvL0Iru5frFWFSWiRzq2GpyhHVsi029TXpY3e1YKFIXfyGPqK5z4mWj2mmwFIXMBkJ\ncxkhVOBjd2rf8OJJIkdsYnhMWduem0VwvjzxbfT3t/pSSxNYlwDs53Y5616FCK9q+XY8zEyXsVzb\nkniS80y/8I2TLdbL61UKkC/xKeua6DQtbuNa8LWWnPtlhKtE65wwAx09eteSSShmDKuVwBXsH7P9\nzBquuLYPAH2AuARknoK65w5I3R51Oo5zd2dl4TZpbGeIQnCDZGDniq4LC+hTcQYpVIB/hya+gpdN\n0/wrpAumeKGAYDRS7cLk4J9+vSvn7xx4l0qHxZcta3EdzA4H/HuvG7OeMVxU04zud05c9Pl7Hrtt\nprSWyyRbfPd9oTcRnj6V57qsvk+MDnG7gEZyBxVjTviHrt/DHDoug3DDO5Zrltqg461zMNnq2oa7\nc3OrskLk4ZIT0Ir0Kk1GNzDC4eWJrRpQerPUtQiF/wCD7m3aURglSgxjkVxXiPWbq50W3tIW8m3U\nAS7epxWnNstNGmeEllEZIJ5rzK51a+mjkCysMcYWuaVSVTROx6mZYChllJRrXlOWzWiRanS8SQNY\n6k0qEYZlkI7dKbo3hq91fckaHjksxwKz9Mf7Epi3SMW6kKcZr0rQPBWpW13ug1ESwyKGdos7I1I/\nibHBrl5Wr3Ph2m35GBb+G7Qed9quppliH3BwPzrR0m2uZB5SRSG2C5Tdxx681v8AgX4dar4z1Zuq\n6bbuCzxj5GwffqDXrN98DtR1i6bZqLx2rAfukjAIxnA7cVCSb5W9TGblH4Tx7SdR/sqLEUInuQ2T\nLIflT6VxnjHWbi4vJJJ5vNlf5VwOF9hXudz8CtRghlAhmR4j9+5+6w56AdTXKH4PTLrNjDqjASbi\n0kajiJOu4nuT6U4w5WaU78t7nkWm+HrzUGMnlssfUs3XHtXq/gn4R2OsIDcQPM4AIToG+prXs9As\n9O1C4WEGfYRGpb+GvUPBltPJcRLbQtISBuJU7V/D1r2aOHSjzSOiEbs5e9+Hml+DZIRb20STMuTg\nZIrvfhj8NoPFST3eo2/m2aYREYcM1dJP8GNW8Q68s7yRR2cxVnc8FPUAd69z0Tw1ZaFpMNlaxqkU\nYAAAxn3rhVG9WU2rdj0qFK7u0edp4D0yysfs0NlGkf8AdC+lNm0G2ihxEiqFGNqjHFehXWneRJIT\nyrDgYrAurBYy7KvymnJWPahbex5L4o0OMKxK/KwINeFeMfhlot94hhutVsxPEVMeQxUg9Qcivpvx\nJCskLRsgI3AEV5D8VNJlm8N3zQEpOsTNEw6hgMr+orOO+g69H2lNo8N1r4eaFbljplhJhTyzys2a\nwkb/AIReYXMUrQ4GCpGc+2K7TwT4K8Va5b29xqN39nVjuMYAy2fWvR9I+CUJZpJVNxI3JZ+a6vZa\n7nysMFUm/fZ8W+OfEmqa5q7zyzTXESE7CU2hfbFc/DrUwmALsozkn1r9ALz4IWE0bo9pG2VxyleS\n+NP2Y7SGGWS0t9jZyoB5+ldMWlueh7CcY2R89Gey1TT1VYo4p0IJk6bvaltbpY7mCJiPKdhGydMj\npUXizwZqngvUpormJ0Rj+7kI4NV7eKSaPdO3AX5ZB2qnBSWhMKjpPU24fDd1L58cKeYqnI9cZrf8\nLWk8sgtYoXkkThkQZbP0rnbLUH021aSO5ZpjgY3V1Xw+8Zv4e8RxalG4WZGBORnP1rljGUJavQ9O\nU4YiGujR6n4F+APiTxfqcCjSbiG0YjfPKuxQp9zX0/4I+DPhr4PK0kET6xr0+RboeWA9vQerGuO8\nOfHhte8JX9/a60q3NgqlrKC28t5FzjIY54+g4rjvB37Vd5pt1rNvdaVbvO826K4eX97szjDMx5x/\njXq0uToeBU502mcP+0Zr+ueHviJdxvcRiXyC0yHlJgykNGPYKcfhmvmpYPOmwq5zjFe4fF/VE+IH\niG9v4/ML43iVExjCfKAT1GQc15zZ2rabEA8J87ozlePw/OuHFxklzRPayV051/Y1HZSPZfhjp1tq\nngWxtLxvKlglkJXI5JIPSruteBbW6vLbzDFPc2g82IE7WQZ6j24/SvO9H1uWEgJvjYj/AJZ8A/hX\nVL4Y8Q+KLNGncWQT5kurhgG29MY6kc+lZU69OpbT3jvxmU18HzTbXs76Xa/Uy9K8KWUniKHVIruW\n4a3kJeFm3DB6gelel+CLLzNTFxO4mM93Gm08/uk3SYJ99uD+FeXReHdY0X7YY90rkrt+xkjkHqD7\n+lekfD/U5Z57YXcbQOJnK78A8xMOffI/Wuii3F6nkVownG8dz1rW5PL06NQMbsyMOoySa53w1Gbj\nW4FciNQ+5nY8KB1J9qu+J70rHbbGyjQqc+tcnrWtP4a8M314hH2u4Ty4gf4c9azTteb6FxhKbhQp\n7vT7zz74l6d4h13xvfazYx+RbvKFinL7SVUBQRjntXT6HH4u16wFlq+ufbYEjKojKfkH+0f4vx5r\nO+HniyO6lS01RyxwI4JXP3Gz0P19a9Au/C9woZoXdDj7yHGa54ctZc8Hp2PUxlGvgZujWjr0fkU7\nXSNF1PRrezvZmnv7Q7YjO3GxWOQvTI5rV1rwtZ6Lp9hMLKK70zLSSvCSrF2zhjg8AcV5x438E6vq\numpa292bOWHPlzqp3DJyeR9KwPCN146tNQt9B127km0tuI7oH7jAEqT/APXqopPWpHU5W9LU5H0P\n8Hb/APsC5u5WsnvJJnCA26FtiZPzZ9OlfQaXRfTkiaNDEX80HYA54xjd1x04ryX4KXcMVmsd1dxS\npEdnlgkyYz3PTnr0716f4j8T6Vpej3F7POlrBAhlnuJGCxwxqOn1/wDrV6NKqkrHkVcPJXkzzT9o\nv4laf8Nvhtcy+aX1PUpVs7a2QkSNuB3sD2Cj8ckV8+eA/BmnWLPqsWmLbX92oyjMXaNT/Dk9z3ry\nn4u/G5fil8WLfU5w0Ph+xlENpC/JWMHlyPVjyfwHavtj4MeB9EuNEtdVhv49XaZA0cqY2AH0Hr9a\n4a2Kgp88vRHoUcLUdLkXXc8i+Ivw8v7fQLC7aPfvYOUTnYuDjd6GuP8AD/hma5vN0W9TGhO5QRg1\n9uXPhiK8jMbxK6EdGGQa5+/8EWtvG6xWsaA8naMZrzvaqc+ZnpxpclPkR8ZxaKbHU3juUxeFjsmP\nUjOetdpeJNrHhkTpI0eoWGFaReGMZP8ASu3+Ivw8SQebCpR1O7cB0PNcn4SZWvmsZvllljaKRPXj\nr9KwjD2dTTZm7nz0rvdHt/wkt9e1DSLWFLy3upZAFhlnJwOwLAda6xPhIui66db1W9l1TWUcOJT8\nqow6bV9uw7V498L/AB5N4SKw7DN5UvygtjGGx1NfR0HiQeKbE3bLFHISQ6RSBwD1HI61rUnUWiZn\nCnCerR8YrKoUqf5Vyni+NL6WGylu2tUkUsgRNxlfcPl9uCTmuomlsNPjD3d1HEByTK4Arzfx38Q9\nBTV9M+y3H20wuS62w3EcdK8anTlJ6Hq1qkYRbMu48DX82pG3sruKOyEWTJdEE78HjA9TxWdqugSa\ndGYrkkPHEpYqQQM8ce1VZrrxHrF3dXen6GyWksnmJLc/ISPxNYl5a+JdSlv2upG09I0XeiKSGHsa\n9P2NrWkeXDEc17xNK+0u4PhXUjFM0EyQlkkcqBJ/sivErLSB9r3XucAjIDdfWvc4vh7aSeHbi6uJ\nLrUJ0i3jfMQq/hXG33h+K2to3SArG0yHdjkZyMfyrowU1aSTPNxTc2m1ocvqWjWEPmT2dvNHaYA/\ne4OD9aufC5RL4i8ixvG012UhrjvjPQV3fi7wssfgS6vQx/dmMYz15P8AjXnnwis5rzxlDFbhPN5I\n39MV3SfNScmcUdJqKR9Qad8L9D1CwNxqOoX+tTlGKtNMdmcccD/GvOPGmiaZoGuAWNnHFGgAAUd8\nZ717z4e099H8LMt2Q0pyWIGFyewrxHxx4NfVfEupX0eoHyLgoI4QD8m0AE/jiuCNWMfiZpKpy7nd\n+GbiaLTY45pY42dOA5xj8a878U3U2l6tGoZpJJZtrd+DnpW9pei6l4hmkhtVk2oBuZjhR2FdbpPw\nzgtwk10pubgfeLnIB9quri4yjypGOFxDw9eNaOtmmcXaMZ9OubTdncmB7Vt6J8JYysEt1dNFE3zM\nxAA/A12fw7+Ff9p+JruW8hDadaMreXnHmZyVH04r11vBEN1fLcXTPLHEcxQtgLH7AD+tcUW+573E\n+YUMXVp+zd7LX52aPLNP8DNql3CljpcMcCqqNcvGAzAdwPWu18UfDkPp2l6DaobeK9l3XUicHYvJ\nB9zn+dek2cFvZJ8oCLjgjrmoLmQalH5axltpJD/xKw7g+tWlK58NKrd3Ra8EeB7Dw7owht1xFu+U\nHsOwrsLXTo4Y/wB2q78dMdK4PVPGEPh3SWN1N5cMK5wx5OO5ryXWP2rrmRmttAsNgzt+03B3Fh6h\ne1XGFruRjaU2etfEfx9pngmzL30y+Y3yxxD7zN9K8gstWm1X7RrN4u0XKs+wdkHSuN1rUrj4pahb\nSJfy3Gl2uD5tzEFcyHl8eoyOK7nXN1j4eggt7cMJUEG/HCAkZJp04L2ij1OhU/sLUx/B0ceo6ts2\njznOEAUYOT1+tfVHgrwdHp1rHLIq4UDagHX1Jr5d+E1szeMLeVyXjyNuRgADgV9maawe3TA+XA/C\nvdqSt7qPdw1NNXZahdUGNoGO1S29wCzdvSqso2HrhcVQvdSGl20k/lvKQCQiDk1gtTvdkbupODEu\neDXM3kRkLAPx615R4x+NXiSJWgtdKZBnhihJArym6/aS8VaXeNFc6PNdruwSqFcUSp3IjXS0se7e\nIIQpYHPPFcT4n0o3WkSADtjiqXhX4wQeNUMb2z211/FG9R/EHx/aeGNAkZ2Xz3+SNAeWbtXKoWZ3\nc6cbmV4FjN1pxlLh1tnMLAfwleOf0/OuttPGdlZSeW0keEPIyOKsfAf4XzxeAr+51u5Qz6xObrZD\n/wAs1I6Z9TisTxZ+z1p1/fubXWntZGOAcZIFdu+55qTWx2Nrrdrqse6JlJPQCoNQ0j7ZCQEDA9eK\n57Sf2fdX0OFLnS9ca5kTnEw25rrdCv7opJY6jb/Zr6Lh1PRh6j2rPbY3i7rU8c+IfwvtPENhJDdw\nqV7ccg+ua+PPiV4OufAusGyMitbyDdC2eSvv71+gnjq7XT7dlIxkE5NfB37RmqvqPiOKI/MkWdh6\nVrTv0OHExjY86tA88wVieTwPUmtS5gms2V1YYOMqDz+I/CsvR2BEZI2kd81uTWMs0gmSJo40TJbO\nSx961lA5adSzsehfAzxaNG8TRyywNcoQY2h3YBDDGfevZPFXwh0/VPFiSalOLLTbyN7tTECgO3ko\nWyctzn8K+avClzvv41XMc24BWHB5PP8ASvpU+LZ7zS7S1uIRJPYFZf3i8ucfOp9iB29ayp1OWTTe\nh1VaTqQ5o7nUaTp2j6r8Mm1caOlrZWs7wXEaMMSbchcOecHPNef+L/Cf9vePdItrOzCahfFZEs0H\n7tIgQD+n8q9E8O/E7SfiNGmk6d4Hur65aSKOTTbRsRvjJaRiBhegHOM9+ldB4qv7fwIZbuSK1u/H\nU8RtoILMZj0yJv4d3d+Bz/Lv6ejVjx03F3W55l4J8JaOmva7Z26BtQs55I4XkXciqCQCB35r0TTP\nAtvCIp74/wBoXO0tuk4APHQfh0rznw5qDeB9L1DxTd2csqkpCYouWJLcnk/n+Nej6342TRLvS5bi\nA/2XexhDPn/Vsect7YrzWoUr20R60quIxskptylYm8uze4khhECyJjzEjxlfr6dKWTRrXUmLSQLv\nUgJKq4IPrmvH/hPbyeFvFfiPU9QuY5NLvJNiSROJCwMxCu56j738/SvUvGviO48NalpqQ2n2izZW\nknCk7kAIAPHsT+VPnildsxjQquShFal6fSMLHBkSrGMIWOGX/GkGhWv2SaO7jS5hfhw68Eegrj9b\n1E6h4y0rWNK1K3ktrW3fzIWcnduB4x03Y9eRmu00vVl8R26NbxMYsDzHA+RXIztz3IpxlCV4ocoV\nadpS6fgzxHx74DfwrcNfWAd9NLDKnkwntk+levfCHXv+Eu8OGKZt17ZgRuc8sv8AC39Pwro/7Ci1\nC2mtLtBLbzDawI4INeLaVLqHwt+IMtlb3Agi3qhaRQVeEnIJ/D0715c4PCzU4/C9z7ahiP7awc6F\nXWrTV0+6W/z/AOAe53GhI6lWjGD1zWXP4UgkViUAA6nFegafZm/0u3ujtfzEDFkGBWR4ittqwaeg\n/e3jbPl4ITuf1H512QqRmfLSozpHnvhfwxqepeIDLpG4QK3lRAD/AFpz1+lch+3N4W8U+EfC/h2S\n/wDEXnaZqEjo2l2yeXGrKAck5y/Xv0r7H+HnhO10TT4XRQJGXAwPurk/qetfG/8AwUB8YR+NPH2m\neGrWcfZNCgIlweDPJgsPwUIPrmvOlXcq/JHRLc9Wlhm6XvayPi5dozk85xya9U+Dnxn174a3bSWO\npTpaK6lrbflCO/yniuMTw/axZLyBmHqa0Nc8NWukG1kiJ23EKyDv1rWfLJcpcaNSk7vY/RD4Y/tY\naP4t0aGd9NvJZ4gv2wWqBxDk/exnOO/FfROnxaP4tsHk0u8juLqJN01vn519wO/vX4s+H/Fus+C7\n9LzS72aznU8PE2M+xHf8a+t/hB+1LP4naytNUjg0vWUIKanCjBXx/wA9EU/qK86dOVJX3RUZKo7J\n2Z9c+KPCiyRSI0YHbp1rwPxJ4MTRfEdlqJV0hjlBkaJQW2Z5wOMnFfS3gPx1b/Eu5l0OcW/9uW8Y\nkaa2bNvKpGVwcnBPv61zvxA8NQafb3Ut7tgghUs7vwqgdc1tSqdSJJO8XufNNxFGmrXvlK/kPKTH\nv+8Ubpn869y+CjudJuLdHaQIyli2MDnnHNclo+m+Gvibp134o8KLJaaZbYtr7Tp87onUgefExPzR\nvkZHVTx0xXsfwb8MR2ltf5lWdZBshlaQEsSM4A7f561VWacbdSaS5fe6H5w2nhvR4zv1b7Vfy5+9\nNMSPyqaW10WXxJo8em20MKqzbvKQAnjvXNaZcXsd4q310lxZr97Pysap6T4w07TfFNvJcTotvHI2\nGBzxjjpWMISuzprOPIz2bxPHbaXpi3UryHttj5wOe1edHW7a9S8W2kLRTKM7j90jNR6z+0fpAWa1\nj06a4CfdZiArdR0ryjWvibcajO5s7SKwjPQLya7I0Zs8aM4Rdzt/7Y1u2kljeRV0vGzggcVp63qm\nmx+FzG86JcCWJtrdcA9h1NeRNrt5qEAFzcu6HccdBx7Vk2V3Ol9FOpLOrAgHkV0woqOxjOd+h614\ni8faVd+Fb3SI7ie4uJCmzKfKuDk153oOtN4L1Rb+xQNcbSAXzg+9dPZ+GrDXNaEs1wbaOdBvZF+6\n3Q8V12pfs2+I7Xy7w2skmh5jjTUHKomXJ2g55PcnAPerbjBcrCNN1HzIo+FPj1f3VzNDr0zGCQfu\nvJThT6EV3cPiDTZjbtNcJCtwcrvOCPr6VSvP2QvEOlJ4wvrC+0nWbDwtax3d3fWFy0kLq+CPLYqN\nxGeeB0OM15PBC83G5mkPy5z+lcFWlTm7nFVXJKzR9PaX8QfCOkWptbfUoGkH3m5GT9abafE7Rr27\n8mKcTyyEqoQ8L7nPavm+70e80zYk0RRmUMMEEkH6V6B8LdD8u2ur/wAtJJGZbeIyD7jE/eHuMVEq\nUY6nFqtGj6PPivUPCGnzz6baQXchw7wyA5ZR6EVm+Hfj1JrcxtrnTHgkdwAY3yFFdFbW8U3lo4Dl\nVCs4/i96qtofhrQ9VtrmUJZ3EjEncPkf39qzjq/I+hyWGXYmf1fHR1e0rtfI2Lu81a8faEkhslYE\nSgflzVqLU76ztZTJMqKvO70Heud17xO99qdra2czQwOwTK8q2DWH8XvEB07TYNNtpxumO6bnDY7f\nhW6koxbZw51l2GwOI9lhqvMu3byb2OE+KfjiXxTfNbW0ztZJ8ue7nv8AhR8P/hLfeKLbUbm4P2TT\ntOhNzeTsMbEHIQf7TcfTNWPh14ebVZp3SFJrvnyVkGVXrliP5V6/oF5L4s+Gtn4Z0WAjUHupP7Q8\npQN6qwwWI9Tj8qqMJyXPM8/DwhJvvY4/4Z+F/wC2bu3sooTFZRYad1H3R6Z98V75NodprOiX9vaa\naGsLe08olSCVO44b36GtjwP4Bs/BXhiOxnYHUHffJNGQUZjnAz14GBWgkn9kau1vCBDY6lGgZj0B\nUjP45/nXZSppO7PXo4ZQhZbs8h8E+E4rC4hZSzlFyAqZOMk1754ckE8cQU7kYfe+lcE1yfBfiolQ\nJIC2VOM7lJ5H5/zrtrOZdMure8i5sroAyIOkbev+NbtdbnRCNlodK2lmdjjO30PeuV8VeI49DVo1\ni3yHhFUV3tiDyQcqec1zPijw+Lpi5QNnrxUvRAknKzPN7bwzqHxI0rVLqTV00ZIFYI3lFySAT0Xn\n0/OvnWTwYV8fzf2XrE+u2AIVJpARvyoPQ9MHIr3jxdY3OmQv9jmkgc9NjEGua+GPgW9vPExvLl3C\njOcnrz1NEppxSW4QouEpNu6Ot8L/AActBpb6jJbrFdiPLEDBrxXxB4Q0+/8AifpUOoXEd9Yz8g7z\nmN8EbCO3OOfevtGyhNpbKh5DLtavnrXfAUWj/EK8uvK8wS/6TGCOFwfmx/OpilFsqzk1qbtjr58N\nINPWHMEaFUGfbiuT8PJ4p8SR6zry2AfTNLZDcETAyYfft2p3xsOf613XiPw/HPa+d2kUMpHXpXJ6\nN4XGnXDyLHKFkZWdFdgrlTlSwB5wSSM9MmqQ5RfQ9K+H3jWxvZhYyArKVwRJwQfcdRWZ42sxa62Z\n4+BjGfxrA/suO31ttWgjkiunUB2LElsetb1/frq+PN4fvQ2thJNas8z+M9pJN4UkuYVy8Z5+lfAf\nxPMmp+LJNyHO0ADPGe9fo947FqPC19bzkHdGQCeOe2K/P/x3p0DX05+84kKhxxj3rakro8zEvldm\ncFo2nCSVkKiKRGxtfoa6iG4WwdkMW2Q/KvII+tZF3aPI/nK/z5APP8qvab9rjUt5fmDHXG7A+lbr\nY8/VsxIzNp+s+azHeDuG2vqP4afEmK50aB77w/Ya9cnCqZvlkGBxux1r5x1MW09wpVsSADPaur8G\nvPHcL5EjJkDbtPWvOqVVRfNa59Rl+Clj/wBwnZ9z6ej+Kes3mmf2foNrbeFbU5zDpluInY9vmHPP\nPpU+m+DptUTy7pWjthJ5u5wDKx7kt711XhfQItM0ewhlj8ydYwJHbqzHqfzq9rMFw1l5FvGEeeRY\nQ5yANxAHI6HnivT9q5LTQ+bdOMZuKdzh9Z0a80q3is7y6iutHkbdgQjzFC5YA9j6Vmw65L4/8P8A\n2STSWS3chhMw+XaHwMf1rsfFGnt4L0GC5l06W6tI2bzQysyHIxkke/PPFatl4w0WDRmeaKNNEESC\nSRQF4kGQABgkAnnHIyK82UJSbi2ezTrRhBVFDW+60/D7jxDXPCl5oXg1Y9Lj+2n7f5zKhBaRVP3c\ndevP411nw78dL8QNT1CzudPktNQkjEEsfPynoPvcqevHfFdH8NZ9IC3Gp6vZTWVnPLJ9kEhLo4By\n2CefwPNavi34epregweKvB0zRXqNHcxmGQqLpEkJCN6/KDgn1xUxoTt3RdTHRknGS17+Z55qHwr1\nPRdXvYY7mAaDHIJ1gmb5lbGSD7ZHr0Nd9b+Hbz7Pp8lm/k6PcAXUSqiNEMjBGOpPXB6AGvCNH8Za\np8RNe03wUbO4t7ua9c3sDE+ft8wtIG9MKO/oK+uNRjtrLSbTSbZCn2Dau0oRsjxhRz74/KtsNBxT\nZlmFWUuXmaba/wCBr5nPJGLaNU6gcc8ZryT45aTHFcaXq6L8+4wSfzX+tevXSHeckKoOB9a88+Ls\nRuvC3zKPkuEcOfxH9ayxnvUpHTkM3Tx9Jrq7P56HQeA/H1xpunQxTDzrVYd4XPPAzgVteAPid4Y+\nKHjHGl3qNPFCI/Ik+WRHL4YYPXt0rzHw25Gh2zjrtKn9RXxprOp3nhnxvqM1hcy2dzb3jtHNC5V0\nIYkEEd68bDpzbSdnY9vE8tNarqfq/wDHD4y6T8DPAl9rFyytesDBp9o3BnlHA4/ujqT6e5Ffk94h\n8cah4k1i+1C8lae6upXmlkc8szEkn8zUnjb4g+I/iHeLe+I9ZvdZuY02JJdSlyq+gz0rmICfmPsM\nZruoUFSjfdnBUrycrR2LI1K5kkT5sdjW1rOr3c6WkV1K8sMUarGCfuD0+lYJHyhlI+Vq9M8LfDs+\nOryC3N2toDBuLFNx49q23aViYyfspO+1jiQI72LyycMOd2etdd8PZfsms2ZRjw2DmuU1fQrnw1qM\n9tMC0SMVDj+dbfhcyLe2stqhmlDABR1Yk1z14qUGKhO09T6nt/jzrnwXsbHUbCK2urWRhDO00WWA\n5I+YEHHXjNem+G/iNrv7XEP9hXlo2gaJGvn3N7ao588D7se48AnP6H0rxD4h+Gm1L4bXVttzOIfM\nC+jDmut/ZB+IMuieGIYhmbYxgdC3TkEfzrzqS/d3W6Z1y/iWezR9Kf8ACv7DwnpUOlaVbeVaQqFV\nEGST6n1NHgPSpH8X6eEllgRJQ0joRwB16kDoK6SK/uXddWlv49Ht4lz5jS7FXPv1JPQAV0fgr7Hb\n3YnsLmxAfH72X5j/ADro54uNjCSkk7bH4X3er6he5eaeVwfU8U3SoJru+iVVJbqR+FdL4clDTQ2k\nqxSWxcFt69Bn1r0rw/oWk+Ir9tUs9Ht7AWkZEsIc7Ztv8QB6ZHavXcraJHkqLle7PF5LYC9iaRSY\n2wTgdR3rsR8P4tTMd+l1Elk0gjCKCG6fTrXV32s+HrlbiCXw7Gt1HnyJYSQqHP681kaTq+qWtpNY\n28220mcPJCV4LDODT5m12J5VGV90djB4K8K6XpVha39mr/aSzGZZP3qKODx9TnPevP8AUNE0yz1C\n4Sw3TQo22OV+rgHrXWolvq7wm5keC9jjMaNnKMPf3rGkshFdMg6n5clawpN63ep01YpxTWxW065k\ntLiPywc5znFfXvgH462q/CmB7vRYNdurGYGW0u1Ro0cf6qXaxyRkkcDj8a43wF8AdK1P4cjxCl6m\noXs0/wBllg3+W9sNuVfaR8ynkZ9V9+ea1nwvD4e1a1ht1Y2sZCFx0ZqupTVVcrFTqexbcddzoPiB\n8cfE/jzWdautTv3sxqkcVvd2Wn5hgljjUBFZAeQB65715elvZwq/2axillz0ZuK6/XvDaSRm4gB8\n+T72W+U8fp9a5eeztYIJHjusXI6heQMdapRijjabNDw/4XGreZfSkRSnCpFjgZ69fSvR4dDsvDmn\nWaSbYYo385to5dgO351i+C9VgOmLJLGswjPAKjnjGcetauuaquqlQkR2RYAY8gH3FcFe/NZGiw9T\nGVI0KS1NS8+JcWh2gfyUCFiEZzyfwrn/ABHqUut22l3V7druv5SI4+nloDjJHbJz+Vc/rloupSWx\nugirBkqY+hJ7mo4tJutTu7VzHK8O9VQDp1/+vUU1bSxvmOU/2bh4yqP35P7tz1e9vdP8NeDbO4my\nGgug8ZUZLkA4WvLdZ8QTeIL6fUZ419QH4XjoB617HpHgkeIJf7P1Y/6DYymRYm6uTgDPsMVg/HzT\ntJstO0y1s2jjuI3yIYgBhMHJ4/CrcbaPofJ813pucRoPjO90GOSe2TYzZDSMM5yDUXgb4mat4P8A\nidpH2K7ltrG7jEM8TciQk8kg++Kr6PpV/rFkws4Wube32tIoHK5Pf8q574iWs2m69o+oxoypFhc9\ngQciumjWUmoM7cNQrQtVs7Pqfpb8Kv7N122uIb+MDU5MSwzliAy+gHSsf4l2b6ZfWloVEZWXeFA4\nxnll+vcVx/w71z7R4X0+/RsXNuqshz1z1X8a9S8XRJ4/8KWGo2xxfWiuAT1OduQfrgV3aNHvxnJa\n7mX4j8Lxat4US9RSZIBuHt61V8GR/bLOS2mPmIVwua3vCWtQ3elPYTOA7x4wex7iuUsbl/DWtT28\nikIDlH9VzUs1i5M9D8HXBUS2M7ZlgO0Z7r2P9K3dQsVljJK5X0NcLLfyw3seq2y7lxtlUd19a6bW\nfEqxeHmmhYGSUKqP6bj1pW6Byu90cl4k0GC43sdrMvYdqreEYFt5yioFANZOs36+H7tJl3SLJxOT\nz17/AJ0zT9Ze1i8zG1ZTjcf4RWDjZnXBXVj0a41WMHyV2nHBJ71w/wAUbRhpUep22PtFtzz3U8MD\n+FbGn22mzwmeTU41fHCKcmqHjKeKfQJ1Z1eMIVJ9RWttCFFLYyfClhcajaRvcNujAAUdgK6aPQo0\n3bF5A/Ks/wCGKrBoVqlySflBXJ7dq7ae7gjVgEHzDGa6IwujidWzORvNARoy2ACR2rl7/TxbyHAw\nAeld/d3sSjH8q5DXLqIE4wWxWMoWZop3PD/2hZLi28FXbwuUYDGVOCODXwrf398EuIrhvMy4IZuT\nmvub9oa0udR8IyR24JYsAwXqc147Y/si63rPhQ6k14qS/K3klfvA9v5VtSkoKzPOxEJVJXirnzBq\nmp3FrcbIAWXGSdvFdDoGL2EMbgxSA9pMA+1dv4v/AGbfG2j6dLfHSzJaQqWkeNgSEH8RHpXCWWkS\n6PA11LH8owVDH5TWyaa0PPdKUPiRn61KyaoIpFB2kD930NelfByFNQ8b6RYlRJG1xGGX1xyf0rzh\nbhru9MshCohXCdsHNenfAiFpPiJpEy5j3XBJwOQdjdK86uuZpeZ9LldadHmcN+V/kfacqozplcAD\nHHauUujqms6lNEYpLWwhkDfvJA3mMp+8oxkDFdOJDIjIy+5YD9awLbxVY3+o6hYiKeP7FKIZZHTa\nrOc4AOec4/Wurnt1PHo0Z1LuMb23+89C0WRtSibS9Ttme1ntnxKBwAPlIPoeQRXg3h/wrp998SdT\n0qOea78JWsKyOJXJ2zA8KDj5s4HFd3p+pXerzatb3UT20kMz2sbJP/rehLADGABt4/8Ar1dsPDse\njxR21rCIYsZO0cnH949zRJ89milGWHcqb30/4cwtea10jw/qsOrRrqehyz71tooR5qbzsLJ6Mow3\nHrVv4P8AiTVdF8G6hpMcMkkGlSQvp4uoPLlmtS3PBx8wHB9z0ramn0qzsTNdXNt5UbhSzyDAfsDz\n1/Wo9P1fTri90+RbkNb3rPbRSQAupyDnJHTG3P4VUZ2difYTcW+Vv5f103MHwb4NtPDvxU8ReKxB\nufUZlfzG6oGBLhfx2n8DXUahc6o+v3s7ywy6XOyiERg72UZwxz7/AMqzdd1c+Hbie1eN7kxJ5lw8\nWMW8eSA7evQnA7AmqU+v6lpKTxm1hurSyZDeTGUhlVzxsGOcAhjnsazUlFWubRwtaqlJLdaf1933\nnSXXyWsgAMhHXI6H1rzr4ozZ8O29opy1xOqgHsBk13yzIJWtvP2zNztY9RXnfj//AImPjfS9LjdA\ntvF5rsTgBmOACfwB/GuPGT5KMn8j0sjpc+NhLpH3vuG6dp4s9Kih6ttFfEvxPhNt4/1yNhyLgk/i\nK++L2x8mVIhj5MLke1fEXx4sDZfE7VgQDu2v+YxmvNwjvU+R6uNV6d/M4PcDGoHeoo3KZI6YOPzp\n64AUep/KoFOMg8YyMk16+jVjxL63LLnbCw6HIPFe0/BXUfN8RWQPB2FCT3+U14gufLBzuyBXrHwW\nmaLxFp3G4NKqk/XIpRXvIttulNIb4giiv/Hl9p0o4jd15HvxXa/Cf4f6da6vdXFy8jS2+2S3UH5e\n+f6VkeNPDDWXxKvNQEuFYlgmPX3rqPA97nVlByGVCOOhFeRiKlm4o7qEbpSa1PWZLY3doQ+CjKVx\njsRivGfhJrT+BPEPiPTbiG6cbz5XkRF8kMcY/A17VbPvtkBznFOFrHHHI8caoxbJwME+9cNOq6XN\n5nbOj7TlfYzZfFXinWrZY4YHt0VcCbUpTI+D6IDx+dU9L02+i1K3uL/V7u6kjdX2o3lxjB/ujr+N\ndRaIJ/kBC07UtPNrA7go20dB1NTKtJ6XHGko6nw9oHh+51rVrWxsojJcTShFRRkk17f4G8H3OhSX\nsF5GUMMmxtw4zjkVwPhddR8H+LbO6sd0eoqwa3lT+AnjP1r6N07SnvbALeXHnTz5M0znlnY5LH8T\nX0cpx5dD56MJe02PCvHOjwRapPLbqix5BygwM45ra+GPhXRrzR9Tu9VuBHcKCLSEqSZW/wDrdfwr\ns/EfwgaVPI02UuCD88zDBPoMVzmk+Cta8PXLWt5ZSggY3lPl29eD71lCqkdcqEpPscrqGgeUI25E\nZOwH1PNaHgN4Vv5La/ijcpyGdA2V9ea7o6NNegqsIWNB8kfr71lTeEjYP509u0bglFdTnI9/Sn7W\nKVmJUJJ3R7H4fS0awjjF6sdqyA+VAoUN+VXNY8N6br2nqtqDgxk4A54OP8/hXl+k6TrOn38Zsy8t\nucfu3AOzj0P41758NdHecxSXTYkIKeTjcxGfQVM6nKk0bKKk7JHM+D/2dNZ8f2TRRQrKqttQFwvm\ne3JGD9eKq67+yHc6RcQia0hsBKxCPc3aKkmDg45NfbXw68BT2Wg3UtqhtGuEzG27GTj07c85rg/D\nNn/acfjfwFdQzS6po/8AxOLBJhyQ2Syqx65JAz6sTXFKpVeiIjOkpS2drX7nxj4t+GVx8N9QOlXK\nKJSiyAI4cYYZXBHsafe+CdV8K6dYSapbG3bUIReQq33jGSQCR2zj+VfSHwafQ/2h/ilBe6nbw6fH\nplsmNLuJA0tz5fHAwMqDyfb619G/Ff4B+HviqsU155ljqMMflRXcHOFGcKVPBAz7H3rWjSqVV7RD\nwWNoZfjueunby6H5i/2JFfKwkj25PUcVt+HxJ4emiaLLBCBnrx6Y/CvcPiF+y74o8DedcxW/9q6e\nvS4tBuIH+0nUfy968lfSJzKI9jeYxwBg5/KlGUqcuzP0yWFy7PMPq1NeT1X+Xozd8W6iLHQE1uKR\nI5J5FVAhOBjO4Y/KvMz4bv8A4hXkt5pwe+v1j8zygcmRQOQo9a1tT01Tfa3psqNHc20cciIxOSCM\nscfQitb4GeI7n4aX/wBt1HR7ibdGyW+/5BknhuRyK6X7SvK9OOp8F/YWDy+jVqY56O6j5a6bdfwO\nx/Zg+G8tzqM2tWpbdJDLYXdnPwrgkHp6ggfrVD41fDOHwr4bj0C8239xJetcJLJCqzRoBwu4feGT\nwa+q/g6h8RhtZa0ispboiV44FwuT39z71614l+GPh3xzpX2PXNNivFx8smNskZ9VYcivWWGpwgnK\nPvnxKxVSnenGXuHxh8IbbGjW1qxOxkC5r1XwTqL6JfTaXeNhCxxnoc10OrfABvAxWfRpJL3TFOSj\nAebFz3x1HuBWN4w8Pl4Le/jJWRBtcgfiKyV09T06c4SV4sztS0t9C8THy2LQyMHTHTHf/PvXReNd\nCjfTLfUUwQgyx9iMGqFvE3iDw4ZkJN5ZEN7sO/6V1GhTRa5oUltKdysm2qtdHQpvco+ELFXs5Ld8\nSI6fKa57UUNtpeoWEpw1pIJYyT1QnP8AiK2fCPnafcvZzHEkLFPqM8H8sVF8RPCUur2hms5THckY\nJU43L3FQ9rmibTsio2j2uraKsrtuyo47HisLTYdkD2roHaI7Gz3HY/lWp8PdQhtyuj3JLKF/db+o\nxwVPuP61F8TPCVy9s02mzPbTp8yuv8S9wfWl8Wo4yadjzrxLcT6NqAW0nFthWZmAGPpzXU+HNGh1\nTS7N7qSR5GRJHiLfLkj0ryqOwurjxJDDd3M16Fbc8TDA49fbOK9b0+d4LcSABMLz7VvTp9zmxFTl\ndkzamY2NyPIIWPGNo6CtAX/2uFQSdoHUfWuW/tBrl2G7PPHPFTR6m0LbSflbk+xrbWxxXuatxKQS\nwcjHb2/zmubvpEeTJ6k+nSp73WAQVLcjvWBJftJKU6nPUmsWjWLfUq61pK6p+6dA4LDAxmu7s7KE\nOlish8vylMm3+HAOQPfpWX4StE1DV442Yb8FgreorWuNKSOcpbXksDhm39CeeorA7KcerNLxFPYt\nY3ltKYpLcWbbn24BQD/DNfkV4w1lJ9RltbTdJbwyMvByOvb8q+3f2mfjLbeDPC2saFaXqS61fW32\nVED8wxtw7HHTIJxXwfNJBp8c0sTecwbnA4wRW9KFk2cOKkuZRXQhRJpbpYYkbJAJXH9a9h+Ecq6H\n4l0u5kYAQ3SFucYBOD+hNeMRa1KJEdZWEh/ACu40PW2cqVYk7gORXnYu695dD6XIHS9o6dVfEmvv\nP0ECEIV2cketeX+Pbq3sZdcKWzJeM9rc/Ng7grgblx22xn8zXT/DzxZH4k8EWd40gN0qiCYdw68E\nn6jB/GqniHwiusawL9riGKI6dNaMHOCGcEKfp8xraT9pC8TgwcY4LGSp4h25d/VO/wCNiJdZS3bR\n7qG5RIr3U5ZmXPzSRGQxLj14ZW/CrcmhWkOs63MZJJ5GhJPmOSp8xZNy7ScAcL+VRnw7BZ+HYLS4\n1CGGO2s4oo5jDvMTq4JZST34rUFtb3txd3f2l5Ir6JYtyEAKoBGV446mriv5h1cRSjd0pd18uZNf\nhcwmsDD4RtjbWNpZ6g8ll+4B/dsfOUI7Y/vc5NLY3lvFo2iXl/PFaibUZZbg5CCOQxzb156YJx+X\nrU4s9G0jS5lkL3MAKzAT3DM7GP7vOeg9OlOvNRgtru4gfT7MFSLss6BlJYHc/TrlQD+FJRs7injK\ncrxs3q38mrW3ZV1HyLD+17dEmL6zp0Qsg5LmR2V1KlsdRuUn0BqTVbK4a61XTFtZpU1NoDHcRjMa\nKFRJN57ECPI9citGfxtZxKI5hOybd6MijBHzdOefumoT4la+hnitoHtQYpXjuZMdFJUnAz3xT5Vf\nc5ljJRWkPx7Wt/6SvXU6K9urWwt7i8nAZIUJJC/Njnj/AOtXjutaXf2OvzXd1CxmmmDTcZEZH8H4\ndPwrs9U8VLPrtva2W2WLTmSe5nI3J5mfkU+uDhvqBVLWddtH8NanfxuLqC1ISZoQXCOc7VJHQkg9\na8nG1VJqEe59Bl9CWDwznPSVRfPl/wCCa24zPbSdiqnivkD9qjThZfEbzF+7LACWxjOD/wDXr6q8\nPa0NU8O2N8kbRJKowj/eUgkc/lXzr+2FYkaxod3/AM9ImQ4HUjFc2HTVZfMWI96hL5fmfPTbvLXA\nOA3BqIjc/XqenpVqP5ozgZIpjW+HJHTI4Fe3c8PlvqhkMbOM56A9a9L+E83k6nYvu5W4ix/30K85\nRTkgE9cV3fgGQQxGQfeR1b8jmpvZpm0FenNeR678TYQniiXP8Sgn8qzPCqGLWUKMRkEY9q6n4q6Z\nJNrazKAyiFGbHYetc74bTZrEAxwTwTXh4n45pdz0sOm6cX6Hs2mRsbWPnoBVxwfLII5PORTdPjb7\nGoX6VckjwAc/eGOO1edzLdnckzJjnKyZBIPFW2LzMWL7g3B9qz7hTHIzHpUtrcLkr3pRGzy/RBo6\neEde1qa7hiudPjTyTIAN7l1Gwd84JP4VxOnfHhpXEdxvgK5GUGVA9a868WeOJPGVy8zwpZo7bzFE\nxIZvU+prIiMWBjA+lfTU6KinzdTwVOT0R9V+EfiNB4haCOKbzXPQIen1Fe26Hq+oy2i2boLiNhgx\nug6Y6n2r5l/Zf+GVh4+8QSyahqT6Xp9rhpJYZAJTk4+QHqeQcd6/RKx/ZW0DX9JSDRPG97HIkYQy\niFG8zIzzghhx1BORXNODlLlW51+3p0UnVdl6N/kePjwz4f8AEninT7KKyubfTTanzdRsAHbzlbax\n2nAZQSB68GuzH7JGs3s27T7iC7sxKwWYyBQQCR8ynkHIru/DPwq1D4a6jpmlzFNSggs73bcQoSPm\nfcMjsc19B2lrGmpXM0QxKSFc5OGA9vxPNZ0aTlLlmYY/GxoKM8O07q/l28n/AFseOx/BDSPC/wAL\ntYmvNDs9a1y1tpZ1FwhYEopIRWUhgGx2Pf2rJ/Zwi+HPxA0xtT0uxEWpRkG40+aVibdv93PI9D3/\nADr6IaNSrKVBD9R6188fHP4O2ng/R9Q+IXgSMeG/Eekq15P9kysN3EvLq8fTOBnj8fUdtal7Hlcd\nUjxKOKliOanOTUpPR9vL0PopUVFCqAFHAA6CsSbwjYv4ttfEaRiPUYraSzd1HMkTENtP0ZQR9TXk\n37PP7U2ifGG1i0y+KaX4mRcNbOw2XBA5MZ/Xb1r3IPvLBT904J/pXfF068U0eZUp1cLNwlo/zR+S\n/wAZU1b4SfHjX7jSbqXT7nT9VlmtJYTtMalyyY9tpHHcGvtD9nH9tbw/8Ura30bxPLDoXicAJvkY\nLbXZ9UPRGP8AdP4E9Biftk/sw6l8Q7v/AISvwza/bL7yhHeWUXEkm37sij+I4wCOvA61+dutaXqX\ng7W7ixvrSeyu4HKSQzxlHQ+hB5Bry4uph5tLY+p5KGYUE2/eS+aZ+4/DD1rw/wDaP8M6T4e+Hms+\nKLPSrUatZbJUlCbSxLhcHHXrXyP+zB+2vrngi9ttA8S/a/EXh7aIkK/Pc22OAUJPzL/sk/Qjofcv\njj8ftN+IWjy6Ho5c6G5Uz3MkRBnYHcFAPIAIHuT+vZNxrQu9GcGEoYnA4uM4PRO911XY8K8FeGrb\n4neO5tUvM2t/LCHaNWyAAAuP0rvviBPbSX1no7Qr51mu4uFG0qRgD9P0rL0PwndaZp0/iixj2xRR\nsfKDbWZQDWbbz3et3b39wG8yXGQxztHYV6WDpa37HTm+NlVjabu2fQHwC8b6daOuj6hNHaycCCWQ\nhUfttz6/zr6SVcCvgSBeNrAMpGCD0xXp/wAK/jxqfw+voNE8TPPqvheTC22pYMk9hz92Tu8Y7Hkq\nPUYA7a1GT1R8imfV1cr4n8B2ut284h228sgIIx8jH1x2/CultbqG+torm2mS4t5VDxyxMGV1PIII\n6g1LXBY0jJwd4nzNaaddeDdZmt7gYw5jkHYf/rFWfDl9FY6rd2iHCZ3J9OoFegfGTw7us/7VhU5O\nI5gB3H3W/p+VeIi7l0+9W6VsnGMn+VSz3sNNVI3Z2+qt9m1qG6xtDrh/f0rSXVYbgiINgn1rkrrx\nE15AjzQnaMYcVNB5MipcQzfPjPWsbne7Wsauo+DbW5vo7+3Hk3cXzBh0PqCK17ueO90MmSPbKgwQ\nexFV9L1lJEG9skd6q67q8VrFMykbGX5gfX1o2Rim7nkeo6zZ6J4k89oFY3MRXcw/unI/TNTXPimC\n5OYgqJtwUzxXC/FN3uNJW+gYq9vKGGPTOD+hritM1nVZYiFTzAcYIOTXZSkuSx52IT9pc9jXUI9x\nZXXPYCrH9oq3JOT1ry62bUp2VssmOqmt2yhvYwXlJ9cmm/IzU7HQajfbP3gAb1NUtNneaSRmzjqP\nc1mpHdapOsaEugPb0rvND8HMsAZ1IwPSsJO2htFObucpN9tW9iu4JnhkiyVdDgivmP43/tR+L9D1\n6907StVaDK7XkMalickcccV9mavpUVhbEBRkDNfBP7Rngyzm8byXaRyCOTLSmPG0HPHPqeamnvqV\niJOMfdZ4nDqt74y1p/tV5JNc3cgDzzMSSSe5q3cX6aJ9q068gYKPlG3ox9TmqWq2sWm3kH2PepJD\nYY8itzx3ZQ31vbai4eOZ4QJI1GRuHv711LU8rqcnZpE12TJKIkI+XjcD6CvQtD0qO2hVvM3MSGVl\n5BFea6fpb6hNM6blSJc5Fdp8P52uZRaTzeSm/Cyn+A8/pXnYqL5WfR5RiFTrK57t8JPH6+GdTMdy\nWOn3BAmTuvXDj3HpXvHiLSk12xs57BFvUk3HzUcYIKMB+pFfKDWT2cu4Y3Aj5x0b3rv/AAB8UdR8\nLDylH2myZvntZDgE+qnsa8uhX9l7svh/I+/zPKI5lBYjDtKrb5S+fc9z03QdUvdOvra5lX95F5cR\nc9OT1wPT0qmfA7W7L9o1HeiniPBIAwRjr2znNWtB+K/hrW441S6FhcnrDdnZ+vQ1p3+hWWrSi9W7\nZ8tkLFICnb/4n9TXrRcZK8Xc/Nq2HxGEk4VoOL80VH8M6VLb28vnGFIoxHkEBDjIzn15NXphpmpX\nQMkkM0zoYF2NnIPJH/jv6VKLXTrDT/LllWOAHoz7Qec9a57U/iL4P8NJhZopHTpFafvGOM9+nr1N\nVKcIL3mZUcLiMS0qUXJ+SOsg0TTolH+gwhgc5Kg9yeM9K4Xxh4sUXk+l+HgJLor5csyEGO3yTnGO\nrE1jp4j8TfE6cW+nQtpWmSZ27P8AXTKOoB/wr0XTfhjP4Lm026tIhJHbo8skEbgyOSuQWz1+Yf5F\ncc5yqRap6Lv/AJHv0MHRwEubFNTqL7Cd7f4n+iOY8JpfeCfB11fS/Z4IWYyXK3UW65YpkjGemTn9\nM9K8l8C63eXmmeLoknlFsyLfT2CMfKlIl2hm5/h8zI60fHT4majrboqq9j5jEOit95c9CO3euI8D\neKbLw7ca29+7xxT6TdQZBIG8pmP6/OFrg0UeWJvWqVK05Vam7Pobwm6XvhGEqhQJIyYbnPOa8g/b\nI0wP4d0G9UZ2SlCceoP+Feh/AvWrfW/AsqJHtkt5vmJJO7I6/p+lZP7Wtgt38IbWcYJhuUye4HNZ\nQ9yrFPuaRtVw7a7HxZbneh4P0qwfkZ/daqwDGdvTuamwS55JG3Few0eRF2ROmOc9M5zXcfDuFJtK\n1E7gXjIyMfWuGhG5MZ54Ndf8OJ/JubyI9JeMfjWUk7HVTSbfmmfRXj6QfZtEvF4W6s0+Y9+Af61z\numWC/bLdjIA8fVcfe5rf8ZILn4VeFb0lVCW8alicfwKP6VxVp4itX1C3WO5iMjhVwHBJPfFeRjIy\n9rJo7cJJKhFNnvOmRb7JTnnPSrjxlIyMZI/KsB9ftvD/AIek1K+kMdpGoZiFyeeOAOa5dv2gNPuQ\nYtK0PV9Vbn5o7YhD+JrzoUp1PhVztnUpw0kzrdQX7wZSfpWaqmP5ycADrmuLufiF421hn+w+Djbq\neQbydR37isLUJviDcRTvcXmm6YkQG5IQZGUH1FdCwtS+pzLEQ2jqfMdsMAEt+NX4SRk5xXSt8G/G\nVoqs+h3UiAZLQgSD/wAdJrN/4RfUbIlLi1lhx2kQg/rX0fPF6pnkRpVI7xPZv2etRWy3zSS+ZaGX\ny5LUqXViwIUsgI3DNffGg2GmzfES20vXLyw8DOvkkJpVxNEt3OYj5bRN/qxgvjA5BY5J4z+Znw/1\nK98I6xFfQfPggvERwR9K+9PAHj7SPjL4Og0W4sWudRa7QrBdOEPQDcGAD8eitj1FcVSC5uZHQ+az\nT0PpLRPizrnw1TVk8fSvPbWTusbxxBpnhUgLKWAUEZZByM5kX3r0fRfEdp8UfA0upeHLqWykvImE\nUsqbXicHjcAfp0PQ18+ftUWdo/gHw1ZXttcI1tcx2i3cziSWfIC70wSznCAHdg8jjmvTPhNqd/pP\nguylubuHVns1W1JiDLLFjqrxkfLjIBB7rnnPBCty3hPWJ49bDxlBVoK0vw+7bU9U8My3c2jwR6hg\najCvl3AH98dx7HqD7188/tm/HTT/AAv4C1LwhpNyl3r+qxm3nSJs/ZYT9/d6Mw4A64JPpn3Lx/4P\nt/Gfh94pIna4VQ0TwXLQN7jevO0+leGL8AdIs5PMbQdBhbOTJeJJevn/ALaNjP4UY3E/V4qm1o1u\nY4GnSqVPbS3T2/p7fI/P/wALJqdjqlvPYtNHdI4eN4Sd4bPBGOQc1+mX7LfxD13xd4Qax8R6bfW2\npWhLi8uYHRLpWYnILDlgTz7Y96ytM8KRaIoSHV1slB+5ptnDbr/46uf1qvZeO7TS/HNxpZn1KaWG\nz+0fbpr6R1AIOQYzx2rx6GYRp1OZI+gxOHqY+m4Qhtrc9X8Q+KpWke3sn2Kp2mQdWPt6V8tfHj4L\neDvH/iNtf8SeNIfD88EKwzxLYm4lmIyQS24YODgdelcR8VP20f7P1KfTfDMJiljLKl5KgdZj0BCn\npzXmF98QfEPjEQSeIdYkvpB87RsoSNPTAXAz716ip4ivNTns/wCtjlp06eGg4wdn1ehb8NeEtC8N\n67cLpBuNQsnk2x3FxGEdx2JAJA+leieKbiz8OQQWsu0krvjEQz2/+vXmK+KZ1UR2aCFVP3h65qjq\nOsPK5e8uWlmPGA2Sa+hoYWy984K+YJK1Pc7dfiFqn9lrpXnpbac5O/8AvH6+1dHpFwotgqtxxhh3\nHrXirX08yskY2KOg6k/jW34a8T3Ok7Y597WhIGOrR+49vavWpxUNIng1akqz5pM9gjkCN0yDxXQe\nH7+XTb+G6tX23ETBlcjIHsQeo9R3zXI6ddpc2ySpITuA6cg+hBrc0+fa+WPQdjXTfmRzJWPq74V6\nlY6lprXOiAWkW7/TdGz8kEpOS8P91W5O3ofY5z6HXyH4S8YXXhDWYdSsyGIGHiJwsqd1P+eDzX1J\n4a8U2HirSLfUbCTfDKMFT96Nh1Vh2IzXk1abg/ItFvVdNh1jTriynGY5kKn29D+FfL3xA8JX3hbU\npIpoz5X3kkU/K4z1FfVpFc7438H2/jLRpLSXEc65MMv91vQ+x71zbqx1UK3spa7HynoniGKzla0u\njutpeMsfumtm3cWVztDbopOUI6YrlfHWjz+Eru7i1CMQPbH5w5wAPXPoa8V8ZftOJothJpuiRLfS\nAfLdS/diPcD+9XO6cpOyPdVeEY3Po7VvFEPh23ee7uIreEc73cCvGfHP7UGhWSTRW8r6jIRgCEYT\n86+V/FXj3WPFc5uNR1Ce6dzkB2O1foO1cvI0srYLZ4GPet1TSWpwyxLb9w9l8S/tF6hq2ny2EFjB\nDA3ylmJZiD+lepfBdp/E/hyC+lVEkkcrtQdQOM/zr5LABypGeM56c19Ofsm+KYLgf2LcOFkRzsyc\nZzyP1BrRK2kTFNyd5Ht0fh826gsmWI9KJNPJADrtBwOa9Fl0pWTC4BxwccVnyaMvGVzjBGRTNlFI\nx9I0VLfaIYxuPcCu3t7U21mC3HHejTtNCuHVQFrA8d+MoPD1m8EREl7IMInZfc1nyObsi+eNJXZx\n3xN8TxWkbQRSfvyvPog9TXw18YvFiatrDWtvJvtrcnMn99z1P9K9Z+NnxLXTtNubO2l8zUrnIZ88\nhecmvly5uGuGbJzjnNdDgqa5V8zznOVR88tjO1a2F5EcEK4+6e496i0TVTDZXNlqE8s0LHI24PzD\n3NQ3+oq7mMZwOpHeqccW92O3CnkCuiFJ8vvHPKWtjRsr1NPs7mC3cEy9gn3Rn1pPDBRL7F5MLaLO\nTJ1GPWqZG1iq/KFOCe5pksa5HzcdCaU8NGcbNmtKvKlLmR6D4g8QpIQNOuJJFQDYc8HAPUVL4a8X\nm8if7RHteJgCVHB98V55brsIKEnZ3FaOj6xPpl20se2RDw6uoORXmTyyKhaL1PosJn2JoSvzadt1\n9zPdvCmjz+M/Oj0uJLuaFN7RLIFcAdwD1FXpvBmv2swQwS22Rkb5AoP45/Stn4WfEPwRFpcGq2Vt\n/YniWwjbzVZvknQghvrn0xkVxPxX+N6f20yWztexFyxB+XaM/d9q8yOCi5tSvofVT4nxFOkpwUXf\n1/zJtRsrq0UyXVyXHlmQbSXyB1H15qtL4h0DTZ4xb3cVw5f79xwpXeB+oz9K8l1bxzf3tz9qtVuI\n7fy9pdRxuxjNZdxaX+oWqXTQD7LCVV5IjvyW5554z7V6FPC0YLmUb+p81iuIcwxUuR1OWPZaf8H8\nT7G+Hv7S+kaP4ZtbK/W683S5XRp9PhAjAdm2HLHJ3bsdO1X/AA58eNV1LxTJqN2s+oWEESWzSWzB\nFkk6lOeM5wCR2HvXyPrEzJY2trbWV2li+2SX7SoWWSUJgqH67AACFPvXo/wYtNVOlTTXVzcy2ILq\n1qrkjYwPzL2DAjnoeKivKTVzzsIkptdT2f8AaM8F+d4J0Txfc2zpfXjN9oQAtHzyMSD5TjpgfrXz\nT4unSbSVigtGWR0Xcwb06mvovxPcavr3wfXQ7TT7y7u/tIMUKpIzInqRnAGP58YrkfAvwH1y/wBS\nsbjxDpM32JZFE0MsgjDx5GQeQeRn3rzoxtqz3JO65VuaP7Jt08mg61ExUlfLATPP8QP8xXVftCQf\na/hPqkB5MYDgd/X/AD9a7S0+GfhfwJ4y1W/8LRzWWm3oKLZs5YKN2Vxnngcck1zvxftRc+C9XIO4\nNFjA6HmuObvPnR2UE1DkZ8FxKN2D37VOo9+D2qNwVuCuMAE4qRAWI7jNeyeVEWHAHc8c103gd9mt\noD3kTp3zXORx5LYHYgVv+D/l1heSGDIcY96mW1zooJ81j6g1Xw3D4q/Z80qGU8pcGLdkjpI69voK\n8NfwhB4av7OaODa6TqN+Se+K+h/BWb74D6sqrvazu5TtA9H3D/0KvO9U017zSv7QvIxAWcMidQOf\n/wBVctZu7XRioxTSfa/5np2t2v2jwsrEKUPlsV7dQarQ6nY6YikAEFdwWMZz9K2PJ+0eCQxA2eQr\nfkP/AK1aml+ArK4uUvVUqzAEIp4X1x9fSvPwV+Vo78TbmUmcaZdRvJg1smQw4CjgowyCT7YIon8F\nSak0kl/OPmXaSo+bAJx7dCc1vaj4RuNOuFhk1S42SbtiwgRKoyDtH6CrF34TtcPBexTTNarGgXzS\nxYNn5iBjJGfWu18qfvMygqko3irf8OcroviqaW3C3WnWUgIyTHH5ZH/fJFcL8StSsL+8UxQNA4Xl\nVckfrW896lpBuONprznxVcKskkx79zzgVw0Y3lofT1rOFjFhttUuLgWukTlWnO0xkDB9zXuvgb9m\njxxq1ppl3omrWsGsuyy2rNceQXbBOxGPVsjFeE+DfGen2msLDqMdz9gdW/fWq5lRhnBHIyPX/Ir2\nCH9qf+1YPClsbu+ubfRpgjaVdQpKrqhLRzB/l+YMTwADjIyc16bjJLRHyNacJSdpH1P4b8ZeNNP0\nyCx+M3w1l1W2hKRjW7OJWniVAcM+w4JzjnK9+Ca988HeO/D2qaol34UvrGztGtWa802ZTFJJK3MZ\nwRyR0Jryb4JfHTwl4Y+H9z4gu7u9MjRgXkGo3LSeWzO37hQFypXcnzuCG3AZHb37wzpHgr4iWNp4\nlsLC1eS9gWQ7cbipHAkUHBxx19BWdODn8DtI8zEyjC6qxdu62+79TudPJ+xxKQFdV2sqrtAI7Y7V\n5h8RksrKx1i+j1Dyp7OVd0L4CncAcA/ifyNdP498eQfDjRYLiaynvBIxhhWAZAIUkbj26Vwmk+G7\nvx74Lg/tux87+0poLucAkB0dw/XpwCePwrDM5KUI0VG8vyOTL4yov6zJ2jf79df68zk/ECzzfD20\n8Q6XBPdytcNDNHC2/jOEIA5JJx09a+e/E/xTs9D1CLU5ZZll1CI2TQxqDJsGQzgNjkZ47V9TfEyQ\n/BDwneeJtPkVtPsrr5NNjXClm4VQBgY3beOMYr8/9X1S/wDE2vza34luZtS1W4x8vUqo4VfYAACs\ncHlSqVLyVrfcz0Kua1VT5YS0v8/Qyrnwlbaltkt5DI4nLB5YsMFHTnJrZi0FLSMy3MpVcZPHU+1X\nYb+/RCLXSY0XsztlvyqO9u9Tv5UF1Y7o1GFWMkfjX3EKMYJWPnalec92Y8xd3OxfLiz0HWiCxV+o\n3HsfTmtqJNPmYqwks5cfdkGVP41fTRJI1yoBQ8qw6Gunl0OW5kRaeFA3DJH3eat2tkvG9fmB4J6V\noWtkSzYGTzketWhpfmxFwSpHGw1aiTc1fAKObi5tDK/lqvmRqgBK/wB4deR0+ldssghdcToSOzAp\nx9DivObJZ9JuI7y2fE0PK/7XqD+Feq6VqUGrWME8UY2yAbkIyUPcGtYohj0mWWJEWXEjEbVUZ+v6\nV0vh/wCIN/8ADvUrG6tCZbVspNbO2FkXj8j6H/6+cWYR22yVAquXwoKjIzWd4xiYpZgcMN2ffpU1\nI3VmOJ9reEPFun+M9Dh1LTpvNib5XU/ejburDsaj8ZeNtH8BaBd61rt9FYadbLueWQ9T2UDuT2Ar\n43+HnxE1T4b62LyzkM1q+FubVj8kqj+Teh7fnXkf7cPxnvfHnjS0soJpl8Nw2qS2kX3Q0jD5yw7s\nDlfw9+fMdHlfkaRV3Ywf2o/2oLv436+Y7G1XTNDtQ0UCBv3sy5PzSHv9Og96+e53ITG7LdsVXMzN\nJluo6Ckfc+4sazlvodcNFYiMhaUcgkcelKGBIHTtn1oAAViTj61BLdCFiCcseRUdAukWkkVm4UAD\nrk9a7r4L6tLp/juyWKY25nyisem4cj9Rj8a86t0nvHzEm4HmtXT0nsZ45gxWaJg644INGxpGS2P0\ns8Ka1carZxibCyYAJFdXHANhBO4mvnT9n/4u2XinS7ezmk8rVreMeajn/Wgcbx/WvUPEHxKt9Oi8\nu1Imm+7kHpRFSm7I3lWjTV5HSeL/ABtB4fthZ27hr+RfkGMhPc186fE3xOfDWj3d/fT+bdSZO88k\nt2xXR2utz6jqLz3cvneYACMfdPY18tftBfEb/hItfezt5N1paEodvR3Gcn6V3aUI+bPJnOVeV+h5\np4o8Q3OrXtxPcSl5Zmz6hR2Arlrm7aONogwbjkjtUl3Mfm3P8xGTg9Kzox9ocDd8g/ClTp395lSl\n0QzaGG5ufr2qZnSRgRlTk/SoZmLMYwcKBkkVZhiaZgoGEIzkjrXbGNzIYSzkoOfXtUsWnghg5KgD\njFaFvCoUjsBySK2NF0G41IE+UWiz/COv49qmTjBXkCdzm002aVT5QLlRztH1qQ6Nc7P9Uxwa9KaK\n0tYvstnCs0xGG2chfqaojSm/eNcybgp/1a8LiuN1Wy0jiYXu9JkilAeN/UDvntWJq0q/2pm4VnE2\nTuDbeTnknB6HGa9KvLy3uo1iWBGXGwZHyj3qLwr4N07XPFEdrO6MIgJY8jIbBBKn9a5aji9Tqoqb\n9xdTJ0bwze6tpcNlfXFwbJj+4RIgyiQZ289cbS3A79ele7fCf4LeHtI8UeHNV1WLU7jSuHu44Y1X\nfAo+fGeoYZz3weORU+jeMfA/hPEMdhJbXFpKkavdpuXflskd+T3PGB71zXxL/agu5bUWOm77ZSxH\nmxptZQQQSpBGD6f0qlyqN73M5JqTi1Y9Iuv2ffD9kZJLrUfMto5ZXit7hw2FwSAfoMdD+NelfBeb\nwt4B1G9gsNLjWXkPCpOcDoxUnJPT8xXxHbfEJ9Tij0p5pZbma989L5mzLvbjp2UnBx7mvU/AnjC+\n8J6lqgnl3354ea83GQndzsGCC3bnjBrysRO/u2se3gYpWk3c+qfFfxP0bRZnu9XvodNExODKoiD4\n64HQn6V5xqv7SHgG13Z10Tnt5Ubvx+ArxX9ptYbyPT9ZufL+3amFnhWWCaKcRbcdCBHt6fcBHvXz\n25KjuOlccaSauz3HU5dj7A1j9rXwZHn7JZ6jeyDoRCqKT9S2f0rzvxf+1BbeItHuNPtdClhSRdpa\nW4HAz6Af1r5/BIAycMSeKdD8yNkDIpqhCxEak9hk376dn+7uOafEoJwPWk2gNg5znAqaCPIY9Qv8\nq6r6GEVqySGPkBc53EVteGUMetRDqWUfoaybfd5nA6N1rc8O4TWrbA7EVlN6M7KEbzR9e/BGM3/g\nHxnYHBIuGIHsYo2/oa5vxFYRat8MZ5EBQ2+Y3bA6jBFdR+zo6PeeMrTs0MEuB/tROM/+O1T07Thf\nfC7xxGo+eBncD0IVv8Kwqu+vkiKUeVyj5v8AzNDw9J9r+H9sG/599ua7/wAJzKdFtJAQWMKnP4V5\nB8O9b83wfbwuwGExz9K9B8EXDPotl8+4KoQr9DivPwS1mmdGKdlGx2h0yC+uoLh49zwtlQTwT05F\nSf8ACOwea8726M0mNxbnp04qG1uNl2QpIU8YrpdHntz9qS9xDIjDynOcEY9K6qslTTkzOlzT92J8\nheJ9Dv7LS4rqaMxxMQFB6nPSvOPEOmv8kM2YvNxyR2zX0T4hvbbxjpltLbgm2eVTgjHAPSvMfidY\nSQapFdrlVjjxwBhcVhQWqPZrVVyu55BLqEGgagGSBJI4x0IyM+tddp3wZPjTw22s+HZ11HUmmRng\nglUeUuDu+Xrnp3rznVfN1W6cRnfI2QFTuat+D9B8Vadf79Jlms7tD91JCjZ9OOvGK99JRR8S37Wb\nZ7vpvw+8R6N4Su/Et8lzqWk3862U/myFJyQ5XIBzydp5r6X+BPxjj+Gfhu40+HSNTOm3GrCx0y5c\nDzsLKHEcoBzkq5B2A5449fGvh78bPGS6Da2PxF8HP4l8LxKY47s2zI8UmMK4cfKSuOPXJ5r3bT9Q\n8I/G/TpdO0l7xYLW7h1B47wIpVjGyMVXBLN0B5GQorxqt1K56sYKcLSV1ofZvh7WNM8V6H5tldw6\njbHMTyRE4LY+YEHkHnoeRXM3vii88JWWp3Wp3EcFhZzpbR5iCxhCowwwM9SBXn/wr8CTeDPFdjcW\nNxeJY3sjrLbQq7wsAuQ8hOdhOfulj14Jwab+0t8Qkh0HWfD5sUkg2xb7hpgGDghgFTGT2Gc/hV1a\nrqUlOL5ZLTTqeHHCqGIVLeLs35K+p4n+058QY/GlxpmjeHdTludOx9pu4YyyxGXJxkHqRk89ORXl\n2leHIrCMSOgMjDOTyahh1iOyV3eMvIxyWNW7bxPbXDshLQ7ec5r6jBYZYejGEtzy8RNSqPk26Fwn\ny2G1Pm9hVhYrifClEC55+lRxalBMitHMm7jqPetMTZx8qyA8kKa9KKOS5Qm8LwXkeHRCx7qMVTj8\nOS6IqvA5miJy1ueldLaSQO/yPznOGPNaiwq0jZx9MVqo3J5jnLLTbfUITLEcE9UPBWpxpKspCABl\nq9Jp0ljP9qiOQDgrt681uW9rHNGlxEMq3PPajlDmOKm05QpOzLjqAKdomoy+H75i5JtJTtfP8PPU\nV2N/pICtIADmucvbHc/7wDBGAMVOw9zrpIlngScHzFI+U+o9ay/Ety0lvZLgfLvGfXp/hWV4V1ka\nfqC6VeNut5s/Z3dj8p/uf4Vr+LIFgW2UEFcvnHTtRPVCj2MAgMzqfuHoR61w/wAQ/AVr4v0h4WxF\nexZME2Pun0Psa7YAFtjH5c5FVrm0YbuSO496xcb6MtOx8Z6tpd5oOozWd7AYp4WKkEdfcH0qmJN6\ndcHNfTvxJ+H8PjTTWMYEOqQIfJkIGH/2W9v5V81zaPdW2oS2lxH9nkR/LZG656VxSg47HRGVykqS\nXUoSEklhg966fRvBwkkR7oZTODk81a8M6N5UmEX96hIc11Et1/Z9zGAuUYYkB5pqPUhz7ER8M2tt\nFkARjHG3Fc7q9tFDIVQ7i3fFddqVlLHF5yPmJhwSa5iVDIcZ3E9CR0o5SVIxoNcvvBmqWGoWkjxv\nHJwUPVf4lPsRmvq/Tb6C+0+C9jlWRJo1kjbrkHkfpXzBd6Ut7YmGYAED5G7g19MfsWaVofi2KTSP\nEGpRrcaWSyWbNiS6jzxt9QOcgc9K1oyUU0ZVE5WGfEWDVdD+GOp+I/Ja2sEKwR3DDaHlY4AX1xgn\n8K+G9W1A3F3I+4tzyTySfWv0H/4KQ+K/sHgHwloNpCLGye4lligQbQwjQKOOnG/9a/N6RzLIFzyw\nzn3qkued30LilGNkJJIZiVGWzycdafIfJthu7np61JDbNDJyo3EZpl4PMdVUcEfrXQtNiRlhAzsf\n4V4JJrVt7d7gmOJCVZtowOSaZb27NstkUvJJgEAcn0Fex+D/AArbeGrEXcwRr0fxSfcj9vrUuagJ\nalLwn8JSLQanrhMNsihvsw+8w9/Sp9RuZNWzaadGmm6OmAWXgvx29a3p5NS1qIW6mVLVzmSVwcuP\nQDsKtSaba6NbjapkkA4dhwPpXJUbepotDkHurXQ7ZooIhEg4Dt1PviuP1rWzJkR5fce9dHq1o13d\ntJICV61kPpdtcM752srcAflWD1KVupmWls9yoeQ7RjgZ71ueFll0vxbpdyFDhpfLI9cjFZkkbRFU\nQhWHJJrtvhjotrdeOfDhv5ylu87MzE8cKxH8q5ZvdHo4enzNNd0dd8cNIs9E8EnxJDFC+oQlIjFP\nGHSVWOMFT1x1H0r5ne7nvrV5mmi+0SMSYgm3AA+U9OnUcV9YftHeDpdW0CynsLgNbW2oRIkB+7MC\nduW+hOfpmua0j4N+H9KDXt3dSWghRANo3Ju3crg9cEZGD9OajCxvBu51ZkrVkkuh5T8H/hq1z4w0\n+e/EsMIMdx5XAd42zjaG6kkY546DPNfYdj8Iz4k8TFDpl9ZeHBseS+1WWNZNhHzyIVO0HIIwMDBB\nFedr4x+H2keI7HzGn1a0hERigtU3LIueEbJXaP8AZ9Rmvc9I1TUPiSNQ02e7Xw14Y1G0e3S38kvM\nu5GQfNnCEEKxyMDIGaxrxRvgtFZrQ+Kv2j9W0mbxq+m6NNHcWWnHyVlTaQxBOcMANw9+c9a8lY/J\ng9ceterfG/4B6/8AB/UVa+xe6VcOVt9ThGYpCOq57MM9Pyz1rymVT3wSB2rGKVrHpzT3ZECct3w3\n8VNjbAZacxyDj2NLEBlzxwDQRFbAxO0AdQAf1qWDgSDnOP1pjkADAzTo2MZIz19qdyrLmaLasxBA\n7elbOir5esWgbnLDj3rET5VbPBHNbGkyk31qwGCHHJqJbM7aLtJep9cfs73ap8RNXtABifS7eTH+\n6zL/AOzj863PBcXnRfEXSzyDDKdueB98Z/8AHq4f4C3gg+LtiM/8fOjyLz1+WWM16H8P4Fg+MHjO\nxc4W4tZOOx5U1yzfur0FtWqLz/Q8j+HDv/Y8GDlQ2CD3616/4GufJ0wg8bJJBj/gRNeQfDecRWU0\nJA3RzMv0wcV6d4Uv4oGuYJQfmnIB7DIH9a48L7tWdx4r3qcbHceGr4apMypz82efrXb3+jTXSI0L\nFHXAO0ZyK8+8Cxvb+bIu1FQsA0nfmvTtP8T6dY2a/wBo39vBIQTteQA455x+FRUkqqcGbwhKg/aR\nPAf7LXQdPt7KI7hGCS3vXmPxW1Jo7JhJ8u5cDA6mvQ7/AFMPvJfgnPNeK/FrVGlnSAEbAN3X1rto\nQXOY4io3F2Of8Aafp0FpqWpXZma4iKrH5RUBQTyxzj36etZmk2uoy69q7aE13qsEcbTtDEGDxgqf\n3gIzwp9eoqx4D8R6Xp2qJZa1k6bdMA23HynsxyOle06b8M/Ftro8knw/1WA6TewyRXMwgiMxiwSV\n3IpYg5x9TjpXoSb1PEUVpZGj4B8c3/hLQLi01K5ujrOnx272drJILm2unkbcilAWHzRtnOcgqPXj\n66g+G3gf4r+A4Nbi06PwN4nWKKR5GfyXRmxtZgvBVyxKnGSMelfNPgbxovgq18PeEXtU3WUBln1K\nx/0ZjIyHejfeVph8yhiBuyQSDg13Xw1+ImsfCS8ubPW9ura9q95GwtpBHIm/bndI4YBBhyRjGDnO\nRXmzbTt1O20pQvB2sfVttHL8DvhXqFy14t/d2tuZopL+U7ZCFJC7upJwcDqa+WfiF8W7bxTbPb3U\n1p592/nTSRW6qCc54c89fevpz4QfECP49eFNSs/ENpYXFpNCIrmyjVxtbcwIzkgjAUhgQfbvXxb8\nVPh5p3g7x54g0Gwle6sbS5ZIZH+8o67T6kE4z3xXdhcMqtaE73iunmeJUqckKkanxu2vkFneWNzG\nFJRsngdagutHtbpn8llDN1x3NcrF4ZngcvDMwGDitCxtNQgmUEsfoK+tSdzwy8/h+4tyfJkJOABz\nWjp89zZlfN4JGPrWlpkE4VWlQkdOeta8NvFcnBVeBjBrVXIY2x8q8jG8BZM/eUVtxPJbIpZTKmMb\nh1rFks5bdS0LAEHFT2V9dMAmzJHc1UXbcg6qw2X1qxR9xPUdxT7C3a0nKHaIpens1U9Glaxn8/Zu\njI+ZF7e9dD9lgv7fzYzluqkfwmtWSTR6eJkaJscDqaxtW0NWGOHbGRiunsP3sSP/ABAbWqSeBHiZ\ncAnrj0qWropM8P8AEGll5FidjE4OY39GrWt/Ebaxp9na37GO5ti6vwCW4GD+OK6PxZ4eFzAzKpDD\n5lPpXl2vXb6dNZyrlZmDKzdcgY/xrNqysNa6nVsu5NoBQDnpUgPmW4bhjjBFUNIu01S0GCRNxkk+\n3pWikf2bcrgq4OMHqDWb1KRmT24VyQRg+hrgfiT8MIvFtsL2zxFq9uuVPaUD+E+/oa9KlGG3ED7v\nSoUGD04NQ1dWK2Pl7SrmfTrp4pomjuFJSQMOQfemXOqhrxlkZW3EHjtXtPxJ+Hy+Ibd77T1EepRj\noox5oGePrXhVhpztqrLcK0ckZ2tuHIPvXO1bQZ2SzNd6DMFy7xpkKO4rg49ZiVsSHbjI5FepeGkh\n067VnUeWww270rn/AB14Js5rx57aLyQxyQp4P0pNdQRx9xrsbELuyhwMitLQ9dfRtQt9QsbySzvb\ndxLFPC5VlYdCDWQ3hq1gJyTvXnk1oQeGbO9t2YxtCgH3gcEVHM0OyOu/ad/aFuPjVoPg6C9tI7e+\n0aKWO5kQjbO7bf3ijsCEGR6k1812cZadpiPlBOK6LxdC2mXLQeYZFGdrH0rEhjEdtx1PXHeuujrG\n5OwHP2h3AyFXFRWwErNK2dqjjmnScWkrg53VbstO89oYGICMcsR6elaNqKcmK19DpPh7a/6bJdFA\nZCcK8nRPU17bpC+GoI4jcanHd3OcmI9FrhdD0W3s7NBKu1ARx6/hW1a6DcaqWOmaUqAdZpFwB+dc\nSlzas15e52V/4o0TAEE8IwMYDVx+s+JbISOEmRwegU5qT/hAI4nJuCJnwOE6Vi65Da6MRtgRHPAD\nCpbe5KsUNU163uYJFSKQueOFrlxcT229kjVQWyN56Vav7+4unZVdYxu7VVeyJDPI4wp5GaxbKUHI\nRJxLMZZDvk7+lenfB7wzeeKvFNsYx/o9kjTZ29T0A/WvPNM09Z7mOKMGVmPB969Y+FN9q+g+PFtN\nPRSstsTKuOeCMH9a4Zat2PocHFe7fa6Nz42eH9Z1nSrHTra5S2tGlcPvOCWVcgfTOa82i8N3mmQW\nDXmrw38UykGGGUs6bAOo7jAB47Vd/ai8aXVxqNrolndEyWCs90qFlLPIAfTsPfvXiWmeIprWyWNZ\nmgmGUKSEhiW4JDemOue1TRivZ3LxtblruMeh65OPC2laxJGbkS38UkR2BGYMOCflI6g9RXbxfGDV\nNFkbS9C1qS2uJ2Ek19CVfcqYC7z2OMHjv9K8B8F2/leMCWuUk1BkZIGuQHhkm2/dLZG0HoD64zXo\n/wAJvBV/c202t3qx/ZpX8kRpcIkkpbflU4JO0r82ASNy+tY1VZk0anNolqfQ3jDX5PGn7MGp6XHp\ndxql3ZzLi8Eb3YkkZmJaMDOwkBuScDnua+C5wyg5G0g4+lfa/iia5+GHwou9chR7KC8YwwC2lQXE\nJJIG8lBv7Z4XI5Hevia5keSSRzzubJPrmuem73Z7ElywREZCUKjqVHNEY/esD39KZ1xxnqP1pVdj\nIB1bGRW1jFXHkhcE89qlTLq3ODxzURGFUn6mpbcbhn16e9K9kXBe8Tx4UEH0rU01ws0PJB3qayxg\nAE88Y5q/ZEELk4II5H1qHqrnXFpM+iPhFemD4p+DZATtmhu4T/3wWx/47XrVldGz/aLkjjdVW4hk\nV1752Ej+VeD+B7trLxv8PJwSANSMBP8A10jK/wDs1e9a7ptnZfHPR7/awuZAo3bjg7lIwfzrjrNR\npxb9DWMb4mol5M8n8HW4ttW1qBusN7Khx7Oa3zLFBrl0s9xJCGRG+Xpjmo9FsV/4WF4qtFCnbfyn\nH/Ayf61vpBBbeJTHcqhUxAjcgYna2SB+BryZ1HBykjshRUuVM0dE+IBWVbK1tbu4tVyWuUiJ6ccc\nc1tar4vtNPuBfaxpElrboiqL65UAnPRfXv8AzrsdD1KKOWJIbe2W2JG0xrgha8l/bU1OW08LaZps\nEE8jXrECUD5cjnH16Vw0Ksq9ZU+53VEqFJzfQ57X7nzbaRfM8v5ck56fjXhnim4a+u5YzLvCnaJB\n39677x3rQjkMcDjJUgnPP0xXlN7dvDMFwXZjklq+wpR5Vc+an775TAm0uWO9RfMLhjgE16XpGreK\nvh/ow1TSNXm04RAMoWQgMSeQF71l+DNAPiDW0aVjFaoQ0kpGRGuep9q908VfBd/E8D6RO0pmLQmw\na1VTGzsvzhmzjAXkevb329pG/vswnBwjywWpn/Cr9su707/iW+JvD2n6xYTPuldIgjgscM2MYbuc\netfRfgjVPhn8bPElteWWo+brdssazWerQNi5iRduFbO0fKSMEHkCvljS/wBmjxdo/iBbfTtKm1R1\n/eRG3jLMU3kBse+K0fDmj6l4L15Zo/P0jV7SVlUlSjo2cHIrlrKnLWPU6cNhptWufdVprOkfCCwW\nXTb3VpoLCzFrMZQj20rhd0DBfVSxU5x06nAz8kzateXmoXMk7tNLLIZWlfksST/jXp3xY+JWmXHg\n7RvD0N1HqOtCQXGqTxEYVtp2A44B+ZvwxXnlvCtwgK85+YAV7WV0pKm6kt2fK5lKPtPZx6bmjpls\n00OWJBz3rZtERZVYgnOBkVSsI/Jiw3BHWpxhCzbj+Fe4jxjZkvY4wN2QOnNS20qP88cik45GKy4i\nXiXdg5q9bWkYT9y3zZ6dKtMzNOKZlUs0W5SOWFXIltLkEIQrjsetVrKYRRhJAcZxmrtzpkN3gw4R\nwOCK2WpJF/pdlKCkZmX+4oySK6LR9RjIDbHtyeqOMA1naHqIkMljOfLuFGFbHX6Ukmq32huYp2W5\nt2OVLDkjPShAdTaFYeBwrtkEnvWg2Qu71PNZNndxXtizxg/LhgPStAyeaVC/MrAHPrRYCvLEZsq6\n7l968V+Lugtp9xZFMGOQyFT6HK17wIwjhW6Z9PWvPfi/pn2q1sGXt5hGPXC1lNFxPI/DepS206q5\nIBHB9cV30Ew1CFXBHmYLMS9edW8fk3o3/Kwboa3hO9gWngGR6e3pU9BrQ6NW3yEAj8eM0CMu/wB0\nj2FMsNQt9XtfMjb96Dg7hjHFTEFCRz7Gs0rFEohBIOQc15z8RvhuNUmfV9MiUXyjMsQ6TD1A/vfz\nr0SPOc9FH3TVheFJcAY656Gk4pgfOf2lnkjhBKkYDKRyDW74vsjYaDayuMs6c+oHaoPjRfWGleIN\nOlsIVF08mZTHwG9M+9bHjKweXwpZXDvvFxHuAHasOgzxzyRdagm0sU64z19qs69dNDAlpGxTfjNW\nPD9jJHLNJKu3Zzk1haxdyX1zeSrhfKQlVFc77FpHA6+Wn1Sb5m8sYUZ5we5rKunNudpB3IeR6+ld\nVp+gSaldbnYiFBuZq0tc8B/2jpf2u2VlnB+UH+MVrSqW93oJ6nCld/kQ8gHkgV13h3Rmkvoc4VQA\nWJ/QVz9nZLDqgkuHKeWQpjxzn3FdxYXKqheNMc8HuKqpNPRDhHW56H4fWxs5PNuFSUg5G7qfwrp7\n7xmssIjtIBCgGML/AFryG31SZXVmRmRcdWrptP1ZJMsIJmB78cVkpNaIbiYHjn4j6l4Z1O0jt03N\nNkkdc+nFbNiZfiF4XuL+/tLeyltiqMyTKrOT0IQncevYH9K5/wAe6Rb+IL+xnivbiwnSJkJWHJPO\nR+HXmrNnbaL4fsIltLqe4lJDz3FzIWd3xjp2A7VipO+uxTStoV5PC0drnLEox+Umq1x4dV4XfdkD\nqc1e1XxZbkqFDPk9cVkNrVxqFx8uY4gcBe5qW4pasunzXsjU0bTBZESIw34GGPaux+E2uS2HxRLr\ncQ24aykBa4PGAQePyrlnk+yWygsPMZQTgcgdhVfwnrsOm+PrSSWOzl3RtE5vRmNd2RyM89a82Dcm\n5H011SjGK3ujn/jDpupeI/GM+tYYpdzNs8p+XVABn247/SsOx03xXfeLfsNtZ3nzys6xeWZCOPUj\nnA79ua910nR49L1+20+7canH9kUi1jjBaF8ja5PYEcnHqK1/Da6onix9HsJooLq4kJtp5CRJDlix\nVmPXIUjGDkHrXbThZJM8au3OpKb7nN/C34EadolzfRavM1prU0BVYbuNXjVWUbyMHG4DkH69CMj3\nP4W/DLwr4NheK7ls73ULZluhJE21t2PlBJxkEMO3U1438RfB/jH4d3zXms2lzcotyrQXguAybioC\ntnnA+8CDjrWZ4c+IUOo31vDJZxrvdHkdZXc7wxOE5zgk/kBg1yYlpaW1O3CK1nfQ6b9pzSddl8Az\n6vqkyxWB1RYrSycqkiKVdlITrtAO0n1xntXySWDEjOcV9hfG3RtY8f2Ucl1qAXR4IGuDYQu5mkuD\nkbmTkYyMZJAAz3r4+uImgkkjZdrLwR75rkptNO2h7UrpK+qId2MDGRk9KEwrgkHgdacRlVOOQRzT\nCArKeT7/AI1oQlcsYV0GBwDx+dPiUYUEkc9qjyvlHsP/AK9TRMvz4OealM3Wj0JPLbALCrFoQo+n\n+NRlty8HApYQyuV+9nvSexq4panrGlaj9jn8H3fC+RrFq5Yj+Esua+jvipfQ2nxH8M3LMFG6LOTj\nkEV8r3sjReBYrv8Ajtbm2kH0BP8AhX0z8fNP+23Xhq9iRmYMrgjoBuzXLXjzUFfuW5cmMk11SMfR\niE+N3ieNB8rzF+nqoNbWv6kmheJIbh0MkRV0cD0O01ztjI9t8dtRZ8ATLHwD6xj/AArf+IGnz3mq\nW9vayLHJK4Qs4z8pU5rwasffafVHpwlaCa6M3dK8cfZpo77T4k1AAeR5TsI1OfunOO1eR/tLav40\n8eR6aLjw7PpdvZK0kf2aXz42bu24AEdsAivVPD1k+i6W9t9ljuYo1wuxcSbuvfqKfqseq2ItR/aA\nNuRwnlrlG64yetY4aUaVRTUbtHTWh7am4SejPjHxl4mkvfE0vlZVIm2AZ6+pqjNrC2dyk0tuLhNw\nBVmxn8qr+L9MkstanURshVyxJHWubkuzLEckk7sivuqcItq+x8bVxDgp8r1Pvu8PgH4E/Dn4ZePt\nO0Ca9i8U2pgvGa4dlhljcCUqc8MecA5HBr3nwr4V+Gvjr4d+IPijpL6pqel6aZHhkinliubbyUBL\nBA+3IDZ4xxzX50av+0fNrP7O+lfCy90lZG07VTqFpqvnEtGpVlaLZjoS2c5/Cvr3/gld8QLbXdO8\ndfDfUj5lteW4vY4W6MrDyZh+Ksn/AHyaJ4dJXZ5rxlSSbvtr+P8Akeufs6S23x/8A3Gu2up3dh4n\n0nUng+1QzlGNs+TGrKPl6lucZyDzXdePfhVqnj3Tra01zRrDUri1I2X6XUcdye2GYYz9CK+df2Ed\nWm+G37Qfjb4cX0nlGZri0VX/AOesLsVP5K/519FfE+7m0rxJPgkxy4lA/wB4ZP65H4Vl7ON0kjoh\nVqe1eu2q/q58tfFz4Nw/C/XIIbfTn06O/RpiJLkTb3yRnIJx24rhrXVn0q5SOUMEIxur0v4uXT3O\nr2sjgsAhxk+5rhLjSRfguOWXnBNfQ4eLVNHgYl3qyZsWmsW0tu5WQO3HQ5rQjvUAbDAkj1rhV0Gd\nHLR7ou/y1ZstH1W8m2B2Zc/e6ZrqRynZNqMccWM7mzxitLQ1e5lDFiee3SsbTPCc6qDNJknk5Ndp\no9ktmiglSQBg1pFMzbRsRQoYMEcnHQUsFq6zlomHGOtPgKs33+CMDjgVM0c2CsLjDDo3FbmYXGmr\ndoJHbyZ1ORInGCKsLpc1/p8iXPIHUqen+0P61Vh1xrEiG8geNG4Dg5FbZ16x05IzJIFWReho9AOL\n0XUrnQNeSwvCRE7cMTwR611GnaubW7NqxJCMdp9u36Vl+K9AtvEmm+fYThZYvmiZTyp64+lcNa+K\nLiK7hkmO2cYilXHRl4piPcRN5tsWz8wwT71y3xC/fWtiVOOX4P8AwHFafhjUotQg3MQ2R0NZvjvC\nWto3C48zP5jFYy2uWjyDXtO8iXzU+YdT71MJRJZmQcBh0Arcmt11C2ZVAPHJrCtIikM0RH+rPArK\n5oijYPJBcJJCzI4PTsw9DXc6XfQ6lF825ZVAHl4GM5rnbOy3ZV8Fjzmta0tSn7xCQwPbtSuK5rTx\nNavtwF/vDPWuZ8ReIBaW83zj5V6E8irur6wYY9rEZ5JfpivF/iN4lMdu8SSFnk5B9qYJnnvjDX/7\nW8Rea7EbJBgZ969g1B/P0fSLcEyFbYMR25zivnK9kYXySH5gWH869ot/EL32i20lmolkEIjPPIIF\nckt2UtTJ8QXttplvIig+ZMdvFVfAvhWPUprh7kFYmRg3uCKsw+Gr3U5kmvZURV5Cs3NddM0Xhvwx\nd3aKG3KERh3PtWcVcq55pe6ZHp00tnCzG3jJ3MByfSol8VGFFjS3Dqp2jPGRWtYaUt9ayT3DkLnc\nT6mub1SyEFtMEUgNkjP86h3S0KSucl4iu7O/8QtIEETFRu2nq1aumwwyzFGlcsuCVBxx3rnksRBf\nPBMgWT/WZPcVv2l7balJFFtFvdL8scvZ/rWa31Gux6fY/C7+1dOg1HRtQE6v1t7gZOe4yK567W+8\nLahILiEqOhU9PwqXwP4wuvCGuJbzgiKRwHH8PPGRXq3ijTbfxRZlZI1aZV3RyHq9bKKavEV3secy\nXcOo2iuqgEqM5Fc5e2EceScKoycdeRWlG4sZJbaZTFt4wfxrB128/cMolDKDWbWg02UNSvIzbAxK\nqtuwQByKraH/AKTMZnYbU5xWbJcedBJxk4wtaGmslvbpngsc7R3rkq6qyO7Cr3+Z9DQvr8AFpB85\n5PuO1ZvgzT11zxhA00ohjSUSPM+SiYPBbAPGcU28dpZGZ+F5Zj2A61J8PdTj0oapqE8DSiSJo413\nhQM8Z98HtURXJBtbnZKftKifTU9o+M3j+28IaTdv4X1EfaZ4cSy27q0QBwAFxnBA6exNfNFv8SNW\n/toXk+rXJvEAC3G854UhfqR602TxVdLZ6hpRjjltr0h5SY9zxlc8oc8HFVtN8M3b2M2pvZCTT1yG\nlYFVVsZC7ugYgcDvmtk7I4Zu70O0j+I/izxbYtZah4tvdSilnjhCXly8ioSSN3zE4/Cr3wp0W+sf\nHFrp96JYoVufKM0I3FMsRn3Ht3rnvhVYXLR380MNt5liwvPLuGVXkVWGUXdwxwckYzgMe1fSPwR1\n/T9O8avMttaXklxM/nx3QKRwkByGEgGEYEjDEY+ma5a7TTud2EpvRnS/FddK+HPhBpPEOmrrJnik\nhiWWRk2y5LIw4+dcknrkZ6c18Q3J8yR2xjJ3V9C/te+Pdb8e+JtP+06bLaaVYxNBb3WS6XZDHdKC\nVGCcDI56Z71877TjBOOK4YKyuz6FvTlWwxSCvPA71HLjIAPepWAG4Ywf50nBz9ea0M90KqfIeetP\nhAGQOTmkxgkHnPSnxEZZsZwAKj1NUuhNFwvX+KpEOG4/Oo42DBgcgA09RmUL69xTVmbR20O4vFa5\n+HMix7pHdFYAck4c/wD1q+xb7xJpV94K8Nzf2jZSXLWaPIhmUsvyKcEZ4OT3r5O0Rnh8E6XPEuWZ\n7iA/gVP/ALNUUc9y0ZBUDP6Vy1ZJRdNmsqTlUjXT6W/T9D3vT7u3ufGUVxC0c7F0/eJ8xx9RXQ+M\npTBr9o7LkebGcD8q8v8AgtI1v4qswTw6MPpwa9X+Jmnz6iNltII7hkDIxHAYHI/UV4MpJ1LM9Tlc\naWhrxJqcdg95G8LS3TqEt3xiFQeeR1JFTRarBfao1soWKWLLOkq5BJx09f8A61eTR6N8RbqJlj1+\n0t41+6sVmGx69TUJ+G3jq9YiTxRcsG6iG1C/qOamVOGtpIiNSTXws+bfFmuvrN9LLuALda4qSMwh\nVPJHXNagcl+x96z3dZr4Ju4x6V99GKVkuh8C5OV79ShcuVbOfut0r3P9jn4qf8Kr/aB8Ia20ojs5\nbpbK6LNhfJl/duT9A278K8LuFwXUdhnPrU2nTmIRSq2GQhhjsQa75x54tdzzISamfop+0rdv8D/2\n5NL8W24EdpqZtNU+To3OyUZ92jf86+vfjZp4vBp2q2482AkqGUcFGHmIf/Hm/KviX9rXVP8AhYv7\nP3wb+JcOHkjhFldTLydxXkH6SQzH/gdfW/wh8Wj4nfssaDqIYS3lpbi2kJPO6AkL/wCQzXj7xjPs\nz1I3hUUWeAfFN4zPZyZA+Rhg/WuEmvQsYKEDPGBXdfE5DcWzSBQ5jJyMc4ryS3ha7nwJSuOcZ4Ff\nRUFamjx8Tf2rOqstWEhSO5jB5xuAx+ddlpUcMcXy47civPY7ZI5AY3yQpJ966nS7oW0S5G5Xxjno\nK3Why2Z1qY8xlRt3HfpVlUQBGJII7YrGgPll5IWxjGR1zVhNYCALKu1jjPHvW0ZXINyNJFclSemc\n+taFpdifeJAdwA5FZltcwyKjI2DgYGau2IJdyW2lsDge9WiTXurGLULTZKuQV+Rscg1z0elGZm0/\nUoC8bArFOvVa6hJWijG5gyqKry6tb+Ucqd+SuAaCbnkutw6t8NdWS5hlefTHbnJzkZ6Gk8V6jY3l\nlDqdiVP2iQGRFGGBwa9R17SLbxNpDQzLuR19iRjvXz7qehy6Fe3GnbyyLJlGB6+9JlI7/wAD+LY4\nLlImlAIPc9K6vx7qCy6VZSI+7eZMY78LXk+ieBJDIkrTkBznGcEV1+swPp+lada+YXUO+Cx69Kze\no0S6b80SkEgZ55qjLamG+kPRZFOe/Oa1LMCK1T5QfX2qO/CNAZB8204rJ3sWmVNMhySxBJzyDWxL\nMtjGzMeSMjFZS6hFYWpkZhk8AVzOr+JGljlUNlfX2oSugGeLtdiNu4J+UAnJPevCNeupNQuc/wCs\nbcQv+16Cur8W6406mINkE9PSsnwdox1TV4S8ZkjhAdxjOTngfnUsEeb3lnLBculzG0csLFWRv4TX\nWeEpZGtHwzhyfvA44r6j/aa/ZghtvhvovivRrY/a7GzRdXSMcyK3Im46lWOD/skdlr5U8J/6HdPb\nTNh84rkuWjpZ7/7PB+8kYsTtyexrZ1SZ5fD1ja7g4OZm57DpXI+J3EN7BEpAVyCQPrXUSHz4ri3X\nlobFeh6c5NJDK+tXMdl4etoUYieZggx3zVLXNN3TJbAHjEefX1/Wq0N4mqeJPD9mDlVuFLfQc1tW\nm/VL22l6mWdiRj/bJqWNM5bxTo1qdUtbOKJTdqn7yQDnbWf4e0WzjuNUEkZkeM/ulbsAMn8etdRp\nEAu9X1vUXG51cxqx9M//AFqztKtXHiDUIZE2ux798j/A1DWo0R3OjDURdx7yz24R43J5MbDj8jXo\nHgLXZdS0YwTsPtNhgMTySPWuJ0HbHfae0hJS5tXtjk8blJx/StTwZcDTvEoDtsS4XyJAfXPBNOL5\nZamyXPBvsWvijp6W9yl7ANkUqhjx0zmvIL12uFaMtkHoR3Fe9fFSMS+FrZ9nzRExk/jXh5QRIsjA\nZVsbazm7SshRXu3K4jSNI05znJNNnfycAYJ7e1RyTK8jN94E5OO1Yur6qbZGCtmViBjHQVhGDlLQ\n6OfljYj1LWfMkFsjEKx/eNnr7V3XhzQ9F1Xw9Owuy2oWn71LJhxIpwOv6/hXkEgYPkkk8nr3ro/D\nV/8AZriK5uG/0NCFmCsN7DPQDPI9arE0+SCa6G2CnzTce5614D+HWi+NNRZdZWa1upmLi4gKKCQr\nk8EgEfc5Hoa7bw5p3hPSPDV34evTc3tvciJZxbDaZi4HlscH1PHXBzUGiWmhy6MmoahPG9u6x/Zn\n2FGiHykbfyIPPRqu6z8VPDfw38Potppb3ly1vHAt5KQzRuv3UY4/hzkHA6U6bXKjKorSdzT8K/Af\nRtO0ye7tL9bMRl5b3+00JKW2zllT+IgEYxyCaj8KR6N4LM9+ZDJBfMFDE8hZdvluVP8AD1P0/CvN\n7v44SXaTy3TSW+rxyPILYxFUG8KGDH/gOeR1Irl7/wAUa9Z+Ibdrj/QopI2jNrLCA3l4wEOR1wvy\n8cGuSqrJnXh6trJH2pq+jeG/i1os/hqeaSO+tDJa2t3bBF+YDGATkbDxnAB96+BfiB4K1T4deKb7\nQNYhEGo2bbZFDbh6jB75GK9z8NS+I7waddSXF5p9v5oWErmIOzHGOMY+7yR1wawf2soWPijRpb64\nMurmxCXaFssrBiBk5OeP/wBVebG6dj6KD5o6HgrDLEkZUU1YxuIUnoDUy5OR7U0rscDHOK111FZa\nAibyCeAAakgUK2STgiljClyM5GKVQAQP73Sod7XNUiSHGWBHAwealjZYiWzxnkGkSLlxkj5c0uxV\nJ4GPzp7GsNmup6v4ERbnwZbBj8sd/MNuem6OPnH/AAGtGWxjG4AVz/w3uC2gX0QOBHeRtj1BRx/S\numlPBHc4NeRim/a6eR7GHVqSfqbfw8cWfi3TsfKN2MH8RX0RNbQXDB5Y1kKjgn0r5p8K3Bh8S6aS\nQCJ1HPfmvpOynBkKtydoxXkYhPdHXTd7pmrZ+TbAqqLHkZyBVhJyrdfxqrIY3kQDjjBz3qr5370n\nJwvTNcMtTdLXQ/Me8vzbjai8YOT6Uvgy3j1bxJZQTE7JZQjewJqpetutpDnkiun+E0thpniKC41L\n/UEHDAfdbsTX6k9mfk8Ja3MXxJpE+g67d6fPGYpInaNlYYJ54P5Gsm1Yxsy44Bxiu6+M2vW3ibxz\nc6narsWVIy2BgZCBSf0rgW+S7YZ6gHivRi04nnS0Z9jfBLX0+JH7H3j/AME3Mjve6BJ/atmh5wmQ\nxwPYo3/fyve/+Canj+PV/BHijwdNMS8ZW7hViOF+5J+hWvj79izxdBoXxkXRb11GmeIbWbTJg5wC\nXXKf+PKo/Gu7/ZQ8RzfBv9pyTR52KRG7k0+VTxujYlR/MH8K86dP+JBeqPVjNNU5P0Ppnx3b/YtU\nvbOcKTG5Q59sjmvHNTs00i5mSEtmX5kB/h9q94+O9lJa+JZrkqUNwu9c/iD/ACrxTVbRp7qCcnPy\nkc16mGd6SaOPFxSk9CTT7QW0CmTLOy8mtFWzBGsZJ2gAkCsaXUZLNiWBdBwcdxWlompWt1v2zKvG\ndp6g11po83XoaOn6pLbPtYHaOpI4FddZC31ZF+UFtvDY4rkEmjLYSRQSOTiun8MyDePLYHHbtVRe\npMtrmk1nBYxgPOYwuMnoK0rO6tVjVjexF8/Lg81ZvNOh1K2cSAZI7CuVuvh4ZWAgneHdznNb27GK\nempuawmrXQ/cHyU/vBgPpWDceHddlcMLssce3vWvpvh3WdLgVW1YPGG+4ybuPqa6FLaFYgZ5ljfo\nQrYzRYpHK6PoPiW2uSxvkZFH3WOQRWb4t8PCHWobqYKxZMFAehB5I9q9MtbaIKCreY/IGTXDeOLd\nv7VgUsSdpJ9AfSk9hJamTHepCAoUYHNW9atzdrp8pHyDdjHPHFY81tIqjHJJzTdR1mWytLRC+NpY\n9OnT/CsrW0KS0NGS6W3hA54PU1yms+JXthIInxgnJPT2qhqeryX64LbcnGc9axLuZljxwSTzzx9a\nNFsGwlzrk928jzSGXBwB0xzWdqWrmK1cE/MM4pFZYVcnGSe1c9rmorOjqhLc4/Ws02hrUxbudpmc\nsScjsMV7b+zzoUC+MfCNrOu6S/v0LIRnIXLYPtha8k8KaXJq+rxhlxDCvmMcdcdK9y+Alytz+094\nN01TlLaG5uGGM/N5Lgf1/Ok/hYz72k0+2mWaxuYI57SRWgkhcZR42B+UjuCDivzJ/aT+DbfBr4q3\nOnW779MmRbuwkbO427EgKT3ZSCvvjPev1AvFJumI7oG/Ef5FfPn7dnwxk8a/Cuz8WWEe7UfDbl5d\nq5L2r4Eg4/ukK3sNxrjZa1Pzy1yRbya15G9XH4e9b/hy7J8WXttKT++twi899tclfTKnkXOflVsj\nj9K3bC5VfEVleZGGxkihFDtChWHxvpy7QrJI3B9cGur0G3Nlpy3rL8lrHLKSfXnH61ymp3Eel+P7\nZgchXzxziux8X6vDB4Va1t0MZmO1tv8AEuc0khmd4QiM3hG4m2DzJJ/vY6881peJbKCDxhokUahJ\nXtleTA5bAwM1t+E/DzDw1YQ42+a28DHauW13V/N+LAj3CQW8SxKQf8+lU7WEcx4gLaTp+l3AwDba\nhIG9wTzUfiG7/sjxMXUkrIVlTHuAab44vlu9HkiGMpqLc+mc0yNBqp0ia4PzjTwCT3Klk/pXO47G\nsJuNz0rx7qUN98OIrqPB8wqeP4T/AJFfP0t55iyNwT0Brsdc8W+V8P4NLdv3puCOT0Ga8p1PVdhe\nKHIVeCQelS1zy0Kj8Ni1fakLfIUZHseprmrqWSWeRmP3j+VTCYuwbOW7g06d4fKB2/NkZNdtOHsz\nNu5XgfawDIHJ4xV3SrZLm/KPIsUJwrFjjH49vrVNNigyAcr05rd8G6FHrep29nJcLC00iqM/xE9v\navNxtR/Cj28up2TnI7Pxh46iHhPTdLtl2wW7gxqkgd8KMfMemfcCvMY7m91ydNNhgDPMwRVXO9nL\nHAxnk84/KvUfiF8Pr3wj4wt7S3QHT2tohJLEPMRVbqG688ciuaPw2m1XUDHZQTPAHHJQ+ZgjqfQZ\n7ntWNFOUbs48U/fdjATXdYu5bx21G4nuL2NUupJZ2LyKjDarE5PBRCB7CvTfBmg694iv9M1CTUNR\ntpUWSWS4S5HmtIoIDjcQR1APcDnmum8OfCLSPD1w9xrbvpFrbzRPI13F+7cbSfLVj3JznH+Geg07\n40eCba+sbC3sDqKtdCWR7PKb/lAXqBg8kE806yUY3ZphE6krJHpHg/xPp/hiyzr0rNpuYbu7up4V\nnlDJIfmMcv387uoIB3Hceor5c+LnjP8A4T3x/q2rJctNaPMVty6iMeWOFwg4XgDj+dWvjj451LXv\nFFzpz3qT2Nl8kPlbcY7gleCeBnHGRXl4kYNjOTkZrzLKXvH0il7P3bF4rh2BcDg9KVghAIfJ71S3\nkOOODkVJtGV59DxTLjJFqNYkYZk60sDRFsb/AJfp+tVwm2RTnK96SNRhiowcmla5albpsaKCNHbD\nEkg4p+5CBt4IweaqRjbtJycntT1zheMHHf60mbRa2O++HN55NvqcBY5ZopABz0JH9a6y4nw4zz17\nVwnw/lEd/docfPGo69cMK7LV/FVzol/A1vHEwtxwGUZOe+a8+tS553PShVcKSsaGjRyprFrOI5Ai\nzKxbaeBmvoW21+0S4CK5aUgDYqkkVwXgrWk8WaFaw38ZRHuiu+FxkHaPvdypIX2zXQXMkWizGSEm\naBFAV24LHPtXJUwibSuRTxcrbHZ6hrlpZeV58xgJXcA4IJ564rQs9G1XUnhaDTrhobn/AFUjRkJK\nM4ypPUZI5qDXdI8NtHZa/NAl1fXMUjzW85k3BhISVfnHzcj5cYr0G78cxah8E7WTwfdGw8TeF0ed\noHhjlDRSudwCSKd4AAzgccVpQyyFWbpuWpz4jM50KaqKP56f0z8cri7EsW0Ek5zXZfDzw3N4ovUs\nreRUkC7yz5wB/k1w+ladc6zei3tY2llPRR2FfQXwz8E2/gya0uJ5fMvbgFZCG+VRj7o/xr7OEebc\n+BlLlV4nOfFz4RXngPQtI1mS6juoL95IRszuRk2k5z7NXlVzwySdt20/SvsP9oO+s9Q/Zw02EPJ9\nqh17KR5BRYzCwJ9ck479q+Pbhd8BHsDXbGPLpY4ubmu2X9B1mbw7r2l6pbN5c1pOkyN6MrAg/oK+\nivj/AHUel/GLw1450v8AdWfiKyt9SjZez4AYfUcV8xYMluWPUEGvddZ1P/hMv2ZtAvMhtQ8K6k1m\n5HXyJcshP/AuKxkkpxl8jrpTvCUfmfoH8QtRh8afDjQfEsTiV5YVL/8AAl5/UH868Rm2kouSQDjp\n0zXY/sweLIfHPwCv9JuX3z2Kkx8ZbaRkfrn864+eLy5irHC7uRjpRg3yqVPszqxXvxVRdUMlsRMj\nKx49DVCTw2Gx9nBUk/fHFa1qQWIBJA5/Ctq2RJNpEZO3kg969Jo8a5jad4V8mJpZZ5PNzhWJ6fX1\nrqPD97DZsI2lVpAMfvBisnUdTe33q0ZSPOAeuaithFdDcME8jBrSMbEN3PRbG/WNNxn69FBGK0f7\nQilEZaQFiSK8pXQbm5ZtlxgFhwCRj6Vpp4U1SzRJY2FyA27BY5Fa9DOyPT47qOaIxq6qw4BPSgaN\nA8jSzs8rjGMcCuItNTvYiUkhdD24xzW/p99e7t9zL5UeB949fagEzp7UR2ozHHtIPVua5rxWq3MC\nTtgSmTKn1B4/wrXu9Vjt7BWkbDSDvWTcKNU0e5v2jK7f9WP9kHr+NHQaORuo5I34+U1zPi+5eK3t\ndwwMvjHfpXV6jqaW8Zd9rd9hPTrXmPjPxFHcPEFI2hmB9uRUPTUaZn3Enn4Jyu3JwDxjFU5bl/Lb\n+IZHHSqUerrK5RZN3OPaqmr3q2y7WdlbGetZ3DyK2pau4ZtrBccAZ5xWZdzZVRwMDqDyaz5ftV2z\nyiI7PU9OtVzPJCz70JI5zU+bGd58OpAL2dd2cqoHr1NeifszTNaftd6a7AkT206gnt+5YcfiK80+\nFszSajPsTKtGcknjP1r0r4SD+yf2iPAV+uVE0s8LnHXKNgfqaTXusVz9HZE8y4DdcLirMNjb6jYX\nen3sK3NpcRtDLDIMq6MCGUj0IOPxpsMfm471Ytx5VwOoBHNcLNUfkr+0X8GL74L/ABL1Pw7IHfSZ\nybnTblh/rIWJ259xyp9wfauJ0e0EumFTzJG449Oa/UT9q/4HQ/GnwHDFbhYtcsGaSwnJx8xHMbH+\n62APY4Pbn8w7S3udB1+40rUbd7O7gkMFxBKpDKykg8eoxRHXUdxni6BbXxLp1zg5njVj710GqyLc\ntZ275OcZFJ8RdIKafoeoRsCqt5YcHPfjNVTfme7gcqDtxk46U7DR7EdUtbDQYdhbzIY8YH868S8M\nwyap46vb2Q5VQz5PXArptY1uSPTpCxz8uOP6VzHg+UWllruoSMQRCwGe5NG4FPR7eLWV1H7SrG2e\nctuQjcjAnDc/55qlcXULRySR7ktbNDFGzHBbBJJ/MmmXeoPoWhokZxcXJ3YHua4jXdfSOAWkJJCj\nLL6n3qEr6AjP1zUTNcMwcvk/Ih7ZzzWM0JZTj72STuPSnmUO/muTvI4PpTWGfmXILcnNdNOmoK/U\nOboUQ7KQMc+1TRur7o3wAehHUH1qO4iwMqfrjvVVZCGJwTnjJrR66CTsbI0pmtg6newJJUDgirvh\n7VzoOu2OopGs7W0qSiN/utg9DWdpmqNbMMnjsT2rSFglzd7kZfJkAYqpwffFeDjIOD5z6PL6iqL2\nTPYNI+MN/wCIdH1G0vdLtWtp5cxuchkY4+6c+3evMdT+J3izR3udMt7ubSlU7Z1hG1mxwQ3r2rVi\n8R6RZwLCbS7YRQsIxGVXZLg7GOQQwBwSMDI7iuQtheXP9oywwXEm6ImcxoSAhYct6DOK46NSevY7\nsVQpWVlqSX3jrxB4ksjZ32s3d9bNN55t5pCy+Z03Aetb/gm18ReB/HdvMmmxR6lZuJGs9TiHlkAZ\nwyNjII7dweKytKs7b/hG7svP5V6kyGK0/s4N5i4O4+dnKkHbxjnJ5GOb2q69PqU1lcfZ5hdQRJC7\nyTF9wQbVxnoAABjsBV1rvQywsEtdtT0D4+/CTxF4fFp4qv7HS4rbWEE2dDnjlt1zz/yzZgnfg+nt\nXioCqzZ68dO/Ne7+H/i7qtl4VvdButE0zU7KZR5P2t5mNnnOTGFcDJySdwP+PmGseGoTqU7RSCBH\nw3kqpwufTNcUJcvuyPXnSc/eWpy5yHHr6UqjCHcB0HFbb+G4iw3XJBDdQtSr4btthP2hyDxwKtzS\nMlRmzDMhIAI4JHFLak7XGM/Nng+9dIPDdogIMsnOO4qzB4b0uHfuknOeTgjioc0jZUJvW5zHmsqI\nSejDipUdcLyMgmuvt9D8PtJGtxJcCLcAzAjIGa6i08OfDoRu8lzqEjqd21WUFj6DNZyqpbJnVRws\npN3kl6s43wPcCLWWIwR5R4P512fioK97HL5SruHbpXZ+AtM+FBYT3Wk+ILedVIBmu4gkg9RtU16T\np83wNJil1HTtUmcYKpLcsyn8VC1zTr2bfKzvjhuakveXXr+j1PMPh3qT2CabNKoNrDenIQhX55zn\nHI4HXI/WvW9Q0ZZ9ZvLaKT96JfLWN8IzHee2cDn3ru7H4xfAvSbIW0XhezMKjhWtC5zjrkmm3fxc\n+EmoKWt/CsbnG0E/IMewLjNc7rTk78jORULO0Xp8v8zIttCm1HRNRZSsc2mFJLmN2O8pIThh2wCQ\nD/vDrR4L1e60HxCuoptkjggx5bsWSVcHMbY7EZFR3nx+8FaK7SWXge0vWVQB508eMDpwSa4nVf20\ndXtZni0f4a+HraEDG6dtxx/wEj+dbU3OT5lH8bEVYxprlm73/p9WfHnwR1bTLLU57O8jCXFyP3dx\ntLYx1Xjp9a920DVtDPiO3028eXaA6+b5R2rlGAOfYkV8raPqNxoV0bixujBNjaHCjOK6Cw+Iuv2d\n/Ddw6vILmJg6OUU4b1wRg/jX1EpXg4o+FpxcZpvZH0J8aNWsIvh3qegWcq38cV2s73ZXZhhjhN2G\nwQ3I9RXy75RYhex3LXV6n4k8QeOZ7ibVtWgYSytNJ/qYVLHqQq4A6dAMVJpWkDSZRJFqunsx6iYw\nyD8myKVLFOlDlm7s65YGWIm5wVonCWbEs8Z5BU16t8FLxdQ8P+N/C9xMsaalpbywrJwGnhPmIPqR\nuAqCy8PQSxtI/iHToiOq+bAP0rQgtYNLYvD4qtImAwTHNHkE8dhSnjYyVkv6+46KWVSi7uX5f5nq\n/wCx18To/BkWow3lxDbxywMgFzIEViDkDJ/GvadU+M8niC2XSNuhpBIQP9DsohOcf9NAgP6818k2\nV95L5h8cxWbsPmIuduevcLXQ6BrGzWbFpPHUd6RKv7j7VIwl5xtxs5/E1nHERnVUnHt3/wAjaWB9\nnRlHn2T6r/M+hEkKthWHPGPWuk0GUCXLEleh4/OuWtlJXJO1yeBWrYsUX5j0HXPvX058edpd2Nnf\nWrRyQqeeDWXD4Nhj3bJiEPOzNaNpN59kpVhg4GRStbSGPiRs+xrX0MyW10aG0UgYyAOlbFlI0SbX\n+7gYOeK5mf7TCF2kk4PzdcUksl9LCFViDjkdKXMKxv32tWVkXOFY4wAB3qvoslxq/wDp10nlW6HK\nI3fHtWfpGitNLvuGEg7qea2da1K30vTgeFK4AQDGapai02MnVZ5dTuEt2cIrsAEHBAz3rrNQKWui\nzQx4G2AgAey1yvg6yuLqeXVLz5M52A9q3TdLP9pHJUIRmiwHztqfiS/1uV0gBxuwAO45qte+CL+7\nitpLhiiktwM88ivV9C8B29igkOOcNkjmsD4m+ModKigsbFPtF5hsInReByal6Akef3emWPhewaeR\ny8w+6ueQea5ZdPn12R7mf5YdwwueorodO8M3WrXAu9RlMkm7OB0X8K2JtOitLd0KEbc1k9y1c5u6\ngigs0tkUIuOuKyJrOOVZCT0FdBfQFYwwG4txn0rBvYyrCIcE8cUJgdf4Xtk07SrQIm1pN0owcZNd\nd4Ou/svjvwXds2x4NWiQt6q+V/qK5tB9n1TR7XI+WHkdulbNqoTUtOm3Ffst9DMTjpskBP54pS8h\no/S+xmL20bDPIFXow5ILetYfhu7W40uzkVw6ugYEcggit+LPBPeuFloNTi8/TJ1AyyjcPqOa+Rf2\ntP2ZT4/sX8a+FrcjxLZpvu7SFfmvox/EPWRR2/iAx1Az9hqgcFT0Iwa56xUwyuhOdjlfyNKOzGfk\nBe6vJe+Hri0kmBeNg5hbqrDuBVa1lkZ0f+HAJxX2F+2t+y5IlzN8RPBlgcPl9YsbVeh5JnVR2P8A\nFjvz3JHxlFqDWhdXTyznOCOPy7U0M1b6+L6a6Z3McbSDyKjvbaSw0WOxYmKW5xJL7IOmazU8QQQX\niz3NuGhjOQu7g1zXjXxpN4iaQR5tosnOOpH19KahKWwm0tzH8WeJWurxzF8whGyMDsPWuatz5pdn\nBMhPOajulKHcCSBjnPWo/MxgqTtzkHPNdVOmo7kt9gkQoxODg8c80btyOCTjNSsPOHzHHAP1qvON\ngwBz6/nVPR6DWq1Ek4GCeg9Kqzwlo8xnnPSrDFSOeppo+QNgcVNhlEMUIHf3re0G7kM1usbKGMgD\nF0LgA8dBzWPcxAKZI+3XiixvprGRpYJngmRcrJGSrKeeQR0rirR54NM6cPUdKopI9Ju7S4kVXe5j\n2EYHl2LgfyrKkEMCnGpNGWGCBavyKxn8X65cxgSaxeSoRwHmY/1qkL66LgNPIxHOSx4r59QaPrPb\nXW51VrqjWVjc2ceu3CW05UyQpattcg8ZBPaqMlzaEHOozso54tT/AI1iLdzOSTK+exzSG4lJYGRj\n9T1FUl3I52tjp7PxI1kjxQateRRyjawjg6j3+amSXljIC0l7fO54JNuM/wDoVc0GZzkuQR0waesx\nKnMjMDxyaXs0Wq0u5t7rEnH2u+Ix/wA8QP8A2ajzLNjgzX5T/cA/rWSoBP8Arnzjoc04xlQ377fn\nnG6jlKU2binTX483Uyo7gL/jVqGLTTu3LqoHYllGa5yG0MkbbTk5yfmqaKxLnaxcgDjB6GlyoIuW\n6NpILEkYt9TZWPB81AP5VOmmW0+dlhqZ9zcJ/wDE1ijS5I4xl3JPTHQVE1tcRqWE74B561XIlogc\nnY6uys7Tbg2t3kZAzdr/AIVu6ZokF2gzZSKinBLXyjH4mvOYrK5dwuMhsdO3er66fMpGVPuM1m6d\n+ppCXc9UXwhaKF3wQ7mPHm6mijH51r6T8NrK6OVm0lMcbZNZVfw615NBYu8BbaQ2MDPYVq2GlzuF\nAbkkdD0qOTXcvnTPWrr4QWNmjP8A2r4XaM44XWpHYHrk4Ws2f4e2KTYjudDlYjIC3srA/wAq4O80\nq7jlKCYZHfHWn2elTy5HmOz549PrSUWtLiVmj5zc+WAD344pVbGcZ+lZ3mnOOnfOaeJpOSDjFevy\nnyqn1L4ZgcA59vSpEcnPrWb5kinmnm4bJxznvT5dA9pY0Mlcnv0zSA43D1NUVmdh/SnJI3OD83qR\nTSt1D2l9S+pznPDDpmrumXTQ3cMkbEPG4YEHHQ1iedL93Off8asW8ro/BG4mqUepDmmfoFod01zp\nNvLIcu8Stn1yBW1Y55JX5CeSOua4zwHcfbvCmkTFiRJbRkn/AICK7XTgFjYlcrnj3r3I7anm3Oj0\n2+aG3CgArnj2Na9pclgG43E44rm7QlwuMr7Vq27BZm52jGSOgrWLM2jYZTLtJbnkDFPhjjRyNymQ\nehrLe/QEBNpYEjinRSqku45355x0FUJbHQW5WFwQSOOawLm1l1XVy5JaJT8oPTNavnLHas+QzFT1\nNVtGiMcTXLn73IJ/wp2Ead9dLZaekEYAkbrUCfu9MlJJVmU8ioYoDqM7SM3EZyB7VoXTLJasmeMY\nwKAseb3fia48R30ek6OxUYHn3J6IPQe9VvEXhKy0a2tSDvmJbdIxyzfd71x39va94Va7i060Qq0z\nL5rIS2NxArj/ABJ8QfFd15cdwEGGYglfpUMpWR6SJ4oNwJAOKryXVs2fmVvxrxmbxvqjMVLHzD/D\n6VNYw+ItadTGJVTPLDjNZ2ch3sel6nFbSYaI/Lj8jXPS2qXuv6fbx/KN25z7ClsNG1PTLG5ubx/3\nKITtbsfWofCVyYra/wBZnPCqY4ie59qS3GbZnW58aYBJWFQq46Vv2z5vbxCSxDd+grkPAcL32oz3\ncmSC3U9a6yyQjU5Wc8SE9vQ015i9D7l/Z01qTV/hrpRmffLAHgJ9lcgfpivXoeMDqPWvBP2XLpX8\nFSRZ4S6kAwODwp/DrXvNqSVwelcMlZ2LRej6jmsS8TyNVnA6OA/+fyrbi+ZgM1na7EEuoX45BU/z\nrOL1sMt2jeZCARlSMEGvMvE37Lnwy8U6g19e+FbVblyWdrZmhDH1KqQP0r0fTnyMVfK4JzxRezGf\nB/7d37NPhbwj8HbLX/CeiRabJpl6qXRhyTJDICAzEkk4cIB/vmvzyaMvCQx+XGK/cz4u+CIfiL8N\nvEnhuVQ/9o2UkMe4dJMZQ/gwU1+IWqWU2m6hcWcqGOSGQoVYdCDggiu6g+aLRjPRnMvau8yIuXJ4\n29M1ACCzwsMYyR7eta0zfZLqOfP3HDFfXBqPxVbRQ3y3Np/qJhvU+gPOPw6fhVXs+U0WpmQcMQxH\nAx60ydAqnnPHWlki3IHxg9cCkILoGyRxS8x3KnHGenvSCPe5IO1almiIbC9O1Qsu3r0FRcojEhVm\nBA54xiqsmY/MDHGAeenPNW7nA2uo+Vx1qnqTDyk5wWwpI9q56mkS4K7RcWa2jRAZVYgAcGl+0w5H\n74EHjNZfynjeTjrTeNgPmNwenevG5Lnvqr3RsLdwA8TKW78U5b23YckH8axwwUHEjflShlZgC746\ncUlAr2tkbAuouSOgNOW5iABDA881X0bY19GhYupOCD/Ouiuobe0uWUQbgw4VRyKTRvGSa5noZaXc\nIlIMqjI7jpT11C2BADDI4OBXQ6b4fluoGkNi7ooyX25x7nHSq+oaLBBDefuNjbFKk+ueamNpNobn\nyw51qUItTsgnzMAVPHFWk1aywSJQrY5ODzWJdqhtEJVkI4JA681ks3JImYYOTxTdNJmf1hvWx2i6\n/bL1m3Y427Tg1ROrwM75kBU9ODXML8v/AC3PJ4zSbJDnE+OcDmiyXUHVk76HSxa3BFuXzWzn72TV\nqLxJDE6jzcnHJZT0zXHmKb/noufrTzFcgctx67qOVb3EpztsdoPFMIf5ZWIPYA4NaVp4wtYwGMzA\ng9ArV52iXQB+bP41Mou8YJYn2NJwXcv20ux6cPHGlSSsTJIX/vbWqRvHViq/IrnH91Gya8yjW4wc\nLyeee9W4mu43UqGDDoT2qY00upf1iTWqPPjyMgH2pV7gjqeKEO0Y5BHelZiwJ75ruPBVnsLxvPzf\nTilEZR8E8imrg5OM+1OfdycihabCWo9EwRk08gdmx2qGI5LADmpASPoaEtRkoGGx15qWNcnOcKD+\nFQLwcknFSKwGeCR6Va0FofaPwM1RdV+H+lvu3GFDCwHGNpI/livTbWQMGjIwp6c186/sp66ZrHV9\nLkYkwsLhM+h4P8hX0RaON2QfmPTvXs0XeKOCorSaNe3GxByN3c560t5qAiDYIwR271nSal5ETKhG\n7nk+tQ2dnLqFyWYkqeuT0roS6syNfT7kzpnbkA5yK1G80pkfeB6/jUVrZrZ2rBVC/Sr0LyTR85Cr\n7danR7CsXLGUS2+xskA8k9DVqadEt2LOscaHJJNcR4o8XWvh+Ly2fe2D8o61y2lQeIfGrMfMa204\nnBZzgGrVxWO6uvGK6hOLPTMMo4dlPWugs0mttPkMp+YDJOa5nw9a6b4dU28B+1XIxulPIzWl4i1F\n7PQbiZ2AMgwBSQyppekQXmnJJIqu0u5iSOuSa5Hx14D02RIMytHI+4Z9MYroLO6votLtY1iZGCg9\nfWsbxDpV5qLwyTTeXENwyeKm4kcdpXhjw/o7KzobmY9XcZP5VvRXcNvaPIiLbW6D78nHArndY8Q2\nXhpDHCourkcc9BXN2usXPiS9FxqM221UkrbR8ZI9qm7Ww0dZqNxN4yhMETmDSlPzybcGTH9K5nxL\ndwRiGwt9sFogwP8Aa+tb8k2sajGLbTbM2tqQQXYYGKs6X8OYbUG6v5VuZF5w3TNS0NeYzwLaCy0x\n58YX+HqM+9aMcnlzW0hIV9x4B6Z/yKkvJzsEUeFiVcDaKx7id5ZViUj92RITjGAD/WhvsB9sfsuz\nofCUmMljcuWJ7nC19CWhBQDGK+Vf2SfEMV9oWqW6NmSG4WU5PUMMf+y19TWEoKDJPSuOpuaI0YwB\nz0NVdaTdaI/Uow/wqypGeuabfRmaylUcnbxXOtGMoWZKnrWoCXXsDWNYSfID1HrWpCxxg5qmgHOv\nHXNfkV+3J8OP+Fc/HfW/KXFjqxGqW+OBiQnePwcOPpiv10J4YV8T/wDBTTwEupeCvDviyGLfNYXL\nWM7AfwSAsmfYFG/76rooO0rEu1j83Ltcx54KjHNU5Qbm0dD83lZI57Gra5dZOQGOOPSs9wYZScbs\nHJ9D6111I21Jg7lCLKylTzxTnZIjlclTzTr2LEpdTlWG4VDGWkXjLdqlaoYyT5txz7gUSxBo9yD6\nipJY9kfLBeME1HbsI3IJwBkHNQ11NEV3UyQMCOF5AqjcAC1Y4DbGDAH61rzFY5TxlKzbxNgmXqCh\nIP4Vz1FeLuaU3aSZXWVMcQAmjzyD/qQO2PaqinKAhyMjJ5pVJ6b8/WvH7s9lNtWLfn7eGgUjNPMy\nlcCADNUw4UH5jk+9OjcnPznA9+aXQqLvobOiSL9uhKxbSDwa6GVZ7q4gt7OI+fOu8Y5z15/DFcrp\nLiPUIjvONwGCa7PUdQa2tLEQSmzu7Tf9nu4xyyMSSj/Qk4Po2D0FYy3SNHFunddD6Y/ZPudK+HXx\nQ0Sw+Ir24s7+2S+tJIJUnyGJC7tpIAyCGRsHAIIziub/AGy77wTc/ES+n8D6eunaXdWiTvbKu1Um\nJO8KvYZHQcc8YGBXz5pWsyWuqnUbu6M90uNmw8+2MdKl8Qa9d63OZ7x2L+XtUHqFqacWp2RnGLs5\neVjLW4iewQO7bgSMY4HWqW62ZjtlTPcEUsBdrU7XC/MeGqsIZAWwUbJ5yK1mjppP3UiyscDZ/eRn\nBpwtIizD5DzVIwSknMKEcZxTmh55gYjpwaz5GzVSSWxYGlpn7o+YnoanOkKy8g46dazRHgnMUg7n\nDU87dw/1y8dATSUX3BSjbYunS1JPUfQ05bAqoIZiazlkQn5ppgwyKnUqMn7TL6Yp2fcE431RdSzJ\nDYdgR0FWZ/NvpxIwCEgKVjG0YHtWYshBKi6ZcHjNWiqpbJKuph5GO0w7CCvvnvTSY3KC0scBgbjz\n14PFMKYbG7ofSnkA4HrjrTygDE9feu1aI+fv2IgPQ8jtjpSqFwRuxzgZpxjznBzmnbARyBtHWi4J\nMaigA4YA56U9ApXaWHPvQEwf88UqLtz8o45ovpcauPVlztz075p6AEkhhz2zUQiyOgGT1p6KvQjd\nTTFbQ9V/Z38QNo3xDtY2f5LxGgYBuPUfqP1r7DSVRHuzsIHNfA/gmdrDxTpU8YKtHcRnr/tCvuhG\nM0Ce4HevWwr91o4qqs7kmnwNf3bZyUJzuz1rsLWNbNcKy4xyPpWPpuyGNfmxkYORV+I/aWKDOzBy\nQcmut3MVYuQzNesFzhfvcdKyfFXjKPSLY2dlma9f5VRBk596brmvLplqIrXLTMuBx0NM8F+Glst+\nr6id9zIdwLjOB7UJW1Fcq+H/AAGFYav4ikE1w3zLDnIXvW6pvdTCQWSiCzzg4GBip3La5cOFk2Wk\nZG58YzWvYvDHAY4SFgjJwfU1dxBpfh+DTAACHY9ST1rN8RxLql1BaFv3MWGfA/z71tS34S1kuAcI\nPu59ay0+SNdx/fykM5PX6VN0JDbG+d/3MVnM23je+AuR7/hWF4qsdT1RI45JY7aLJx5ZLEdO5rqt\nMci3RmPBZufxNV9enRYk+YDk8H1pW0Gjz2D4aWEjGa5LXLMOS7dfyrUttCsNOJ8m0j3qfl46Vd/t\nWKJQRknsAM5FZVzqs0zusERyOcnn+VTZj2L9xMyqwIAJ4wKwtTvtiYaUJnvmiWy1W/dnab7OvTOM\ncVB/wilu0m+8ne5dDkANhaVtCtCo11aSQv5VwJCi5bBzTNNTzLZ3I3GVwnPpVjVtJhg0nFnBHD84\nBCj5mHTrSFDDcadbrwWOSPxoSsTa52X7NPjB/AnxUGlXUuyzvx9nUseDub5PybA+hr9DtLlyic9h\nX5f+LrI2E9pqltlJ7SRZQRwSM8j+R/Cv0M+DnjSPxz4B0XWFkDPPAolA7SDhv1H61zVV1LWx6VHI\nDzjmpwdyEYxmqULetWos9a5LFGTAnk3EkfTBrQibDc9TVS/UxXW/HDDrT8lQDnPHAq9wLjZINeVf\ntMeDT47+Bvi/SFi824Fk1zCCOfMi/eDHuduPxr06G43feGD71HfRLPFIjDerDaykcEHginF2Ynsf\ngrcweTduGPfB4qrdQqhZsDA9q7743eEm8CfE7xRoLLtFjfzRRk903nYf++cVwlzILmPjnHHvXqP3\nomKZDaW41S2kjTmaIb1Hcr3FYYl8h3Q8bWrStL2TSNSiuogpMfLKe47g/Wrnimwhuov7SscGKUBn\nT+79a5b8rsbLuYslwZFC7dwzxVWUATHnvQjs23Bxz196ZIPmbI6nrmtEwLAAkTYSc44PqKpXa/uH\nUHLIp5Hepw21wwzxTbgBwzjle49q557WNYvU56J1KqGU5x9Kd5kQyNpyT3qUIY5HTK5Q7eR+VGzj\nPy/XFeI9Nz2Iu6uRh4l/hyP5VKskCnG1h7inovAULGSKk3H5h5SEGpuax0CC5ihdXCZZGzXUPrqS\nxpmBm+XuwxXM8E58tcemavWq+ZFF0I6VDsdlJu7SL8V5GL/dHaknOSq4zS6reSzSqBbMrYxtOPek\ntXWG9yPvHrVu9vT9qiO0MwA5P1qFKzNvZ88Gr9TFWHZDNHMdjowBA7GqrWiszbZ8H61Y1eZ/7Su0\nQZYsG+UZqlGkxYgwvnqMrWvmcKtF8tiZbSbJxICMj8auQZO5ZWx2XHNVlWbB/dMBx0BpyCTbysg7\n/dq4TS3JqQk17hcktpLe3MivvOOAe9VDdXIIxGMtzila4fhCXA9GWmh2ZgCR15ytKcoy2ClGcVZs\nBcyfMGiG7PpTxdOoI8sY9cUkTN825lyOMdKeJBgZ256D5qwuludUU31EF0JcExHPTpmpfMRVYiPn\n6U0Sbj047ENUpdd4z0+tHN1DkujhVjLcggCnNkLt60232kMPunHWrkCKJUDgEZAP513X6M8JIqHI\nXsRnoKdn5iMjt1qS7tTHM2xTt68VXVTjkd6aWgapj1b5OR83apAD1IyOKijTB7/WnK3Azn6UvQd7\nbkqPg/KPlzyDT1JA4GMdM1CGIzz/APXpRyD83I7VWlybs2dBl2arZyAn5ZUOfxFfdGjuZYI8EkBQ\nST718FWcnluj5PysCa+6PBd4l/oljMpP72FG4HXIBr0cI9zkratHZQgKqMQM4IAFWvOEKbYgcngg\nVlzXSxRDaSD05qxpk7SSASfOzfxHtXet7nOTWehpdXBkm5CHJLVqgy6kzRJiO1hxlv7wqdnVrdoy\nwjUAFj0zWVdamk/7u2bCHggd6erJRpW032u4+y24C268MV4yavzhpAIIlCRr1CiqeleXZFSvG7uK\nmDv5gxwM45oGMvQJriEMNtvb4bP95qkjf7RNuYFWzwama3+0KzNyQcACo5QbGCSTO75eFxUgU9Pj\nma2CRLNIdzHJbavU4xUWq6RIRGZ3QMSTlCSf1qzo9809jaMjADYASOnv/Wo9WmL+R1PJx7U9UhGQ\nLGAlncGTb1JNSuownlxj5eDjj6UKp3MCM5xg/pTpCUBBYKMAHPWjoMq3ChlIJPqB/Oq4t/NB25CE\nde9TTSEh9o/H2qOK4EIdSwPzcAnHapu7F6BLbf8AEudMDPDc+vtXI6NI2peIJ7jnZCPLXPrmugu9\nTIQrvGPfioLC3it5maNQElbcccZ96RKNS+s0v7WRJB8n3eme2K9m/Yl8UzWtnrvha4k3GymE8QP9\n0/Kf5L+dePCdXU4yB6mui+AmqN4f+P8AYoHPk6ravFIcYDHaSP1QVnNXQ1ofetleBwA3rwc1piQj\n7tci1w1uFkT5gvJAroLC7FzCrqQVIrjasWXruIT27D+IDINVLctKgycAVaSTqCM5qq6/Zrk44Rua\nSAjkVonznNWklDxEZzxRNEJYtwPNUon8ttpOaAPzP/4KP+BP7B+MMOvRR7YdaskkLdmlj+Rv/HRG\nfxr5Ct5FE7bwVyPvdq/T3/go14GHiT4U2WtxRFrjRroOxAyRFINrf+PCOvzAuY1hc5zknv0rvptO\nKMmtStqEYMhYHORx9afoeprYu8UoDwyfKynoD/hUrFZEXkErwDjisyYYLHbgE81M4mkXck1zS/sE\nplhObdz0H8NZTEjqefSuw0SWLVNLlsphmVFynqy+lctqthJpl40BZHIAIZWB4IyOnfnpWVOVnyvc\nbWlyANuQ+ppvKp8xzzimb/4Tn1NBB2nnjsf8/SreqBGffIsd9nP3kHT2/wD10L3Bdge2KldS89uw\nAdsFcGrH2c9Wg/KvCqu02e1h4twRVtyB/GePXuKlwVcjfnPOKeLYfNugYDrT/KjBJCnHHWstDpsy\nGTJOQV+lLHdmBQCdyk4OKfNBEckK/PUDtVWHbtZR93PQ+tKyKi5Jl27u2guEPVTjpRNqTJ5Rcfe4\nB9Ko3chkKhhk4GBTp1LBcg4HrSUe5cpvWwusvtvdykhmVSSK0C2+JGVnQleCG61Q8RXS3DWYWJI9\nkIQlM/Pgnk+9WIZEa1TEnO3pVX0MI/G7i75oxxNKCe26hJLkEH7TNz/tUzbnJEv0zT0QlshwcDjN\nTc3S8yQ3N6VbdczEAD+KmC7uzH/x8yL2ANOSNgD84JPfHFL9n3LyRk+lC0B3a0I1ku2DHzc49RSF\n7neB5vJ7bR/hUqxEAjgj6mnNGcggDPfml5E2ZHHczqCN6kjjBAp63c7nJ2ZB4+UUJGyJnZnJx1qQ\nIfMwflH1qbq+xaTtucXCSZAMcGrcY/eBhkDOTVWJlDYzliOQasA4JOeRziu2+p5KtYuCRRJKeSNv\n5VSO0tnv61b4y5BGSh4qmF2yZx9M1JpYljYKPUjvTeGkIyDnnIpEOVzgj8aFUAjAJXPNCItfQlhX\nIGACTUmF37eNw5qFDtcYJOD1qRBlmLfnTvcaSLEQXYCMY96+uvghr8N54CsnDDzIUMJB9VPH6Yr5\nC3DORxzyBXs/7P8Ar+1r7S95COBIg/Q4/Su3DS5ZWfU5q8Vy83Y+kLe5e+m2oAI16ZrXhn8hcuwB\nU9q5vRJVijZd+7PXPpWhcyG4Zo1clSMZFexF6HnEl5q7TStCJCeQM5rR0aB1YoAXbHFQ6V4dRFDS\n/N3ya31kW2BEUe5ypyQOBTuhJF21jNsg80gMByT61PbkPI+1uM5wO9UIbR5Nr3DA+orSSWO3jO0B\njUPcAa9MClIzucHbiq7l/skrzE5UbufxqMjMwmIPrx6VX1PU4IYZImbrkFVPWiwyl4fuxNa+QqkM\nCWAJx1JrR1CNy8YJ+RRkr61h3t3HEsV3bFVlTBAHRvUGr994hBs4LlUCIwODI3BB60IVrEzARxFt\nmFA4AHNUbgmRmIIRMcsxrCbxfJfXIt7eZcE4IjAx3rO17xENIieBM3OoSjCqTnGe5pbMdzUufEGn\n6W3ly3IkeTICxZJPpUct8Whabb5MIG7Lcsa5Pwx4Ynur5ru96J82fU9at+JNTaVlsogCDxuB6CgS\nZT03zNe195mLfZ4/u5PFdfayGaaUofkX5AfUVjWtmuiaQT/G4woHc1q6VG0VrHuGDgc5qV5lLQvN\nMViAJPPOfStvwE4h+KXgm4TeWN7Gh3dwTg4/WuandtrgjAHT3rq/hdYvf/E7wWuNoS9V8k5BCqx/\np0olZpgj7ut2WTcrD5SMVFY3v9lX7WrsTBId0ZPb1FEbFWByMZxxUetQGay3xnEsfzLmuayKOshn\nD4xxmp5QJoyrdeoNcLY63PPZrNDJmRfvRt6itLTPFsd5IYpG8qdeGjfg1lyso6GOTYpRmx2qrcrt\nywpjTiVtwYEU+SRGjYFutJAcV8VvDFr438G32k3o/wBEvI2t5D/dDDAb8CQa/F7x54cu/CniTU9I\nvE2XVjcSQSqezKxB/UV+22ohWhntpeY5AQD/AFr80f2//A8ei/EyDX4YhGNYtw04UYBnT5XYezDY\n31JrppktHyvBIFLDk1BMoBJf7oPP60u4hyBxj+I0+VEeMYPXnPXNbNNolaMq2d2+mXsV1GSdjZX3\nHpXRapa6Z4ksbm6srf7LqcIj/dR5ZLhfmDuck4b7vA469K5wIFd45OEPAPYUWWoXGk3aT28mGQ5H\nPBHoa5Zxb1W6N4tdTNKMA69+5/OmxOroM9Ac8Vt+I7qyvtSaaxieGGSNC6yAZ8zaN544wWyfxrB5\nTouDmmpXWpL0ZXu8rcwhcjLZGKebqZCVLEAepqvq0bM9uORkk8delVBbk9CwzXk1YpzZ6mHnKEbG\nwt5K+VDcg/nU5u5CAQU2+9ZelReVcSJkk7ehqTrbSEHBVsHiue2p3Ju1zS+0TE42qc9uKoshaVy4\nCsRkD86LnGYmBwSPpShlNxvORwAKVkXFt3uVZlYbR6DjNSXB/cqc5yM025JCxjOCCaSTDomSOg/r\nT6aidtUM1lgUt2GclMUscoFsgC5OODTNUAFlbtnJGRU+kyHyQD0C8DFP7OpjJN1GQ/aME5/LFPiu\nVwV5z65pzzl4mJCghtvIHSkgcMVVVXBGOB1o0sJXRNBOhU/eAzgkVMlzGXKlm9KtWFtH9nOQASfS\nqEl/bxuAUdT1zsqdHsVG8VqTiVdvyliR1oWYCXAcnPHSiHU7CMEsSp90NSHUdNcgjbGv45NKxrdi\nCTcwxLipYyWYfvM9s0sd1pJ2ncFYcnJNacb6E7bvtOwf3Q3/ANaotqaJu2p5rD94DvjuKtAfvAf8\nmqsGQ5yOfWrROOR0AxXb1PIjqieUkvgHnbgCoEdu5z2qQFmZTt7HmoGX0OR1NJ2vYtuw9ACCuehp\nC+TgdOtOQ4TBA9aYyY5BOc0JCk7k0TdQRyOlSAsxKnkVDGCWB6GpUOwmhaFRWmo9BtyN3XrnvXVf\nDXWhoniywlL7YmfypPoeP8K5IKSGz9eKlhmMbow69citISaaaMZK8bH21p8hCKwGQ3BI9K6vR7i3\nz8wG9eMmvM/hX4kTxN4VspS4+0Kvlyr/ALQ4z+PWuqlnFsxMbHJ7CvcjK+qPLaaep31reQgESSY5\n6A0sOqxbDsAOR3PSuNsbiWTaSzH1Fbtjbr5pPYnjitV5iNs3ks2QAT0IHQVZtrV2IctkY55qKzeK\nBMEgvzxVbWNVaztFht/nmc44pILC6vrVvYKS0qqcEH6VyjXd3rVwDawkR9C5/pWnpPhFrgpd6mzP\nnpH2H4VtzmHTow23y4du1UVcFj2oYKxz7aL9gtbWKSdpJpXx9Oua5/xT4b1PUtWa3iuhb6aFDDJ5\nGeortdOs7i4m+23C7SvEKH+Eev1rjfibfXkGoQWdp8ouI8MfQg//AF6hMfLcwZ9UtvDX+g6Wvn3Z\n+VpcZxmrfhvQ5Fla9ugz3L8mR+al8LeDzaAzXRPmHv61salqSabb/KcYGPpTavqJaDNX1MadZbQw\nDnqo9azPDulvcytd3OTklgMdBVbT7aTxDdGSXPlZyN3rXQ6lcpo+nErxgYA96GugIozStqOrJCmf\nJg5YY49q2D+7ByO2MA1l+FIDBA802TLId3HetYku2SAcHip2Qyrf5KqGIGSPzrv/AINwEfEPQZAV\nUxO0gOepCNXm2pyf6ZCuQFByQT0r0j4OusfjzRy4DAs4DA8/cNLRq4tD7MttSDDJzhh1xVuK88xS\nrHjGDXObUeJZIGeSPqNvOParOm3cZkVbl3B7ZGM1jy31KTGNK2i6sSoPkynqegNXNU0qDU4/OibZ\ncAZEi1dvNDh1aEiGYCQDgE5rO07zrNntrz5dp6r3pXvqUVdL1y5025FveOeOFc9DXYwX6XKDaQzE\ncYNZV14Ws9at2aC5bzRyFZcc1yq6HrVndGEedGo/iUEil7r2A7fVJbRIttxOiPjoTXyD+3RoNn4u\n+F7Xlmvn3Wj3Cy+bGucRt8rjP1Kn8K+oYPA19rFuTeXjIvuvOKqeLPhNp978NvE+hQJ5s+o6fNAr\nvyd5Q7fp82KIy5dhWufiMVaN5MgfTueaf5YdTyeBVrW7WSx1K4t3UxvGxRlYcggkGqRYYAU8kcj3\nrpUtOUlrqVpsopyMcgZ9arkkoRwTzzV2dd8WOhBqhKACVBBPSpfYtNhg+W2ePWmCHzSMckgUiBnc\nxkE54AHeugs7eKCILgeZgZz3rhqTUDqpw5zmdYg+yXVshJLeWdw/GqiSk9AM9DmtjxfFh7WYLjGU\nLf5+lYURBboev515Td3dnpwstES28hS7YZ65qMnelwoPU5z+NJ5o+1LjjtQw3PMANvGeTSWht0sT\nXLh7aD0zig9VI+9jpUTENYK2eQcUhJURsrcFcGloWnbQdfbvLx0w2aapzAjZIwce1OvJd1u2eMNk\n4qFcm2wMkA/hTSE92u4aixOmpjoG5NO0jHlEg8DI61Hdy7tKlUk9RnH1pNFYiM98HABpfZM0/wB5\nr2HsN6SgnJyKdGwDx4GAuBx60kjbmlAz1pqMAIt3c4pXuNWRv6YN6SZ5GMiqsEUciOjqDg+nSpNM\nkCFsHqMc02IYllye5NSjboV5YYI2kXy8gHbSqLMOcRsB04pLp/Kml5BJIz+VVQ/DknHtVPYyvZl2\nWOy2H5WwTj7tLFb6c2FkJwfRcVQEu6EgkdeOKaZTtxu6Hg1Ni+e2pho2HJPrVy1CmdRKMoe3SqcY\n2kgdQatRDBHPI55rqdjzKd2PdsY2iogCSTgn2qxwdpyAM+lRAjd8pOBznHWjc0e4Lzk+3akOeCet\nC/KSByCemaXBYnd1J71K0JfkKGO5CpyOlPDnByAe3XrUa4D9QMe1ODYyCck8jFXoF2iVSTkdB096\nVOHC9R0qNBgkk5zUsZ+fA7d/WknbYe6PWfgN4glstYurHJEcybxjoGFfSdlGbhSSA4YdMd6+df2e\ndLW7vtYnYfcgEYOOhLZ/pXvfhzUUmjw0mHibBA65r2cM/wB3qeZVS5mdRb26RR/cAA4+taUc8cCD\ngDpnvWTbXW4E5yM1aERZRj5ia7OljA0DehZWIUEkZyKm0ye3cu7bFcc/N2rMW2LblUdOeO1SJpLy\nMfJyAe5osBvHUoo2bdIpCHJ57dqpXGv2bMHchsfdY84NQweGhI4aWQnjBq/FolpahWaMPg8E8mpv\noAkWsRLA8ygkHkFhj8q4HXtZivbyHaqvPG5IOO+eK3deuZdVvlsLTKqnDueMU+18MW+mxiUoJZME\n726/WpsitTKm1yKSAbHO9lBC9xWNFYzavLmRsIcd+lWLXTRc3EsjECISNj2GTWsZUtgIYgAxGOKr\noIS0t47QeVEoGOpFYWubtR1OKzDFkU7n5/Kt6VvsNs7ueOpNYPh6F724uL0rnzX79QtSB0dvb+VC\nqLjNOdQCWySuOn86lzweMEDg1TvJjDHJg4yC3ShoSMN7kSas/fJA616b8J5vL8c6EwUKROQT3Pyn\nGa8n00tJdytgEMeterfCKIXXj3REYqqpIzFh14Rjz+VQtrCPsXSbhYJDJHj5+u3jmujt7m3lULJC\nhHriuOsCRgBlBx1FasMsgI5BxznpWMl2NEddaw2ww0aqh68VLLo9ldziWRA0g71hWl80RGSCPetK\nDVAzZ7msLMZsW8FvF9yMKR0IqYumAcCsuK8XJwetSG53DJPApDLnmgdOlRSAA8mqxmP0prTAg8gG\ngD80/wBvr9my58GeLLzxzodiW8Oaq/m3Hkr8tpcE/MG9FcncD0ySPSvjElozww3dyK/dT4j+DdM+\nJXgrV/C+qBmsdRgMTsn3kOcqw9wwB/CvxL8beEn8N+JdT0lpN72l1JAz4xnaxGf0reNRJaiszn2L\nFcZ3HvxVSVT5mOCfSr6wskZTeMYycetRwKFc/wB8nHzUSqK2hcYk+mWiwHe/3sceoq+wBfJBVs8G\nqgHAwVY9BVsochSST6ivLqNt3PQppWsjP8SR+ZpM7EgFcOB9PSuSVgACxyfyrudf0S6vfD17dQRs\n8NsFeZ/Rc1w5AOMDJ4NYNHTCzvqRtlJ0yfSpQNs0qn5sr1qvLnzY8dBjk1YkJFwAcZK0ir6DI/mt\nXX+HPFIxH2Re23vS2+fLlGBjrmomJNkeR1pbjbaQ8S7rd1bnkHP51FBMjWzk881CpJjKhuMCmocK\n69O5qlpcOYmLk2M6gZzj+dLojEMVPTOarGQeQ+DyeuKl0c/vD3xjrS6ELWSuXZflmnG7qveoEA2I\nNpPNSzL/AKQTgEGoA5EQX0NTYtNJmzZsAzd/l/CpVA+1ODkbuar2bkOnA56571YJ/wBLPPQCs15n\nQtSnqSFJTzjpVJgDI2Dxxir2qE+YvBIAH9azmcksT8uPSqiY6JkYm8y48gOqEjO49qqCVt/I3Hpy\narTHbO7A7cHIqaOQnBI74zWytY4XJt2GRruZtp98evNWUbMmTwOnSod4NwcLhcAcmp85K9CM9Kvq\nESR+UUZz9RVcMVOBgCrDkGNemM96gU4Y8g896Vx21HqOM9u1Jkb8Z796arHJ746CkU7mbPFO2gX6\nEmSW68dDxQ2MnbzjkYoXqV/KpBg4Ixn1pJlJ9RYSJDyADSp97kDNM78YNKoznJww6GmnrYT2ufQf\n7O1mI9A1W5T7zzBc49B/9eu8kZtNvY7tDnexR1HqDXP/AABhS28BqQAPOldyc984/pXU3VstxfS2\ncgKtkyRkHrXsUnaKPNlq2dTpd/5ilnICkdhWlHfGKRmwWHTPpXF6LdNCxiY4IbHPauosZ1kRlJzk\nYIrrTMdjWivWYM3IPrVqDWmhVQWHAwSetUrJUkG4ngdulK2mJJOWGcEc/SqQuhsr4hCA5UMGHHNT\nLqayRlvMyfT0zVOy0aMKTICWQ42modakgsYcRKXk/uipAnsL63EzkgA5zk9alv7+AxbQ4ZmG3APQ\nVzttpklyDO8hj5+6DVi20ctKshckA8fhUtdSkzIluv7PvDtI8o4Vs9vQ1f0u389zI43tjGf61U1W\n0B1R4euEy+D0HbNXLe6jgsDJ2RcntSjZAzL8VXTSmKyjYCWU5b1A71oadH9lh2lduBWDoRa+1Oe9\nkIKMcR+wFdIELBufun9KF3J06EiyZyc5OKydZmCWsmDk465rRlAAGW9wRXL+ILj9xIiHaxOPak9Q\n2IdJJjlQHJI5NdVovjG58H65pGoW6q8kcpDI65BUg5H61y1lIYvIztY7c/Wm6hctc6vCseWEUeCG\n9W//AFVvQhzzSZlUlyRbR9xeCPiNoHjCzt5LDUIY7tl+exdwJFPfg139r5jxlV5BHf8Az7V+cNtL\ntuTlnilU/eX/ADxXX2HxT8deF7gHTtcvJLNdvBk81QPcNnFbzwf8jM4YhPRn3sPNQfMAafHNOuCO\nfavku5/aU8V28YijvI2lDHBeBW3dcdvapf8Ahojx0dLmvJWtrdRtWMtCMtk4JxXM8JNI1VaLPrmH\nUJQ2GRlPYH/P0rTtjLcAc4GPSviH/hp3xm4VhJAxORhIh1+hqJ/2jvHF3lF1p7UsMbUjVD+eKX1G\now+sQPumYRW6GS5u1hRRkl2Cgf5xXn/in48+CfC0slu+qf2jeJ1t7P5z+J6frXxfrGv+I/EEH2nV\nNZu7594KrLOzDjnpmqOvx2sclnq6RlopUCSNGejD1+orSOBUXabJ9u2rpH0B4u/aa8S6kJbbwzpt\nhpSk7Rc3zGZ8djtGAP1r87viJJd3XjDWptQkWW8lu5ZJpEXAZyxJIHua+pfDr6VdyKJJnCkHAY4r\n5l+L/lxeP9aW3J8rzsrnvxXNjaMKcVyIuhUlJu5wMg2bsgEN61CuVk4wQeOfWrNzIMIBhc9c9TVM\nuSzAHgHjivNV7Heh6BkbBIx6CrdkDPKiqGckgAY756VURTv3ZO7HQV2fgbRBe6kuc+XF+9Y49OcU\nRpupJQXUtVPZxcmeq6T4StbTwLNYXcKia6hZLhcEHknj8BXynqentpd/dWjcPbytCx+jEV9p6teR\nyCScNvint/NB7HK5/rXzB8bNMh0vx9eSW8flQ3ccdwq5yNxQBv1BP41ri6DpxT+QsJVUpuPc88kl\n8sxnJBB71KxLXMbZ4xz+dQ3I3R/NT2IzHk59SO1eUz1EiSFSJpQPeoYwHt5R6HrU4Km8OCcEZwKg\njH7uUDggVCZVm0Ut5GD7kfWmBwOh45FMd8uB1HXNNaYe1aNHPzdiSEbw23g96m0zaZiqn61Vhl25\nYfp2qbTCBdbR165FT0Ki1zI0pWPn4bqaqjI3cdG71auN3nDsfeqvlkmTJ4zUo1e5q2TsVQkkfMOa\nmeYyXK4X5Txn1qtauVhXnnPerEioskbE4Hr+NQuxuujI9TYfITxwRx9aznb95j1HPvV7UxiNTjBB\nNZ+d0itjjGKpbGXNrqZc8DPMzAZAODiouhI3HnnitezcLdHoATyKr6yIze424yeSO9dMUmrs8ptq\nTKzEiQj161aib7p4AzVXB81jnr+tTng8Z7cUmjePUllAVcA5w1RIRnd2A705sBc/pUQPB4PJ60tx\nkiOFBGMn3pNu4gMcDimKwIIY8fTmpVUBCM8U9kG49BtII6A/nQuVZhjikD/MMZwKMfeJzg0o+Y35\nD0GDzx7D1qQcnGdwBqFW+X0PrTyoZsKTVKwr32PqL4JMn/CBWiR8OGYn8zXazWz3lwk6geZH3Hev\nM/g3qH2DwzZhm3QncCM+5r1DTLmORmKSBtvBxzj0r2aekUeZK12VpIRHdFmXZ5h5OeN1atvmAqwO\nF9M96jvrQXkBAGAevrVPRL7zXeCYbJ4jtYetaoho34r5kYKRtA6cVr22pLvA3DOOAaxjCJANgOB3\nPemm2nQrIuTgcAVSb6Eo7CG4MqH5sZ/nUcenNMS7ncSePauVEl1ESyn72AAT0q9b6jep8qs2ScEn\noKpMdze/s/DDLgqDyAOancLHDkgLGvJ9azIr6cD96OTz7UT3fmxYLYAyCfUUmCMlZf8Aj7mI3NI2\n0MTVTWdOmk0DCsyOAN5H8S5rctJLWKMow3AVQvNRjuttqmdrZ4rLVMq2hX0ezjsrNcKDnkVeI2nk\n43DNV7F2CGFhhouMD07Ul3c7FZm+XHrVX0uRsJc3Cqnf5RjFcjrNwkp2A4G70rfuLjFu2OmPmri5\n3e5llKjIVuoPSiK5g1NiCRIIMuAcL1bsKi0S5a9IuVO3zmLZP93t+lYmtXsg09448/eCO2OgPpUe\nkauyXCKJcADG0jp2r0cLFK8jhxDb907byrpGZvklQk8EdqiubmS1UnymTI529/wqlZa/OkjwTMCn\nY7a1/tPnWwcYc/3TXc2cyWp2nhi1tJ9MjvrgCSUONmRz+NV/HPiGzltnswxeSMbjsHAPXFZfhi8a\nK2l3A7PvAeh6f4VlzaNLdSXsIcKzZYuBnPJrOOurNna1kSaddWS2Cy+SzN3YZ65NOMtteAFZGUZ6\nMO1Tafpk39jZEm5AxB4FMsnWOV42YcjBDCtU7oy5bSGw2csTPJaXqxhSDtZuPyq1Z399bSmOe2W4\nsp8rKI2z+IFPQQtKjNCjIeCtA0aJ0YR/KQcjnG05qWr7mkXylxtGubCL7Zp0I1O1VsmNeJEHuO9f\nMnxJme78YanKyeWGlPykdMdq+pdHtbi3nM1vPJA6kchsq31HeuM+I/wg/wCE1imu7K3jttakcM1w\nj4SX13J2J9c15WKo1KiSWtjro1IxbZ8xSRZAYE/Sm22nzXZxDC8x/hCqSTX0f4K+Cmm+H7NxrunX\nGq3bHOWUhI/pg8/jXb29rp2khRY6LDAwOMBMYrijhJdTpdZHzj4R+EWq6zdRy3yNYWJwWeQfMR6B\nfWvoDwF4J0SS7t/DtlaCzilBX7ZccyO2Cc/59a0b57i5kDRwRQcBtoHFYVxby22s/amd3d4gUiJ+\nXIJ5FejSw/JrHc5alTm+LYsapZQ2TWGnowmFpbtA0gXaHw7YOD7Yr55/aKsFWTR7wEb9phJx0xkY\n/wDHa95kn86O2bYQfmBz1zkn+teRftEaeW8PRSggmC7zgdlYf4muTGK9Np9DbD2U1JHzzKcQMe4O\nKR2Hko3XcOlE7Dy2xxznkUxRm1Q4JIPavm7Hvx2ZYUbJkJwcjIPtTFOZJNxOOgx60hK+ZG/0/Cgg\nG7cVJtfoZVwoR3A5z0FQDjdnk9qtXQPnMc+1V5EIXJx9OtaX6HE7p6DVY4yASR0/OrVu4W6GT+VV\nFc7VA645qeHCSoQDuyKGtBptamncSN5iEHjoM1FnmQZz2xT7nI2kkc9M1GBh2yOorJHTLRlyzdfK\nGOADV+4yzRn06VnWhxC/TI6YGauTs2I8cKSOR16VOxul7o3UBvtxk8F+tZRwp55HqK1dS4tWx90E\nc5rIdl3DHORVIyk9WFq+y9wfr1qrqd0Jrpyp46Urv5UrEHOByTUEMBnmWPIBY4ya3TdtDznu2j//\n2Q==\n','I am aewsome!','m',16,58,50,45.4697727,9.223599,'2015-01-15 17:31:39',7,NULL,'IT','3664e354b268acea907ab82623c48669de81711717385b35c6aefb2adfc7a051',0),(16,'34453443','Zito','1411242b2139f9fa57a802e1dc172e3e1ca7655ac2d06d83b22958951072261b','1998-01-01 00:00:00','f',NULL,'E\' zito','f',78,87,900,45.4697738,9.2236169,NULL,7,NULL,'IT','fgf',0),(19,'353490069506216','El Magnifico','1411242b2139f9fa57a802e1dc172e3e1ca7655ac2d06d83b22958951072261b','1990-01-15 00:00:00','m',NULL,'Magnifico','f',18,32,646,45.47870921,9.23123219,'2015-01-27 15:03:56',7,NULL,'IT','39d8cac9cf2508ad7162560f20907a3014aaf1438cba918352153fbb83076629',0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `pick_me`.`users_last_update`
AFTER INSERT ON `pick_me`.`users`
FOR EACH ROW
begin
    INSERT INTO notifications(UserID, NewRequests, NewMessages, NewFriends) VALUES(new.ID, 0, 0, 0);
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Dumping routines for database 'pick_me'
--
/*!50003 DROP FUNCTION IF EXISTS `AreBlacklisted` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 FUNCTION `AreBlacklisted`(
    userID0 int,
    userID1 int
) RETURNS tinyint(1)
BEGIN
    if exists (select * from blacklist where (IssuerUserID = userID0 and TargetUserID = userID1) or (IssuerUserID = userID1 and TargetUserID = userID0)) then
        return 1;
    else
        return 0;
    end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `AreFriends` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 FUNCTION `AreFriends`(
    uid0 int,
    uid1 int
) RETURNS tinyint(1)
BEGIN
    return if(exists(select * from friends where (UserID0 = uid0 and UserID1 = uid1) or (UserID0 = uid1 and UserID1 = uid0)), 1, 0);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `ComputeDistance` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 FUNCTION `ComputeDistance`(
    lat1 double,
    long1 double, 
    lat2 double,
    long2 double
) RETURNS double
BEGIN
    return 1000 * 6353 * 2 * ASIN(SQRT( POWER(SIN((lat1 -
        abs(lat2)) * pi()/180 / 2),2) + COS(lat1 * pi()/180 ) * COS( 
        abs(lat2) *  pi()/180) * POWER(SIN((long1 - long2) *  pi()/180 / 2), 2) ));
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `AcceptRequest` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `AcceptRequest`(in requestID int)
begin
    insert into friends(UserID0, UserID1)
        (select SenderUserID, RecipientUserID from requests where ID = requestID);
    delete from requests where ID = requestID;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `DeclineRequest` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `DeclineRequest`(in requestID int)
begin
    update requests set Declined = 1 where ID = requestID;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `GetBlacklistedUsers` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `GetBlacklistedUsers`(in userID int)
BEGIN
    select u.`Name`, u.Birthday, u.Gender, u.Picture, u.Description, b.Reason, b.IssueDate
    from blacklist as b, users as u
    where b.IssuerUserID = userID
      and u.ID = b.TargetUserID;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `GetFriends` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `GetFriends`(in userID int)
    READS SQL DATA
BEGIN
    set @ulat := (select Latitude from users where ID = userID);
    set @ulong := (select Longitude from users where ID = userID);
    
    select ID, `Name`, Birthday, Gender, LastUpdate, Description,
         ComputeDistance(@ulat, @ulong, Latitude, Longitude) as Distance
    from users
    where not (ID = userID) and AreFriends(ID, userID)
    order by LastUpdate desc;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `GetIncomingRequests` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `GetIncomingRequests`(
    in userID int
)
    READS SQL DATA
BEGIN
    set @ulat := (select Latitude from users where ID = userID);
    set @ulong := (select Longitude from users where ID = userID);
    
    select u.ID, u.`Name`, u.Birthday, u.Gender, u.Description, 
         r.IssueDate, r.`Text`, r.ID as RequestID,
         ComputeDistance(@ulat, @ulong, u.Latitude, u.Longitude) as Distance
    from users as u,
         requests as r
    where r.RecipientUserID = userID
	  and r.SenderUserID = u.ID
      and r.Declined = 0;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `GetNearbyUsers` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `GetNearbyUsers`(
    in userID int
)
    READS SQL DATA
BEGIN
    set @country := (select Country from users where ID = userID);
    set @gender := (select LookingForGender from users where ID = userID);
    set @ageMin := (select LookingForAgeMin from users where ID = userID);
    set @ageMax := (select LookingForAgeMax from users where ID = userID);
    set @distance := (select LookingForRange from users where ID = userID);
    set @ulat := (select Latitude from users where ID = userID);
    set @ulong := (select Longitude from users where ID = userID);
    
    select ID, `Name`, Birthday, Gender, Description, LastUpdate,
         ComputeDistance(@ulat, @ulong, Latitude, Longitude) as Distance
    from users
    where not (ID = userID) 
      and (Longitude is not null and Latitude is not null)
      and (Country = @country)
      and (Gender = @gender)
      and ((TIMESTAMPDIFF(YEAR, Birthday, CURDATE()) >= @ageMin) and (TIMESTAMPDIFF(YEAR, Birthday, CURDATE()) <= @ageMax))
      and (abs(Latitude - @ulat) < 0.3 and abs(Longitude - @ulong) < 0.3)
      and ComputeDistance(Latitude, Longitude, @ulat, @ulong) < @distance
      and not AreFriends(ID, userID)
      and not AreBlacklisted(ID, userID);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `GetUnreadMessages` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `GetUnreadMessages`(in recipientID int, in senderID int)
BEGIN
    select * from messages
    where RecipientUserID = recipientID 
      and SenderUserID = senderID
      and `Read` = 0;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `GetUnreadMessagesCountsBySender` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `GetUnreadMessagesCountsBySender`(in userID int)
BEGIN
    select SenderUserID, count(*) as Count from messages
    where RecipientUserID = userID and `Read` = 0
    group by SenderUserID;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ReadMessagesFrom` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `ReadMessagesFrom`(in readerUserID int, in writerUserID int)
BEGIN
    update messages
    set `Read` = 1, ReadDate = now()
    where RecipientUserID = readerUserID and SenderUserID = writerUserID and `Read` = 0;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-01-27 15:09:48
