var express = require('express'),
    http = require('http'),
    mysql = require('mysql'),
    crypto = require('crypto'),
    pickme = require('pick.me');

var app = express();
var server = http.createServer(app);

var connection = mysql.createPool({
    connectionLimit: 50,
    host     : 'localhost',
    user     : 'root',
    password : '',
    database : 'pick_me'
});

var io = require('socket.io').listen(server);
server.listen(3000);
io.set('log level', 1);

var userFactory = pickme.userFactory(connection);
var logFunc = function (obj) { console.log(obj); };
var loggedUsersContexts = new Array();

io.on('connection', function(socket) {
    var connectionContext = new Object();
	connectionContext.socket = socket;
	
	function ensureUserLogged() {
		if (connectionContext.loggedUser) {
			return true;
		} else {
            connectionContext.socket.emit('not logged');
			return false;
        }
	}
    
    function debug(text) {
        if (ensureUserLogged())
            console.log("[" + connectionContext.loggedUser.ID + ":" + connectionContext.loggedUser.Name + "] " + text);
        else
            console.log("[" + socket.id + "] " + text);
    }
    
    debug('connected');
	
    socket.on('check username', function (username) {
        debug('check username ' + username);
        userFactory.checkUsername(username,
            function () { socket.emit('check username success'); },
            function (error) { socket.emit('check username failure', error); });
    });
    
    socket.on('create user', function (userData) {
        debug('create user ' + JSON.stringify(userData));
        userFactory.createNew(userData,
            function (secret) { socket.emit('create user success', secret); },
            function (error)  { socket.emit('create user failure', error); });
    });
    
    socket.on('login', function (credentials) {
        debug('login ' + JSON.stringify(credentials));
        userFactory.login(credentials, 
            function (loggedUser) {
                connectionContext.loggedUser = loggedUser;
                loggedUsersContexts[loggedUser.ID] = connectionContext;
                socket.emit('login success', loggedUser.asJSON());
            },
            function (error) { socket.emit('login failure', error); });
    });
    
    socket.on('update position', function (location) {
        debug('update position ' + JSON.stringify(location));
		if (!ensureUserLogged()) return;
        connectionContext.loggedUser.Latitude = location.latitude;
        connectionContext.loggedUser.Longitude = location.longitude;
        connectionContext.loggedUser.updatePosition(
            function () { socket.emit('update position success'); },
            function (error) { socket.emit('update position failure', error); });
    });
	
	socket.on('update preference', function (preference) {
        debug('update preference ' + JSON.stringify(preference));
		if (!ensureUserLogged()) return;
        connectionContext.loggedUser.LookingForGender = preference.lookingForGender;
        connectionContext.loggedUser.LookingForAgeMin = preference.lookingForAgeMin;
        connectionContext.loggedUser.LookingForAgeMax = preference.lookingForAgeMax;
        connectionContext.loggedUser.LookingForRange = preference.lookingForRange;
        connectionContext.loggedUser.updateSearchPreferences(
            function () { socket.emit('update preference success'); },
            function (error) { socket.emit('update preference failure', error); });
    });
	
	socket.on('upload picture', function (base64Picture) {
        debug('upload picture');
		if (!ensureUserLogged()) return;
		connectionContext.loggedUser.Picture = base64Picture;
		connectionContext.loggedUser.updateProfile(
            function () { socket.emit('upload picture success'); },
            function (error) { socket.emit('upload picture failure', error); });
	});
	
	socket.on('update profile', function (userData) {
        debug('update profile ' + JSON.stringify(userData));
		if (!ensureUserLogged()) return;
		connectionContext.loggedUser.Description = userData.description;
		connectionContext.loggedUser.Gender = userData.gender;
		connectionContext.loggedUser.Country = userData.countryCode;
		connectionContext.loggedUser.updateProfile(
            function () { socket.emit('update profile success'); },
            function (error) { socket.emit('update profile failure', error); });
	});
	
	socket.on('get nearby users', function () {
        debug('get nearby users');
		if (!ensureUserLogged()) return;
		connectionContext.loggedUser.getNearbyUsers(
			function (nearbyUsers) { socket.emit('get nearby users success', nearbyUsers); },
			function (error) { socket.emit('get nearby users failure', error); });
	});
	
	socket.on('get picture', function (id) {
        debug('get picture ' + id);
		if (!ensureUserLogged()) return;
		userFactory.select(id,
			function (user) { socket.emit('get picture success', { ID: user.ID, Picture: user.Picture }); },
			function (error) { socket.emit('get picture failure', error); });
	});
	
	socket.on('send request', function (request) {
        debug('send request ' + JSON.stringify(request));
		if (!ensureUserLogged()) return;
		connectionContext.loggedUser.sendRequest(request.targetID, request.text,
			function () { 
				socket.emit('send request success'); 
				var targetContext = loggedUsersContexts[request.targetID];
				if (targetContext)
					targetContext.socket.emit('request received', connectionContext.loggedUser.Name);
				else
					userFactory.addNotifications(request.targetID, 1, 0, 0);
					
			},
			function (error) { socket.emit('send request failure', error); });
	});
	
	socket.on('get incoming requests', function () {
        debug('get incoming requests');
		if (!ensureUserLogged()) return;
		connectionContext.loggedUser.getIncomingRequests(
			function (requests) { socket.emit('get incoming requests success', requests); },
			function (error) { socket.emit('get incoming requests failure', error); });
	});
	
	socket.on('accept request', function (requestID) {
        debug('accept request ' + requestID);
		if (!ensureUserLogged()) return;
		connectionContext.loggedUser.acceptRequest(requestID,
			function (acceptedRequest) { 
                socket.emit('accept request success', requestID); 
                var targetContext = loggedUsersContexts[acceptedRequest.SenderUserID];
				if (targetContext)
                    targetContext.socket.emit('request accepted', connectionContext.loggedUser.Name);
                else
                    userFactory.addNotifications(acceptedRequest.SenderUserID, 0, 0, 1);
            },
			function (error) { socket.emit('accept request failure', error); });	
	});
	
	socket.on('decline request', function (requestID) {
        debug('decline request ' + requestID);
		if (!ensureUserLogged()) return;
		connectionContext.loggedUser.declineRequest(requestID,
			function () { socket.emit('decline request success', requestID); },
			function (error) { socket.emit('decline request failure', error); });	
	});
	
	socket.on('get notifications', function () {
        debug('get notifications');
		if (!ensureUserLogged()) return;
		connectionContext.loggedUser.getNotifications(
			function (result) { socket.emit('get notifications success', result); },
			function (error) { socket.emit('get notifications failure', error); });
	});
	
	socket.on('get friends', function () {
        debug('get friends');
		if (!ensureUserLogged()) return;
		connectionContext.loggedUser.getFriends(
			function (friends) { socket.emit('get friends success', friends); },
			function (error) { socket.emit('get friends failure', error); });
	});
	
	socket.on('get unread messages counts by sender', function () {
        debug('get unread messages counts by sender');
		if (!ensureUserLogged()) return;
		connectionContext.loggedUser.getUnreadMessagesCountsBySender(
			function (counts) { socket.emit('get unread messages counts by sender success', counts); },
			function (error) { socket.emit('get unread messages counts by sender failure', error); });
	});
	
	socket.on('send message', function (message) {
        debug('send message ' + JSON.stringify(message));
		if (!ensureUserLogged()) return;
		connectionContext.loggedUser.sendMessage(message.recipientID, message.text,
			function (result) { 
				var targetContext = loggedUsersContexts[message.recipientID];
				var messageID = result.insertId;
				
				socket.emit('send message success', { HashCode: message.hashCode, ID: messageID });
				
				if (targetContext)
					targetContext.socket.emit('message received', { ID: messageID, SenderUserID: connectionContext.loggedUser.ID, SenderUserName: connectionContext.loggedUser.Name, Text: message.text, SentDate: new Date()});
				else
					userFactory.addNotifications(message.recipientID, 0, 1, 0);
			},
			function (error) { socket.emit('send message failure', error); });
	});
	
	socket.on('mark message as read', function (messageID) {
        debug('mark message as read ' + messageID);
		if (!ensureUserLogged()) return;
		connectionContext.loggedUser.markMessageAsRead(messageID,
			function (message) { 
				var targetContext = loggedUsersContexts[message.SenderUserID];
				if (targetContext)
					targetContext.socket.emit('message read', message.ID);
			},
			function (error) { console.log(error); });
	});
	
	socket.on('get all messages from', function (targetUserID) {
        debug('get all messages from ' + targetUserID);
		if (!ensureUserLogged()) return;
		connectionContext.loggedUser.getAllMessagesFrom(targetUserID,
			function (messages) { socket.emit('get all messages from success', messages); },
			function (error) { socket.emit('get all messages from failure', error); });
	});
    
    socket.on('get unread messages from', function (targetUserID) {
        debug('get unread messages from ' + targetUserID);
		if (!ensureUserLogged()) return;
		connectionContext.loggedUser.getUnreadMessagesFrom(targetUserID,
			function (messages) { socket.emit('get unread messages from success', messages); },
			function (error) { socket.emit('get unread messages from failure', error); });
	});
	
	socket.on('blacklist', function (targetUserID, reason) {
        debug('blacklist ' + targetUserID + ' (' + reason + ')');
		if (!ensureUserLogged()) return;
		connectionContext.loggedUser.blacklist(targetUserID, reason,
			function () { 
                var targetContext = loggedUsersContexts[targetUserID];
                
                socket.emit('blacklist success', targetUserID); 
                
                // The target user is notified only if it is a friend of the issuer
                if (targetContext)
                    connectionContext.loggedUser.isFriendOf(targetContext.loggedUser.ID,
                        function () { targetContext.socket.emit('blacklisted', { IssuerUserName: connectionContext.loggedUser.Name, Reason: reason }); },
                        function (error) { debug(error); });
            },
			function (error) { socket.emit('blacklist failure', error); });
	});
	
	socket.on('read messages from', function (targetUserID) {
        debug('read messages from ' + targetUserID);
		if (!ensureUserLogged()) return;
		connectionContext.loggedUser.readMessagesFrom(targetUserID,
			function () { 
                var targetContext = loggedUsersContexts[targetUserID];
				if (targetContext)
					targetContext.socket.emit('all messages read by', connectionContext.loggedUser.ID);
            },
			function (error) { console.log(error); });
	});
    
    socket.on('unfriend', function (targetUserID) {
        debug('unfriend ' + targetUserID);
        if (!ensureUserLogged()) return;
		connectionContext.loggedUser.unfriend(targetUserID,
			function () { 
                var targetContext = loggedUsersContexts[targetUserID];
                
                socket.emit('unfriend success', targetUserID); 
                
                if (targetContext)
                    targetContext.socket.emit('unfriended', connectionContext.loggedUser.Name);        
            },
			function (error) { socket.emit('unfriend failure', error); });
    });
    
	socket.on('switch device', function (username, password, newDeviceID) {
		debug('switch device ' + newDeviceID);
		userFactory.switchDevice(username, password, newDeviceID,
			function () { socket.emit('switch device success'); },
			function (error) { socket.emit('switch device failure', error); });
	});
	
	socket.on('block', function (username, password) {
		debug('block');
		userFactory.block(username, password,
			function (blockedUser) { 
				var targetContext = loggedUsersContexts[blockedUser.ID];
                
				socket.emit('block success'); 
				
				if (targetContext)
					targetContext.socket.emit('blocked');
			},
			function (error) { socket.emit('block failure', error); });
	});
	
	socket.on('change password', function (username, password, newPassword) {
		debug('change password');
		userFactory.changePassword(username, password, newPassword,
			function () { socket.emit('change password success'); },
			function (error) { socket.emit('change password failure', error); });
	});
	
    socket.on('logout', function() {
        debug('logout');
        if (!ensureUserLogged()) return;
		delete loggedUsersContexts[connectionContext.loggedUser.ID];
		connectionContext.loggedUser = null;
    });
    
    socket.on('disconnect', function() {
        debug('disconnect');
        if (!ensureUserLogged()) return;
		delete loggedUsersContexts[connectionContext.loggedUser.ID];
		connectionContext.loggedUser = null;
    });
});
